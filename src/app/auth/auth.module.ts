import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpClientModule } from '@angular/common/http';
import { NgOtpInputModule } from 'ng-otp-input'

import { IconsModule } from '../shared/index';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HelperService } from '../utils/helper.service';
import { LoginComponent } from './login/login.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { AuthComponent } from './auth.component';
import { RegisterComponent } from './register/register.component';
import { SharedModule } from '../shared/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';

const routes: Routes = [
    {
        path: '',
        component: AuthComponent,
        children: [
            {
                path: 'login',
                component: LoginComponent
            },
            {
                path: 'forgotpassword',
                component: ForgotPasswordComponent
            },
            {
                path: 'register',
                component: RegisterComponent
            },
        ]
    }
]
@NgModule({
    declarations: [
        AuthComponent,
        LoginComponent,
        ForgotPasswordComponent,
        RegisterComponent
    ],
    imports: [
        CommonModule,
        IconsModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),
        HttpClientModule,
        NgOtpInputModule,
        SharedModule, NgSelectModule
    ],
    providers: [
        HelperService,
    ],
})
export class AuthModule { }
