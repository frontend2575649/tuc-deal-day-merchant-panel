import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { OResponse } from 'src/app/interfaces';
import { HelperService } from 'src/app/utils/helper.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  ShowPassword: boolean = false;
  _Form_Login_Processing = false;
  _Form_Login: FormGroup;
  step: number = 1;

  constructor(
    public _FormBuilder: FormBuilder,
    public _Router: Router,
    public _HelperService: HelperService
  ) {
    this._Form_Login = _FormBuilder.group({
      'username': ['', Validators.required],
      'password': ['', Validators.required],
    });
  }
  ngOnInit(): void {
    this._HelperService.SetNetwork();
  }
  _Form_Login_Process(value: any) {
    this._Form_Login_Processing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.Login,
      UserName: value.username,
      Password: value.password,
      PlatformCode: 'web',
    };
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.Credentials,
      {
        UserName: pData.UserName,
        Password: pData.Password,
        PlatformCode: 'web',
      }
    );
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.System, pData);
    _OResponse.subscribe(
      async (_Response) => {
        this._Form_Login_Processing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.Account, _Response.Result);
          var _StorageReqH = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.OReqH);
          _StorageReqH.hcuak = _Response.Result['AccessKey'];
          _StorageReqH.hcupk = btoa(_Response.Result['PublicKey']);
          this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.OReqH, _StorageReqH);
          this._HelperService.NotifySuccess(_Response.Message);
          this._Form_Login.reset();
          await this._HelperService.RefreshHelper();
          await this._HelperService.SetNetwork();
          if (_Response.Result.UserAccount.AccountTypeCode == this._HelperService.AppConfig.AccountType.Merchant ||
            _Response.Result.UserAccount.AccountTypeCode == this._HelperService.AppConfig.AccountType.SubAccount) {
            var TAccessKey = _Response.Result['AccessKey'];
            var TPublicKey = _Response.Result['PublicKey'];
            var Key = btoa(TAccessKey + "|" + TPublicKey);
            window.location.href = '/';
          }
          else {
            this._HelperService.NotifyError('Invalid account. Please contact Support');
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._Form_Login_Processing = false;
        this._HelperService.HandleException(_Error);
      });

  }
  ToogleShowHidePassword(): void {
    this.ShowPassword = !this.ShowPassword;
  }

  ShowPassword1: boolean = true;
  ShowPassword2: boolean = true;
  ToogleShowHidePasswords(val: string): void {
    if (val == "new") {
      this.ShowPassword1 = !this.ShowPassword1;
    }
    else {
      this.ShowPassword2 = !this.ShowPassword2;
    }
  }
  onOtpChange(event: any) {
  }

  LoginForm() {
    this.step = 2;
  }
 
  resetPass() {
    this.step = 5;
  }
  EnableFA() {
    this.step = 6;
  }

  EnableTwo() {
    this.step = 1;
  }

}
