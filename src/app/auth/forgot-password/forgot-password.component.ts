import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgOtpInputComponent } from 'ng-otp-input';
import { Observable } from 'rxjs';
import { OResponse } from 'src/app/interfaces';
import { HelperService } from 'src/app/utils/helper.service';
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css', '../login/login.component.css']
})
export class ForgotPasswordComponent {
  formProcessing = false;
  _ResetRequest_Success = false;
  formForgotPassword!: FormGroup;

  formResetPassword!: FormGroup;
  stpper: number = 1;
  ShowPassword1: boolean = false;
  ShowPassword2: boolean = false;
  public AccessCode: any;
  ispwdContainsNum: boolean = true;
  ispwdContainsUC: boolean = true;
  ispwdContainsLC: boolean = true;
  IsminLength: boolean = true;
  isPwdContainsSC: boolean = true;
  @ViewChild(NgOtpInputComponent, { static: false }) ngOtpInput !: NgOtpInputComponent;
  constructor(
    public _FormBuilder: FormBuilder,
    public _Router: Router,
    public _HelperService: HelperService
  ) {
    this.formForgotPassword = _FormBuilder.group({
      'username': ['', [Validators.required, Validators.pattern(_HelperService.AppConfig.EmailPattern)]],
    });
    this.formResetPassword = _FormBuilder.group({
      'password': ['', Validators.required],
      'confirmPassword': ['', Validators.required],
    });
  }
  get _formForgotPassword() {
    return this.formForgotPassword.controls;
  }
  _Form_Reset_Process(value: any) {
    this.formProcessing = true;
    var pData = {
      Task: 'forgotpassword',
      EmailAddress: value.username,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.MOnBoard, pData);
    _OResponse.subscribe(
      _Response => {
        this.formProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.AppConfig.ActivePasswordResetRequest = _Response.Result;
          this._HelperService.AppConfig.ActivePasswordResetRequest.Username = value.username;
          this.formForgotPassword.reset();
          this._HelperService.NotifySuccess(_Response.Message);
          this.stpper = 2;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      },
      ()=>{
        this.formProcessing = false;
      });
  }

  onOtpChange(otp: any) {
    this.AccessCode = otp;
    console.log("this.AccessCode", this.AccessCode)
  }
  public ResendCode(): void {
    this.formProcessing = true;
    var pData = {
      Task: "resendemailotp",
      EmailAddress: this._HelperService.AppConfig.ActivePasswordResetRequest.Username,
      Reference: this._HelperService.AppConfig.ActivePasswordResetRequest.Reference,
    };
    let _OResponse: Observable<OResponse>;

    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.MOnBoard, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      },
      ()=>{
        this.formProcessing = false;
      }
    );
  }
  verifyOtp(): void {
    this.formProcessing = true;
    var pData = {
      Task: "verifyemail",
      EmailAddress: this._HelperService.AppConfig.ActivePasswordResetRequest.Username,
      Reference: this._HelperService.AppConfig.ActivePasswordResetRequest.Reference,
      VerificationCode: this.AccessCode
    };
    let _OResponse: Observable<OResponse>;

    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.MOnBoard, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);
          this.stpper = 3;
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      },
      ()=>{
        this.formProcessing = false;
      }
    );
  }

  resetPassword(value: any): void {
    if(this.formResetPassword.valid){
      if (value.password != value.confirmPassword) {
        this._HelperService.NotifyError('Password does not match. Both passwords must be same to reset your password');
      }else{
        this.formProcessing = true;
        var pData = {
          Task: "updatepassword",
          EmailAddress: this._HelperService.AppConfig.ActivePasswordResetRequest.Username,
          Reference: this._HelperService.AppConfig.ActivePasswordResetRequest.Reference,
          NewPassword: value.password,
          ConfirmedPassword: value.password
        };
        let _OResponse: Observable<OResponse>;
    
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.MOnBoard, pData);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess(_Response.Message);
              this._Router.navigate(['/auth/login']);
            } else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.HandleException(_Error);
          },
          () => {
            this.formProcessing = false;
          }
        );
      }
    }
  }

  pwdValueChange(event: any) {
    let val = event.target.value;
    this.ispwdContainsNum = /\d/.test(val);
    this.ispwdContainsUC = /[A-Z]/.test(val);
    this.ispwdContainsLC = /[a-z]/.test(val);
    this.IsminLength = val.length >= 8;
    this.isPwdContainsSC = /(?=.*?[#?!@$%^&*-])/.test(val);
  }

  ToogleShowHidePassword(control: string): void {
    if (control == "password") {
      this.ShowPassword1 = !this.ShowPassword1;
    }
    else {
      this.ShowPassword2 = !this.ShowPassword2;
    }
  }

}
