
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Location } from '@angular/common';
import { HelperService } from 'src/app/utils/helper.service';
import { HCXAddress, OResponse } from 'src/app/interfaces';
// import * as cloneDeep from 'lodash/cloneDeep';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css', '../login/login.component.css']

})

export class RegisterComponent implements OnInit {

  interval: any;
  stepper: number = 1;
  forActive: boolean = true;
  forInActive: boolean = false;

  Form_AddUser_Processing = false;
  Form_AddDetails_Processing = false;
  Form_AddUser!: FormGroup;
  Form_AddDetails!: FormGroup;
  IsBusinessRegistered: boolean = true;

  ShowPassword: boolean = true;

  constructor(
    public _FormBuilder: FormBuilder,
    public _Router: Router,
    public _HelperService: HelperService,
    private location: Location
  ) {
    this.Form_AddUser_Load();
    this.Form_AddDetails_Load();
  }

  ispwdContainsNum = true
  ispwdContainsUC = true
  ispwdContainsLC = true
  IsminLength = true
  isPwdContainsSC = true


  pwdValueChange(val: any) {
    this.ispwdContainsNum = /\d/.test(val)
    this.ispwdContainsUC = /[A-Z]/.test(val)
    this.ispwdContainsLC = /[a-z]/.test(val)
    this.IsminLength = val.length >= 8
    this.isPwdContainsSC = /(?=.*?[#?!@$%^&*-])/.test(val)
  }

  index = 1;

  ngOnInit() {
    this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Account);
    this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Device);
    this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Location);
    this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.OReqH);
    this._HelperService.DeleteStorage("merchantDetails");
    this._HelperService.SetNetwork();
  }


  ToogleShowHidePassword(): void {
    this.ShowPassword = !this.ShowPassword;
  }

  formAction: boolean = true;

  SecondPage() {
    this.stepper = 2;
  }
  ToggleSectiontwo() {
    this.stepper = 1;
  }

  goBack() {
    if (this.stepper == 2) {
      this.stepper = 1;
    } else if (this.stepper == 1) {
      this.location.back();
    }
  }
  // business type code labels

  BussinessTypes: any[] = [
    { value: 'businesstype.individual', label: 'Individual' },
    { value: 'businesstype.company', label: 'Business Entity/Company' },
  ];

  Form_AddUser_Load() {

    this.Form_AddUser = this._FormBuilder.group({
      Task: "createaccount",
      AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      BusinessName: [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(128)])],
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(256), Validators.pattern("^[a-zA-Z _-]*$")])],
      LastName: [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(256), Validators.pattern("^[a-zA-Z _-]*$")])],
      selectedBusinessTypes: [],
      Password: [null, Validators.required],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2), Validators.pattern('^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$')])],
      MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(11), Validators.maxLength(11), Validators.pattern("^((\\+243-?)|0)?[0-9]{10,14}$")])], Source: 'Merchant',
      CountryIsd: ["234", Validators.required],
      Policy: [true, Validators.requiredTrue],
      newsletters: [true],
      Host: null
    })

  }
  UserDetails: any;

  Form_AddUser_Process(_FormValue: any) {

    let str = _FormValue.EmailAddress;
    let email_lowercase = "";
    for (let chars of str) {
      //Get the ascii value of character
      let value = chars.charCodeAt();
      //If the character is in uppercase
      if (value >= 65 && value <= 90) {
        //convert it to lowercase
        email_lowercase += String.fromCharCode(value + 32);
      } else {
        //else add the original character
        email_lowercase += chars;
      }
    }
    var _Request =
    {
      Task: 'onboardmerchantrequest',
      BusinessName: _FormValue.BusinessName,
      FirstName: _FormValue.FirstName,
      LastName: _FormValue.LastName,
      BusinessTypeCode: _FormValue.selectedBusinessTypes,
      EmailAddress: email_lowercase,
      MobileNumber: _FormValue.MobileNumber,
      Password: _FormValue.Password,
      CountryIsd: _FormValue.CountryIsd,
      Source: 'regsource.tucwebsite',
      SubscriptionKey: null,
      Host: this._HelperService.AppConfig.Host,
    }

    let checkBox = {
      IsPolicy: _FormValue.Policy == true ? "1" : "0",
      IsNewsLetter: _FormValue.newsletters == true ? "1" : "0"
    }

    this._HelperService.SaveStorage('t&c', checkBox);
    this.Form_AddUser_Processing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.MOnBoard, _Request);
    _OResponse.subscribe(
      _Response => {
        this.Form_AddUser_Processing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.DataReference, _Response.Result);
          this._HelperService.MerchantToken = _Response.Result.Reference;
          this.UserDetails = _Response.Result;
          this.stepper = 2;
          this._HelperService.NotifySuccess(_Response.Message);
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.Form_AddUser_Processing = false;
        this._HelperService.HandleException(_Error);
      });

  }

  // address
  AddressChange(Address: any) {
    this._Address = Address;
  }

  // file upload 
  imageChangedEvent: any = '';
  fileVal: any;
  NofileName: any;
  YesfileName: any;
  fileSize; any;
  fileChangeEvent(event: any, value: any): void {
    this.fileSize = event.target.files[0].size;
    this.imageChangedEvent = event.target.files;
    if (event.target.files.length > 0) {
      this.convertBaseFile(event.target.files[0]).then(res => {
        this.fileVal = this._HelperService.GetImageDetails(res);
      })
    }
    if (value == true) {
      this.YesfileName = this.imageChangedEvent[0].name;
    }
    else if (value == false) {
      this.NofileName = this.imageChangedEvent[0].name;
    }

  }

  convertBaseFile(file: any) {

    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    })

  }

  Form_AddDetails_Load() {

    this.Form_AddDetails = this._FormBuilder.group({
      Task: "createaccount",
      ReferenceId: [null, Validators.required],
      ReferenceKey: [null, Validators.required],
      IsBusinessRegistered: ['true', [Validators.required]],
      ReferralCode: null,
      ImageContent: this.fileVal,
    })

  }

  _Address: HCXAddress = {};

  Form_AddDetails_Process(_FormValue: any) {

    if (!this._Address.Address == undefined || this._Address.Address == null || this._Address.Address == "" || this._Address.CityId < 1) {
      this._HelperService.NotifyError("Please select location");
    }
    else if (this.fileVal == undefined || this.fileVal == null || this.fileVal == '') {
      this._HelperService.NotifyError("Please add the document/file");
    }
    else if (this.fileSize >= 500000) {
      this._HelperService.NotifyError('file size should be less than 500kb');
    }
    else {

      var _Request = {
        Task: 'updatemerchantdetails',
        ReferenceId: this.UserDetails.ReferenceId,
        ReferenceKey: this.UserDetails.ReferenceKey,
        StateId: this._Address.StateId,
        StateKey: this._Address.StateCode,
        StateName: this._Address.StateName,
        CityId: this._Address.CityId,
        CityKey: this._Address.CityCode,
        CityName: this._Address.CityName,
        Address: this._Address.Address,
        MapAddress: this._Address.MapAddress,
        Latitude: this._Address.Latitude,
        Longitude: this._Address.Longitude,
        PostalCode: this._Address.PostalCode,
        IsBusinessRegistered: _FormValue.IsBusinessRegistered,
        ReferralCode: _FormValue.ReferralCode,
        ImageContent: this.fileVal,
      }

      this.Form_AddDetails_Processing = true;
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.MOnBoard, _Request);
      _OResponse.subscribe(
        _Response => {
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.DataReference, _Response.Result);
            this._HelperService.MerchantToken = _Response.Result.Reference;
            this._HelperService.NotifySuccess(_Response.Message);
            this._Router.navigate([this._HelperService.AppConfig.Pages.System.Login]);
          }
          else {
            this._HelperService.NotifyError(_Response.Message);

          }
        },
        _Error => {
          this._HelperService.HandleException(_Error);
        }, () => {
          this.Form_AddDetails_Processing = false;
        }
      );

    }

  }


  ngOnDestroy() {
    clearInterval(this.interval);
  }


}












