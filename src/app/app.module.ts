import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexDirective } from './directives/index.directive';
import { IndexPipe } from './pipe/index.pipe';
import { HttpClientModule } from '@angular/common/http';

import { IconsModule } from './shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DataHelperService, HelperService, TranslaterModule } from './utils/index';
import { NgOtpInputModule } from  'ng-otp-input';
import { NgSelectModule } from '@ng-select/ng-select';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
@NgModule({
  declarations: [
    AppComponent,
    IndexDirective,
    IndexPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    IconsModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule,
    NgOtpInputModule,
    TranslaterModule,
    NgSelectModule,
    BsDatepickerModule.forRoot(),
    BrowserAnimationsModule,
  ],
  providers: [
    HelperService,
    DataHelperService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
