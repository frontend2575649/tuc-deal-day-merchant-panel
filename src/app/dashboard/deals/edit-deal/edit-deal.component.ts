import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';
import { OResponse } from 'src/app/interfaces';
import { HelperService, DataHelperService } from 'src/app/utils';
import * as moment from 'moment';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-edit-deal',
  templateUrl: './edit-deal.component.html',
  styleUrls: ['./edit-deal.component.css', '../create-deal/create-deal.component.css']
})
export class EditDealComponent implements OnInit {

  public ckConfig =
  {
      toolbar: ['Bold', 'Italic', 'NumberedList', 'BulletedList'],
      height: 300
  }
  public Editor = ClassicEditor;
  stepper:number = 1;
  Categories: any;
  DealForm: FormGroup;
  SubCategories: any;
  ImageList = [];
  isLoadingCat: boolean;
  StoresList: any;
  minDate = moment(new Date()).format('YYYY-MM-DD');
  maxDate = moment(new Date()).format('YYYY-MM-DD');
  public StoreListings: any = [];
  IsDealLoaded: boolean;
  public _DealDetails: any ={};
  dealType:any;
  isProduct:boolean = false;
  _Titles =
  {
      OriginalTitle: "",
      Title1: this._HelperService.UserCountry.CurrencyNotation + " X deal title for " + this._HelperService.UserCountry.CurrencyNotation + " Y",
      Title2: this._HelperService.UserCountry.CurrencyNotation + " Y off on deal title",
      Title3: "x% off on deal title"
  }
  _AmountDistribution =
  {
      ActualPrice: null,
      SellingPrice: null,
      SellingPricePercentage: null,
      SellingPriceDifference: null,
      TUCPercentage: null,
      TUCAmount: null,
      MerchantPercentage: null,
      MerchantAmount: null,
  };
  imageListn= [];
  ScheduledDays: any = []
  ScheduledDayPairs: any = [];
  scheduleDays = [
    {
      DayOfWeek:1,
      value:'S'
    },
    {
      DayOfWeek:2,
      value:'M'
    },{
      DayOfWeek:3,
      value:'T'
    },{
      DayOfWeek:4,
      value:'W'
    },{
      DayOfWeek:5,
      value:'T'
    },{
      DayOfWeek:6,
      value:'F'
    },
    {
      DayOfWeek:7,
      value:'S'
    },
  ]
  dealsLocation = []
  selectedCategory:any;
  selectedSubCat:any;
  imageDeets: { base64: string; extension: string; name: string; };
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    private http: HttpClient, 
  )
  {
    this.fetchCategory()
   }
  ngOnInit(): void {
    this.DealForm = this._FormBuilder.group({
      DealTypeCode:['', Validators.required],
      TitleContent:['', Validators.required],
      Description: ['', Validators.required],
      CategoryId:['', Validators.required],
      CategoryKey:['', Validators.required],
      SubCategoryId:['', Validators.required],
      SubCategoryKey:['', Validators.required],
      SubCategoryName:['', Validators.required],
      ActualPrice:['', Validators.required],
      SellingPrice:['', Validators.required],
      Title:['', Validators.required],
      Images:[this.ImageList],
      TitleTypeId:['', Validators.required],
      AccountKey:['', Validators.required],
      AccountId:['', Validators.required],
      //step 2
      IsAllLocations:[false],
      Shedule:[[]],
      isScheduled:[false],
      Weight: [''],
      SettlementTypeCode:[''],
      EndHour:[''],
      StartHour:[''],
      StartDate:[''],
      EndDate:[''],
      PackagingDescription: [''],
      StoreListing:[],
      //step:3
      CodeUsageTypeCode:[''],
      CodeValidityDays:[''],
      CodeValidityHours:[''],
      CodeValidityEndDate:[''],
      Terms:[''],
      Keywords:[''],
      SendNotification:[false],
      StatusCode:[''],
      MaximumUnitSalePerPerson:[''],
      MaximumUnitSale:[''],
      DeliveryTypeCode:['']
   })
   this.InitPage()
  }
  get DealControls() { return this.DealForm.controls; }
  fetchCategory()
  {
    this.isLoadingCat = true
    let body = {
      Task: "getrootcategories",
      TotalRecords: 0,
      Offset: 0,
      Limit: 1000,
      RefreshCount: true,
      SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
      SortExpression: "CreateDate desc",
      Type: "all",
      ReferenceKey: null,
      ReferenceId: 0,
      SubReferenceId: 0,
      IsDownload: false,
      Status: 0
    }
    let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData('api/v3/tuc/dealcategories/', body);
      _OResponse.subscribe(
          _Response => {
              this._HelperService.IsFormProcessing = false;
              if (_Response.Status == this._HelperService.StatusSuccess) {
                  this.Categories  =  _Response.Result.Data
                  this.isLoadingCat = false
              }
              else {
                  this._HelperService.NotifyError(_Response.Message);
              }
          },
          _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
      });
  }

fetchStores()
{
  let body = {
    Task: "getstores",
    Location: this._HelperService.AppConfig.NetworkLocation.V3.Store,
    StatusType: "default",
    SortExpression: "CreateDate desc",
    SearchCondition:  this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.AccountId, '='),
    AccountId: this._HelperService.AppConfig.AccountId,
    AccountKey: this._HelperService.AppConfig.AccountKey,
    IsDownload: false,
    Offset: 0,
    Limit: 100,
  }
  let _OResponse: Observable<OResponse>;
  _OResponse = this._HelperService.PostData(body.Location, body);
  _OResponse.subscribe(
      _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
             this.StoresList  =  _Response.Result.Data.filter(e => e.StatusCode == 'default.active')
             
              for (let index = 0; index < this.StoresList.length; index++) {
                  const element: any = this.StoresList[index];
                  this.StoreListings = [...this.StoreListings, {
                      id: element.ReferenceId,
                      key: element.ReferenceKey,
                      text: element.DisplayName
                  }];
              }
            this.DealForm.patchValue({
                StoreListing: this.dealsLocation.map((e) => e.id)
            })
            if(this.DealForm.value.StoreListing.length === this.StoreListings.length)
            {
              this.DealForm.patchValue({
                IsAllLocations: true
              })
            }
          }
          else {
              this._HelperService.NotifyError(_Response.Message);
          }
      },
      _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
  });
  
  }
  GetDeal() {
    this.IsDealLoaded = false;
    this._HelperService.IsFormProcessing = true;
    var pData = {
        Task: 'getdeal',
        ReferenceId: Number(this._HelperService.AppConfig.ActiveReferenceId),
        ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
        AccountId: this._HelperService.UserAccount.AccountId,
        AccountKey: this._HelperService.UserAccount.AccountKey,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, pData);
    _OResponse.subscribe(
        _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
                this.IsDealLoaded = true;
                this._HelperService.IsFormProcessing = false;
                this._DealDetails = _Response.Result;
                this.DealForm.controls['AccountKey'].setValue(this._DealDetails.AccountKey);
                this.DealForm.controls['AccountId'].setValue(this._DealDetails.AccountId);
                this.DealForm.controls['CategoryId'].setValue(this._DealDetails.CategoryId)
                this.DealForm.controls['CategoryKey'].setValue(this._DealDetails.CategoryKey)
                this.selectedCategory = {
                  CategoryId:this._DealDetails.CategoryId,
                  CategoryKey:this._DealDetails.CategoryKey,
                  CategoryName:this._DealDetails.CategoryName
                }
                this.fetchSubCat(this.selectedCategory,true)
                this.imageListn= this._DealDetails.Images;
                this.DealForm.controls['DealTypeCode'].setValue(this._DealDetails.DealTypeCode);
                this.DealForm.controls['DeliveryTypeCode'].setValue(this._DealDetails.DeliveryTypeCode)
                this.DealForm.controls['SubCategoryKey'].setValue(this._DealDetails.SubCategoryKey);
                this.DealForm.controls['SubCategoryId'].setValue(this._DealDetails.SubCategoryId);
                this.DealForm.controls['ActualPrice'].setValue(this._DealDetails.ActualPrice);
                this.DealForm.controls['SellingPrice'].setValue(this._DealDetails.SellingPrice);
                this._AmountDistribution =
                    {
                        ActualPrice: this._DealDetails.ActualPrice,
                        SellingPrice: this._DealDetails.SellingPrice,
                        SellingPricePercentage: this._DealDetails.DiscountPercentage,
                        SellingPriceDifference: this._DealDetails.DiscountAmount,
                        TUCPercentage: this._DealDetails.CommissionPercentage,
                        TUCAmount: this._DealDetails.CommissionAmount,
                        MerchantAmount: this._HelperService.
                          GetFixedDecimalNumber((this._DealDetails.SellingPrice - this._DealDetails.CommissionAmount)),
                        MerchantPercentage: this._HelperService.
                          GetPercentageFromNumber(this._DealDetails.MerchantAmount, this._DealDetails.SellingPrice),
                    };
                this.DealForm.controls['TitleTypeId'].setValue(this._DealDetails.TitleTypeId);
                this.DealForm.controls['TitleContent'].setValue(this._DealDetails.TitleContent);
                this.DealForm.controls['Title'].setValue(this._DealDetails?.TitleContent);
                this.DealForm.controls['Description'].setValue(this._DealDetails?.Description);
                this.DealForm.controls['PackagingDescription'].setValue(this._DealDetails.PackagingCategory?.ParcelDescription);
                this.DealForm.controls['Weight'].setValue(this._DealDetails.PackagingCategory?.Weight || 0)
                if(this._DealDetails.Locations.length < 0)
                {
                  this.DealForm.controls['IsAllLocations'].setValue(true)
                }else{
                  this.DealForm.controls['IsAllLocations'].setValue(false)
                  this._DealDetails.Locations.forEach(element => {
                    this.dealsLocation.push({
                      id:element.AccountId,
                      key:element.ReferenceKey,
                      text:element.DisplayName
                    })
                  })
                  this.fetchStores()
                }
                this.getImageData(this._DealDetails.Images)
                this.DealForm.controls['StartDate'].setValue(moment(this._DealDetails.StartDate).format('YYYY-MM-DD'))
                this.DealForm.controls['EndDate'].setValue(moment(this._DealDetails.EndDate).format('YYYY-MM-DD')) 
                this.DealForm.controls['Terms'].setValue(this._DealDetails.Terms);
                this.DealForm.controls['MaximumUnitSale'].setValue(this._DealDetails.MaximumUnitSale);
                this.DealForm.controls['MaximumUnitSalePerPerson'].setValue(this._DealDetails.MaximumUnitSalePerPerson);
                if(this._DealDetails?.Shedule?.length == 0){
                  this.DealForm.controls['isScheduled'].setValue(false)
                }
                if(this._DealDetails?.Shedule?.length > 0){
                  this.DealForm.controls['isScheduled'].setValue(false)
                  this.ScheduledDays =  this._DealDetails.Shedule
                  this.DealForm.controls['StartHour'].setValue(this._DealDetails.Shedule[0].StartHour)
                  this.DealForm.controls['EndHour'].setValue(this._DealDetails.Shedule[0].EndHour) 
                  this.ScheduledDays = this.ScheduledDays.map(e => e?.DayOfWeek)
                }
                this.DealForm.controls['SettlementTypeCode'].setValue(this._DealDetails.SettlmentTypeCode)
                this.DealForm.controls['CodeUsageTypeCode'].setValue(this._DealDetails.UsageTypeCode)
                this.DealForm.controls['CodeValidityDays'].setValue(this._DealDetails.CodeValidityDays)
                this.DealForm.controls['CodeValidityHours'].setValue(this._DealDetails.CodeValidityHours)
                this.DealForm.controls['CodeValidityEndDate'].setValue(this._DealDetails.CodeValidityEndDate)
                this.DealForm.controls['Keywords'].setValue(this._DealDetails.Keywords)
              this.ProcessAmounts();
            }}
            )
        }
  goHome(){
    this._Router.navigate(['/'])
  }
setDealType(data:string)
{
  if(data === 'service'){
    this.DealForm.get('Weight').setValue(0)
    this._DealDetails.DealTypeCode = 'dealtype.service' 
  }else{
     this._DealDetails.DealTypeCode = 'dealtype.product' 
  }
}
selectDealSubCategory(event?:any)
{
  let parsedSource = JSON.parse(event.target.value)
  this.DealForm.get('CategoryId').setValue(Number(parsedSource.RootCategoryId))
  this.DealForm.get('CategoryKey').setValue(parsedSource.RootCategoryKey)
  this.fetchSubCat(parsedSource)
}
setSubCat(data:any)
{
  let parsedSource = JSON.parse(data)
  this.fetchSubCat(parsedSource)
}
fetchSubCat(data:any, firstLoad?:boolean)
{
  this.isLoadingCat = true
  let body = {
    Task: "getdealcategories",
    TotalRecords: 0,
    Offset: 0,
    Limit: 100,
    RefreshCount: true,
    SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
    SortExpression: "CreateDate desc",
    Type: "all",
    ReferenceKey:this.DealForm.value.CategoryKey,
    ReferenceId: this.DealForm.value.CategoryId,
    SubReferenceId: 0,
    IsDownload: false,
    Status: 0
  }
  let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData('api/v3/tuc/dealcategories/', body);
      _OResponse.subscribe(
          _Response => {
              this._HelperService.IsFormProcessing = false;
              if (_Response.Status == this._HelperService.StatusSuccess) {
                this.SubCategories = _Response.Result.Data
                if(firstLoad){
                   this.selectedSubCat = {
                    SubCategoryId:this._DealDetails.SubAccountId,
                    SubCategoryKey: this._DealDetails.SubAccountKey,
                    SubCategoryName: this._DealDetails.SubCategoryName
                   }
                   this.DealForm.get('SubCategoryId').setValue(Number(this._DealDetails.SubCategoryId))
                   this.DealForm.get('SubCategoryKey').setValue(this._DealDetails.SubCategoryKey)
                   this.DealForm.get('SubCategoryName').setValue(this._DealDetails.SubCategoryName)
                }
                this.isLoadingCat = false
              }
              else {
                  this._HelperService.NotifyError(_Response.Message);
              }
          },
          _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
      });
}

Stringify(data:any)
{ 
  return JSON.stringify(data) 
}
setSubCategory(event:any)
{
  let parsedSource = JSON.parse(event.target.value)
  this.DealForm.get('SubCategoryId').setValue(Number(parsedSource.RootCategoryId))
  this.DealForm.get('SubCategoryKey').setValue(parsedSource.RootCategoryKey)
}

ProcessAmounts() {
  var tAmount = this._AmountDistribution;
  var SellingPrice = parseFloat(this._AmountDistribution.SellingPrice);
  var ActualPrice = parseFloat(this._AmountDistribution.ActualPrice);
  if (tAmount.ActualPrice != undefined && tAmount.ActualPrice != null && isNaN(tAmount.ActualPrice) == false && tAmount.ActualPrice >= 0) {
      if (tAmount.SellingPrice != undefined && tAmount.SellingPrice != null && isNaN(tAmount.SellingPrice) == false && tAmount.SellingPrice >= 0) {
          if (tAmount.ActualPrice >= tAmount.SellingPrice) {
              tAmount.SellingPriceDifference = this._HelperService.GetFixedDecimalNumber(tAmount.ActualPrice - tAmount.SellingPrice);
              tAmount.SellingPricePercentage = this._HelperService.GetPercentageFromNumber(tAmount.SellingPriceDifference, tAmount.ActualPrice);
              tAmount.TUCAmount = this._HelperService.GetAmountFromPercentage(tAmount.SellingPrice, tAmount.TUCPercentage);
              tAmount.MerchantAmount = this._HelperService.GetFixedDecimalNumber(tAmount.SellingPrice - tAmount.TUCAmount);
              tAmount.MerchantPercentage = this._HelperService.GetPercentageFromNumber(tAmount.MerchantAmount, tAmount.SellingPrice);
              this._AmountDistribution =
              {
                  ActualPrice: tAmount.ActualPrice,
                  SellingPrice: tAmount.SellingPrice,
                  SellingPricePercentage: tAmount.SellingPricePercentage,
                  SellingPriceDifference: tAmount.SellingPriceDifference,
                  TUCPercentage: this._AmountDistribution.TUCPercentage,
                  TUCAmount: tAmount.TUCAmount,
                  MerchantPercentage: tAmount.MerchantPercentage,
                  MerchantAmount: tAmount.MerchantAmount,
              };
          }
          else {
              if (parseFloat(this._AmountDistribution.SellingPrice) >= parseFloat(this._AmountDistribution.ActualPrice)) {
                  setTimeout(() => {
                      this._AmountDistribution =
                      {
                          ActualPrice: this._AmountDistribution.ActualPrice,
                          SellingPrice: null,
                          SellingPricePercentage: null,
                          SellingPriceDifference: null,
                          TUCPercentage: this._AmountDistribution.TUCPercentage,
                          TUCAmount: null,
                          MerchantPercentage: null,
                          MerchantAmount: null,
                      };
                  }, 200);
              }
          }
      }
  }
  this.ProcessTitle();
}

ProcessTitle() {
  this._Titles.Title1 = this._HelperService.UserCountry.CurrencyNotation + " " + 
      Math.round(this._AmountDistribution.ActualPrice) + " " + this._Titles.OriginalTitle + " for " + 
      this._HelperService.UserCountry.CurrencyNotation + " " + Math.round(this._AmountDistribution.SellingPrice);
  this._Titles.Title2 = this._HelperService.UserCountry.CurrencyNotation + " " + 
      Math.round(this._AmountDistribution.SellingPriceDifference) + " off on " + this._Titles.OriginalTitle;
  this._Titles.Title3 = Math.round(this._AmountDistribution.SellingPricePercentage) + "% off on " 
    + this._Titles.OriginalTitle;
}

fileChangeEvent(event: any): void {
  if(this.imageListn.length > 2){
    this._HelperService.NotifyError('Maximum of 3 images allowed')
  return 
  }
  let file = event.target.files[0];
  let accepted = [
    'image/png',
    'image/jpeg'
  ]
  if(!accepted.includes(file.type)){
    this._HelperService.NotifyError('Only PNG & JPG types allowed')
    return 
  }
  let base64result
  let reader:any = new FileReader();
  reader.onloadend = function() {
    base64result = reader.result;
  }
  reader.readAsDataURL(file);
  setTimeout(() => {
    this.imageListn.push({
        Name:file.name,
        Size:file.size,
        Extension: reader.result.split(';')[0].split('/')[1],
        Content: base64result.split(',')[1]
    })
  }, 900); 
}
removeImage(index: number = undefined) {
  if (index == undefined) {
    this._HelperService._Icon_Cropper_BaseImage = '';
    this._HelperService._Icon_Cropper_Image = '';
  } else {
    this.imageListn.splice(index, 1);
  }
}

NextActions(data?:string)
{
  switch(this.stepper)
  {
    case 1:
      this.stepper++
    break
    case 2:
      this.setDays()
      this.enforceValidations()
    break
    case 3:
      this.submitDeal(data)
    break
  } 
}
EvaluateForm(data:string)
{
  switch(data)
  {
    case 'next':
      this.NextActions()
    break
    case 'previous':
    break
    case 'draft':
    break
  }
}
checkLocations()
{
  this.DealForm.get('StoreListing').setValue([])
}
checkPeriod()
{
  this.ScheduledDays = []
}

countryChange(eevnt: any) {
}
setDays()
{
  this.ScheduledDays.forEach(element => {
    this.ScheduledDayPairs.push({
      DayOfWeek:element, 
      TypeCode:null, 
      EndHour: this.DealForm.value.EndHour || 0,
      StartHour:this.DealForm.value.EndHour || 24
    })
  });
  this.DealForm.get('Shedule').setValue(this.ScheduledDayPairs)
}

enforceValidations()
{
  switch(this.stepper)
  {
    case 1:
      this.stepper++ 
      this.activateNdValidation()
    break
    case 2:
     this.stepper++ 
     this.activatedRdValidation()
    break
    case 3:
    break
  }
}
activateNdValidation()
  {
    this.SetImages()
    this.DealForm.get('SettlementTypeCode').setValidators([Validators.required]);
    this.DealForm.get('EndDate').setValidators([Validators.required]);
    this.DealForm.get('StartDate').setValidators([Validators.required]);
    this.DealForm.updateValueAndValidity()
  }
  activatedRdValidation()
  {
    this.DealForm.get('CodeUsageTypeCode').setValidators([Validators.required]);
    this.DealForm.get('MaximumUnitSalePerPerson').setValidators([Validators.required]);
    this.DealForm.get('MaximumUnitSale').setValidators([Validators.required]);
    this.DealForm.get('Terms').setValidators([Validators.required]);
    this.DealForm.updateValueAndValidity()
  }

SetImages()
{
  this.imageListn.forEach(element => {
      this.ImageList.push({
        ImageContent:{
          Content:element.Content,
          Extension:element.Extension,
          Size:null,
          Name:element.Name,
        },
        IsDefault:element == this.imageListn[0] ? 1:0
      })
    });
}
submitDeal(data:string){
  this.isLoadingCat = true
    let body ={
      ...this.DealForm.value,
      Packaging:{
        Description:this.DealForm.value.Description,
        Weight:this.DealForm.value.Weight
      },
      Task:'updatedeals',
      StatusCode:data,
      StartDate: moment(this.DealForm.value.StartDate).format('YYYY-MM-DD HH:mm'),
      EndDate: moment(this.DealForm.value.EndDate).format('YYYY-MM-DD HH:mm'),
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      Shedule: this.ScheduledDayPairs,
      Images:this.ImageList,
      Locations:this.setStoreDetails(),
      ReferenceId:this._DealDetails.ReferenceId,
      ReferenceKey: this._DealDetails.ReferenceKey
    }
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(`${this._HelperService.AppConfig.NetworkLocation.V3.Deals}`, body);
    _OResponse.subscribe(
        _Response => {
          this.isLoadingCat = false
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) { 
                if(data == 'deal.draft'){
                  this._HelperService.NotifySuccess('Deal saved as draft.');
                  this._Router.navigate(['/'])
                }else{
                  this._HelperService.NotifySuccess('Deal sent for approval.');
                  this._Router.navigate(['/'])
                } 
            }else{
              this._HelperService.NotifyError('Unable to complete that action')
            }
        }
    )
  }

  setStoreDetails() {
    let readyStores = [];
    if(!this.StoreListings.length){
      return false
    }
      this.DealForm.value.StoreListing.forEach(formVal => {
          let catIndex = this.StoreListings.findIndex(e => e.id == formVal);
          if (catIndex > -1) {
            readyStores.push({
                  ReferenceId: this.StoreListings[catIndex].id,
                  ReferenceKey: this.StoreListings[catIndex].key
              })
          }
      });
    return readyStores;
  }

  ToggleDay(data:number)
{
  if(this.ScheduledDays.includes(data))
  {
    const index = this.ScheduledDays.indexOf(data);
    this.ScheduledDays.splice(index, 1);
  }else{
     this.ScheduledDays.push(data)
  }
}

InitPage()
{
    this._ActivatedRoute.params.subscribe((params: Params) => {
    this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
    this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];
    this._HelperService.AppConfig.ActiveAccountKey = params["accountkey"];
    this._HelperService.AppConfig.ActiveAccountId = params["accountid"];
    });
  this.GetDeal()
}

async getImageData(imageUrl: any){
    this.imageListn.forEach(element => {
       this.RetrieveImage(element.ImageUrl).then(data =>{
        this.imageDeets = data
      })
      setTimeout(() => {
           this.ImageList.push({
            ImageContent:{
              Content:this.imageDeets.base64.split(',')[1],
              Extension:this.imageDeets.extension,
              Size:null,
              Name:this.imageDeets.name,
            },
            IsDefault:element == this.imageListn[0] ? 1:0
          })
      }, 900);
       
  });
}

async RetrieveImage(data:string) :Promise<{ base64: string, extension: string, name: string }> 
{
  const response = await this.http.get(data, { responseType: 'blob' }).toPromise();
  // Convert blob to base64
  const reader = new FileReader();
  reader.readAsDataURL(response);
  const base64Promise = new Promise<string>((resolve, reject) => {
    reader.onload = () => resolve(reader.result as string);
    reader.onerror = error => reject(error);
  });
  const base64 = await base64Promise;
  // Get extension and name
  const extension = response.type.split('/')[1];
  const name = data.split('/').pop().split('.')[0];
  // Return result
  return { base64, extension, name };
}
skipPage(data:number){    
  if(this.stepper < data || !this.DealForm.valid)
    return this._HelperService.NotifyError('Kindly complete the fields before skipping the page')
   this.stepper = data
}


}


