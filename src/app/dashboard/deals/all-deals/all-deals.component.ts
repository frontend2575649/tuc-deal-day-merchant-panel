import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { OResponse } from 'src/app/interfaces';
import { HelperService, DataHelperService, PaginationService } from 'src/app/utils';

@Component({
  selector: 'app-all-deals',
  templateUrl: './all-deals.component.html',
  styleUrls: ['./all-deals.component.css']
})
export class AllDealsComponent implements OnInit {

TodayStartTime = this._HelperService.AppConfig.DefaultStartTimeAll;
TodayEndTime = this._HelperService.AppConfig.DefaultEndTimeToday;
_GetoverviewSummary: any;
public PageRecordLimit:number = 10;
public TotalRecords = 0;
public Offset = 0;
public activePage = 1;
public pager: any = {};
SearchParameter:string = '';
Limit:number = 10;
ListType: number = 3;
viewType = {
  ListType:3,
  Type:"running"
}
condition: string;
OrderData: any;
constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    private paged: PaginationService
)
{}
ngOnInit(): void {
  this.GetSalesOverview()
  this.GetRecords()
}

  GetSalesOverview() {
    this._HelperService.IsFormProcessing = true;
     let pData = {
      Task: 'getdealsoverview',
      StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
      EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
      AccountId: this._HelperService.AppConfig.AccountId,
      AccountKey: this._HelperService.AppConfig.AccountKey,
      StoreReferenceId: 0,
      StoreReferenceKey: null,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetoverviewSummary = _Response.Result as any;
          this._ChangeDetectorRef.detectChanges();
          return;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  GetRecords()
  {

    let body =   {
      Task: "getdeals",
      TotalRecords: 0,
      Offset: 0,
      Limit: 10,
      RefreshCount: true,
      SearchCondition: `AccountId == \"${this._HelperService.UserAccount.AccountId}\" `,
      SortExpression: "CreateDate desc",
      Type: this.viewType.Type,
      ReferenceKey: null,
      ReferenceId: 0,
      SubReferenceId: 0,
      ListType: this.viewType.ListType,
      IsDownload: false
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, body);
    _OResponse.subscribe(
        _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
                this.OrderData = _Response.Result.Data as any;
                this.TotalRecords = _Response.Result.TotalRecords;
                this.pager = this.paged.getPager(this.TotalRecords, this.activePage, this.PageRecordLimit);
            }
            else {
                this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
    }); 
  }

  setPage(page:number){
    if(page < 1 || page > this.pager.totalPages){
        return
    }
    this.pager = this.paged.getPager(this.TotalRecords, page, this.PageRecordLimit)
    this._HelperService.scrollTop()
    if(page < this.activePage)
    {
      this.Offset -= this.PageRecordLimit
    }else{
      this.Offset += this.PageRecordLimit
    }
    this.activePage = page;
    this.GetRecords()
  }

WatchChange(data:any)
{
 this.PageRecordLimit = data
 this.activePage = 1
 this._HelperService.scrollTop()
 this.Offset = 0;
 this.GetRecords()
}  
ListTypeChange(listType:number, action:string)
{
  this.viewType = {
    ListType:listType,
    Type:action
  }
  this.GetRecords()
}



}
