import swal from 'sweetalert2';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ChangeContext } from 'ng5-slider';
import { Observable } from 'rxjs/internal/Observable';
import { HelperService, OList, DataHelperService, OResponse, PaginationService } from '../../utils/index';
declare var $: any;
declare var moment: any;

@Component({
  selector: 'app-deals',
  templateUrl: './deals.component.html',
  styleUrls: ['./deals.component.css']
})
export class DealsComponent implements OnInit {
  @ViewChild('dealspreviewmodal', { static: true }) dealspreviewmodal: any;
  @ViewChild('pausedeal', { static: true }) pausedeal: any;
  public PageRecordLimit:number = 10;
  public TotalRecords = 0;
  public activePage = 1;
  public pager: any = {};
  viewType: {SearchCondition:string, ListType: number; Type?: string; };
  DealsData:any;
  Offset:number = 0;
  public _GetoverviewSummary: any = {};
  public TodayStartTime = null;
  public TodayEndTime = null;
  isRejected: boolean = false;

  RejectedDeal: number = 0
  DisableRejectedDiv: any;
  constructor(
    private modalService: NgbModal,
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    private paged: PaginationService
  ) { }

  _Preview = {
    ImageUrl: null,
    Title: null,
    Locations: null,
    SellingPrice: null,
    ActualPrice: null,
    AccountAddress: null,
  }

  PreviewDeal(Reference, content: any) {
    this._Preview = Reference;
    this.modalService.open(content, { backdrop: 'static' });
  }
  ngOnInit() {
    this._HelperService.StopClickPropogation();
    this.GetDealsOverview();
    this.setConditions('')
  }

 GetDealsOverview() {
    this._HelperService.IsFormProcessing = true;
     let pData = {
      Task: 'getdealsoverview',
      StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
      EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
      AccountId: this._HelperService.AppConfig.AccountId,
      AccountKey: this._HelperService.AppConfig.AccountKey,
      StoreReferenceId: 0,
      StoreReferenceKey: null,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetoverviewSummary = _Response.Result as any;
          this._ChangeDetectorRef.detectChanges();
          return;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
  GetRecords()
  {
    let body =   {
      Task: "getdeals",
      Offset: this.Offset,
      Limit: this.PageRecordLimit,
      TotalRecords:this.TotalRecords,
      RefreshCount: true,
      SearchCondition: this.viewType.SearchCondition,
      ListType: this.viewType.ListType,
      Type: this.viewType.Type,
      SortExpression: "CreateDate desc",
      ReferenceKey: null,
      ReferenceId: 0,
      SubReferenceId: 0,
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      IsDownload: false,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, body);
    _OResponse.subscribe(
        _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
                this.DealsData = _Response.Result.Data as any;
                this.TotalRecords = _Response.Result.TotalRecords;
                this.pager = this.paged.getPager(this.TotalRecords, this.activePage, this.PageRecordLimit);
            }
            else {
                this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
    }); 
  }


  setPage(page:number){
    if(page < 1 || page > this.pager.totalPages){
        return
    }
    this.pager = this.paged.getPager(this.TotalRecords, page, this.PageRecordLimit)
    this._HelperService.scrollTop()
    if(page < this.activePage)
    {
      this.Offset -= this.PageRecordLimit
    }else{
      this.Offset += this.PageRecordLimit
    }
    this.activePage = page;
    this.GetRecords()
  }

WatchChange(data:any)
{
 this.PageRecordLimit = data
 this.activePage = 1
 this._HelperService.scrollTop()
 this.Offset = 0;
 this.GetRecords()
}  

  ListTypeChange(action:string)
  {
    this.setConditions(action)
    this._HelperService.scrollTop()
  }

  setConditions(data){
    switch(data){
      case 'running':
          this.viewType = {
            SearchCondition: `AccountId == \"${this._HelperService.UserAccount.AccountId}\" AND TotalAvailable > 0`,
            ListType:3,
            Type:'running'     
          }
        break;
      case 'upcoming':
        this.viewType = {
          SearchCondition: `AccountId == \"${this._HelperService.UserAccount.AccountId}\" AND TotalAvailable > 0`,
          ListType:3,
          Type:'upcoming'     
        }
        break;
      case 'rejected':
        this.viewType = {
          SearchCondition: `AccountId == \"${this._HelperService.UserAccount.AccountId}\" AND StatusCode = \"deal.rejected\"`,
          ListType:4,
          Type:null     
        }
        break;
      case 'draft':
        this.viewType = {
          SearchCondition: `AccountId == \"${this._HelperService.UserAccount.AccountId}\" AND StatusCode = \"deal.draft\"`,
          ListType:5,
          Type:null     
        }
        break;
      case 'paused':
          this.viewType = {
            SearchCondition: `AccountId == \"${this._HelperService.UserAccount.AccountId}\" AND StatusCode = \"deal.paused\"`,
            ListType:4,
            Type:null     
          }
          break;
      case 'expired':
        this.viewType = {
          SearchCondition: `AccountId == \"${this._HelperService.UserAccount.AccountId}\" AND StatusCode = \"deal.expired\"`,
          ListType:6,
          Type:null     
        }
        break;
      case 'approvalpending':
        this.viewType = {
          SearchCondition: `AccountId == \"${this._HelperService.UserAccount.AccountId}\" AND StatusCode = \"deal.approvalpending\"`,
          ListType:1,
          Type:null     
        }
        break;
      case 'soldout':
        this.viewType = {
          SearchCondition: `AccountId == \"${this._HelperService.UserAccount.AccountId}\" AND TotalAvailable = \"0\"`,
          ListType:3,
          Type:'running'     
        }
        break;
      default:
        this.viewType = {
          SearchCondition: `AccountId == \"${this._HelperService.UserAccount.AccountId}\"`,
          ListType:3,
          Type:''     
        }
      break
    }
    this.GetRecords()
  }

  UpdateStatusArray: any = ['deal.draft', 'deal.approvalpending', 'deal.approved', 'deal.published', 'deal.paused', 'deal.expired',];
  pauseDeal(ReferenceData, i) {
    swal.fire({
      title: '<span style="font-weight: 700;font-size: 18px;">You are about to pause a deal, Do you wish to continue?</span>',
      text: 'You can activate your deal back anytime',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: '<span style="color: black;">No</span>',
      confirmButtonColor: '#B11A83',
      cancelButtonColor: '#FFFFFF',
    }).then((result) => {
      if (result.value) {
        this._HelperService.IsFormProcessing = true;
        var PData =
        {
          Task: this._HelperService.AppConfig.Api.ThankUCash.updatedealstatus,
          ReferenceId: ReferenceData.ReferenceId,
          ReferenceKey: ReferenceData.ReferenceKey,
          AccountId: ReferenceData.AccountId,
          AccountKey: ReferenceData.AccountKey,
          StatusCode: this.UpdateStatusArray[i],
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, PData);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Status Updated successfully. It will take upto 5 minutes to update changes.");
              this.GetRecords();
              this.GetDealsOverview();
              this._HelperService.IsFormProcessing = false;
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
            this._HelperService.ToggleField = false;
          });
      }
    });
  }

  DeleteDeal(ReferenceData: any) {

    swal.fire({
      title: '<span style="font-weight: 700;font-size: 18px;">You are about to delete a deal, Do you wish to continue?</span>',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: '<span style="color: black;">No</span>',
      confirmButtonColor: '#B11A83',
      cancelButtonColor: '#FFFFFF',
    }).then((result) => {
      if (result.value) {
        this._HelperService.IsFormProcessing = true;
        var PData =
        {
          Task: this._HelperService.AppConfig.Api.ThankUCash.deletedeal,
          ReferenceId: ReferenceData.ReferenceId,
          ReferenceKey: ReferenceData.ReferenceKey,
          AccountId: ReferenceData.AccountId,
          AccountKey: ReferenceData.AccountKey,
        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, PData);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Status Updated successfully. It will take upto 5 minutes to update changes.");
              this.GetRecords();
              this.GetDealsOverview();
              this._HelperService.IsFormProcessing = false;
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
            this._HelperService.ToggleField = false;
          });
      }
    });
  }

  EditDeal(data:any):void
  {
    this._Router.navigate([
      'deals/editdeal',
      data.ReferenceKey,
      data.ReferenceId,
      data.AccountId,
      data.AccountKey,
    ]);
  }

}



