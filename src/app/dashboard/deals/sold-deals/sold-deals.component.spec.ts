import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SoldDealsComponent } from './sold-deals.component';

describe('SoldDealsComponent', () => {
  let component: SoldDealsComponent;
  let fixture: ComponentFixture<SoldDealsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SoldDealsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SoldDealsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
