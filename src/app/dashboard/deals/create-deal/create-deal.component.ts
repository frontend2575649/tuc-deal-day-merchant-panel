import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { OResponse } from 'src/app/interfaces';
import { HelperService, DataHelperService } from 'src/app/utils';
import * as moment from 'moment';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';


@Component({
  selector: 'app-create-deal',
  templateUrl: './create-deal.component.html',
  styleUrls: ['./create-deal.component.css']
})
export class CreateDealComponent implements OnInit {
Categories: any;
  DealForm: FormGroup;
  SubCategories: any;
  format = 'DD.MM.YYYY HH:mm';
  public ckConfig =
  {
      toolbar: ['Bold', 'Italic', 'NumberedList', 'BulletedList'],
      height: 300
  }
  public Editor = ClassicEditor;
  _Titles =
  {
      OriginalTitle: "",
      Title1: this._HelperService.UserCountry.CurrencyNotation + " X deal title for " + this._HelperService.UserCountry.CurrencyNotation + " Y",
      Title2: this._HelperService.UserCountry.CurrencyNotation + " Y off on deal title",
      Title3: "x% off on deal title"
  }
  _AmountDistribution =
  {
      ActualPrice: null,
      SellingPrice: null,
      SellingPricePercentage: null,
      SellingPriceDifference: null,
      TUCPercentage: null,
      TUCAmount: null,
      MerchantPercentage: null,
      MerchantAmount: null,
  };
  imageChangedEvent: any = '';
  croppedImage: any = '';
  _DealConfig =
  {
      DefaultStartDate: null,
      DefaultEndDate: null,
      SelectedDealCodeHours: 12,
      SelectedDealCodeDays: 30,
      SelectedDealCodeEndDate: 0,
      DealCodeValidityTypeCode: 'daysafterpurchase',
      StartDate: null,
      EndDate: null,
      DealImages: [],
      SelectedTitle: 3,
      Images: [],
      StartDateConfig: {
      },
      EndDateConfig: {
      },
      DealCodeEndDateConfig: {
      }
  }
  stepper:number = 1;
  storeLocations= []
  StoresList: any;
  selCountries = [];
  ScheduledDays: any = []
  ImageList = []
  isLoadingCat:boolean;
  imageListn = []
  scheduleDays = [
    {
      key:1,
      value:'S'
    },
    {
      key:2,
      value:'M'
    },{
      key:3,
      value:'T'
    },{
      key:4,
      value:'W'
    },{
      key:5,
      value:'T'
    },{
      key:6,
      value:'F'
    },
    {
      key:7,
      value:'S'
    },
  ]
  ScheduledDayPairs = []
  minDate = moment(new Date()).format('YYYY-MM-DD');
  maxDate = moment(new Date()).format('YYYY-MM-DD');
  public StoreListings: any = [];
  actionText:string = 'Next'
  isLoadingNext:boolean;
  selectedCategory:string = 'Select Category';
  selectedSubCategory:string = 'Select Sub Category'
constructor(
  public _Router: Router,
  public _ActivatedRoute: ActivatedRoute,
  public _FormBuilder: FormBuilder,
  public _HelperService: HelperService,
  public _DataHelperService: DataHelperService,
  public _ChangeDetectorRef: ChangeDetectorRef,
)
{

}
ngOnInit(): void {
  
  this.fetchCategory()
  this.fetchStores()
  this.DealForm = this._FormBuilder.group({
    DealTypeCode:['', Validators.required],
    TitleContent:['', Validators.required],
    Description: ['', Validators.required],
    CategoryId:['', Validators.required],
    CategoryKey:['', Validators.required],
    CategoryName:[''],
    SubCategoryId:['', Validators.required],
    SubCategoryKey:['', Validators.required],
    ActualPrice:['', Validators.required],
    SellingPrice:['', Validators.required],
    Title:['', Validators.required],
    Images:[this.ImageList],
    TitleTypeId:[''],
    //step 2
    IsAllLocations:[false],
    Shedule:[[]],
    isScheduled:[''],
    Weight: [''],
    SettlementTypeCode:[''],
    EndHour:[''],
    StartHour:[''],
    StartDate:[''],
    EndDate:[''],
    PackagingDescription: [''],
    StoreListing:[[]],
    //step:3
    CodeUsageTypeCode:[''],
    CodeValidityDays:[''],
    CodeValidityHours:[''],
    CodeValidityEndDate:[''],
    Terms:[''],
    Keywords:[''],
    SendNotification:[false],
    StatusCode:[''],
    MaximumUnitSalePerPerson:[''],
    MaximumUnitSale:[''],
    DeliveryTypeCode:['']
  })
}

adjustDate()
{
  this.maxDate = this.DealForm.value.StartDate;
}
watchKeyPress(event)
{
    const pat = /^-?(\d+\.?\d*)$|(\d*\.?\d+)$/;
    if (!pat.test(event.target.value)) {
      return (event.target.value = "");
    }
  return this._HelperService.PreventTextButAcceptDecimal(event)
}

ProcessAmounts(event:any) {
  var tAmount = this._AmountDistribution;
  var SellingPrice = parseFloat(this._AmountDistribution.SellingPrice);
  var ActualPrice = parseFloat(this._AmountDistribution.ActualPrice);
  if (tAmount.ActualPrice != undefined && tAmount.ActualPrice != null && isNaN(tAmount.ActualPrice) == false && tAmount.ActualPrice >= 0) {
      if (tAmount.SellingPrice != undefined && tAmount.SellingPrice != null && isNaN(tAmount.SellingPrice) == false && tAmount.SellingPrice >= 0) {
          if (tAmount.ActualPrice >= tAmount.SellingPrice) {
              tAmount.SellingPriceDifference = this._HelperService.GetFixedDecimalNumber(tAmount.ActualPrice - tAmount.SellingPrice);
              tAmount.SellingPricePercentage = this._HelperService.GetPercentageFromNumber(tAmount.SellingPriceDifference, tAmount.ActualPrice);
              tAmount.TUCAmount = this._HelperService.GetAmountFromPercentage(tAmount.SellingPrice, tAmount.TUCPercentage);
              tAmount.MerchantAmount = this._HelperService.GetFixedDecimalNumber(tAmount.SellingPrice - tAmount.TUCAmount);
              tAmount.MerchantPercentage = this._HelperService.GetPercentageFromNumber(tAmount.MerchantAmount, tAmount.SellingPrice);
              this._AmountDistribution =
              {
                  ActualPrice: tAmount.ActualPrice,
                  SellingPrice: tAmount.SellingPrice,
                  SellingPricePercentage: tAmount.SellingPricePercentage,
                  SellingPriceDifference: tAmount.SellingPriceDifference,
                  TUCPercentage: this._AmountDistribution.TUCPercentage,
                  TUCAmount: tAmount.TUCAmount,
                  MerchantPercentage: tAmount.MerchantPercentage,
                  MerchantAmount: tAmount.MerchantAmount,
              };
          }
          else {
              if (parseFloat(this._AmountDistribution.SellingPrice) >= parseFloat(this._AmountDistribution.ActualPrice)) {
                  setTimeout(() => {
                      this._AmountDistribution =
                      {
                          ActualPrice: this._AmountDistribution.ActualPrice,
                          SellingPrice: null,
                          SellingPricePercentage: null,
                          SellingPriceDifference: null,
                          TUCPercentage: this._AmountDistribution.TUCPercentage,
                          TUCAmount: null,
                          MerchantPercentage: null,
                          MerchantAmount: null,
                      };
                  }, 200);
              }
          }
      }
  }
  this.ProcessTitle();
}

ProcessTitle() {
  this._Titles.Title1 = this._HelperService.UserCountry.CurrencyNotation + " " + Math.round(this._AmountDistribution.ActualPrice) + " " + this._Titles.OriginalTitle + " for " + this._HelperService.UserCountry.CurrencyNotation + " " + Math.round(this._AmountDistribution.SellingPrice);
  this._Titles.Title2 = this._HelperService.UserCountry.CurrencyNotation + " " + Math.round(this._AmountDistribution.SellingPriceDifference) + " off on " + this._Titles.OriginalTitle;
  this._Titles.Title3 = Math.round(this._AmountDistribution.SellingPricePercentage) + "% off on " + this._Titles.OriginalTitle;
}
countryChange(eevnt: any) {
}
fetchStores()
{
  let body = {
    Task: "getstores",
    SortExpression: "CreateDate desc",
    SearchCondition: this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.AccountId, '='),
    AccountId: this._HelperService.AppConfig.AccountId,
    AccountKey: this._HelperService.AppConfig.AccountKey,
    IsDownload: false,
    Offset: 0,
    Limit: 100,
  }

  let _OResponse: Observable<OResponse>;
  _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Store, body);
  _OResponse.subscribe(
      _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
             this.StoresList  =  _Response.Result.Data.filter(e => e.StatusCode == 'default.active')
              for (let index = 0; index < this.StoresList.length; index++) {
                  const element: any = this.StoresList[index];
                  this.StoreListings = [...this.StoreListings, {
                      id: element.ReferenceId,
                      key: element.ReferenceKey,
                      text: element.DisplayName
                  }];
              }
          }
          else {
              this._HelperService.NotifyError(_Response.Message);
          }
      },
      _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
  });
  
}

get DealControls() { return this.DealForm.controls; }

fetchCategory()
  {
    this.isLoadingCat = true
    let body = {
      Task: "getrootcategories",
      TotalRecords: 0,
      Offset: 0,
      Limit: 1000,
      RefreshCount: true,
      SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
      SortExpression: "CreateDate desc",
      Type: "all",
      ReferenceKey: null,
      ReferenceId: 0,
      SubReferenceId: 0,
      IsDownload: false,
      Status: 0
    }
    let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData('api/v3/tuc/dealcategories/', body);
      _OResponse.subscribe(
          _Response => {
              this._HelperService.IsFormProcessing = false;
              if (_Response.Status == this._HelperService.StatusSuccess) {
               this.Categories  =  _Response.Result.Data
                this.isLoadingCat = false
              }
              else {
                  this._HelperService.NotifyError(_Response.Message);
              }
          },
          _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
      });
  }

selectDealSubCategory(event?:any)
{
  let parsedSource = JSON.parse(event.target.value)
  this.DealForm.get('CategoryId').setValue(Number(parsedSource.RootCategoryId))
  this.DealForm.get('CategoryKey').setValue(parsedSource.RootCategoryKey)
  this.DealForm.get('CategoryName').setValue(parsedSource.Name)
  this.selectedCategory = parsedSource.Name
  this.isLoadingCat = true
  let body = {
    Task: "getdealcategories",
    TotalRecords: 0,
    Offset: 0,
    Limit: 100,
    RefreshCount: true,
    SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
    SortExpression: "CreateDate desc",
    Type: "all",
    ReferenceKey:this.DealForm.value.CategoryKey,
    ReferenceId: this.DealForm.value.CategoryId,
    SubReferenceId: 0,
    IsDownload: false,
    Status: 0
  }
  let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData('api/v3/tuc/dealcategories/', body);
      _OResponse.subscribe(
          _Response => {
              this._HelperService.IsFormProcessing = false;
              if (_Response.Status == this._HelperService.StatusSuccess) {
                this.SubCategories  =  _Response.Result.Data
                  this.isLoadingCat = false
              }
              else {
                  this._HelperService.NotifyError(_Response.Message);
              }
          },
          _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
      });
}
setSubCategory(event:any)
{
  let parsedSource = JSON.parse(event.target.value)
  this.DealForm.get('SubCategoryId').setValue(Number(parsedSource.RootCategoryId))
  this.DealForm.get('SubCategoryKey').setValue(parsedSource.RootCategoryKey)
  this.selectedSubCategory = parsedSource.RootCategoryName
}

fileChangeEvent(event: any): void {
  if(this.imageListn.length > 1){
    this._HelperService.NotifyError('Maximum of 2 images allowed')
  return 
  }
  let file = event.target.files[0];
  let accepted = [
    'image/png',
    'image/jpeg'
  ]
  if(!accepted.includes(file.type)){
    this._HelperService.NotifyError('Only PNG & JPG types allowed')
    return 
  }
  if(file.size > 400000){
    this._HelperService.NotifyError('Maximum size is 400kb')
    return 
  }
  let base64result
  let reader:any = new FileReader();
  reader.onloadend = function() {
    base64result = reader.result;
  }
  reader.readAsDataURL(file);
  setTimeout(() => {
    this.imageListn.push({
        base64:base64result,
        Name:file.name,
        Size:file.size,
        Extension: reader.result.split(';')[0].split('/')[1],
        Content: base64result.split(',')[1],
        OriginalContent:base64result
    })
  }, 900); 
}
removeImage(index: number = undefined) {
  if (index == undefined) {
    this.imageListn = [];
  } else {
    this.imageListn.splice(index, 1);
  }
}
Icon_B64CroppedDone(){
  this._HelperService._Icon_Cropper_Data;
  this._HelperService._Multiple_Image_Data.push({ 
    OriginalContent: this._HelperService._Icon_Cropper_Image,
    ...this._HelperService._Icon_Cropper_Data})
}
drop(ev: any) {
  ev.preventDefault();
  let file: any;
  if (ev.dataTransfer.items) {
      let item = ev.dataTransfer.items[0];
      if (item.kind === 'file') {
          file = item.getAsFile();
      }
  } else {
      file = ev.dataTransfer.files[0];
  }
  this._HelperService._SetFirstImageOrNone([file]);
}

EvaluateForm(data:string)
{
  switch(data)
  {
    case 'next':
      this.NextActions()
    break
    case 'previous':

    break
    case 'draft':

    break
  }
}
dealType(data:string)
{
  if(data === 'service'){
    this.DealForm.get('Weight').setValue(0)
  }
}

NextActions(data?:string)
{
  switch(this.stepper)
  {
    case 1:
      this.enforceValidations()
    break
    case 2:
      this.setDays()
      this.enforceValidations()
    break
    case 3:
      this.submitDeal(data)
    break
  } 
}
Stringify(data:any)
{ 
  return JSON.stringify(data) 
}

checkLocations()
{
  let isAllLocs = this.DealForm.get('IsAllLocations').value
  this.storeLocations = []
  this.DealForm.get('StoreListing').setValue([])
}
checkPeriod()
{
  this.ScheduledDays = []
}

SetImages()
{
  this.imageListn.forEach(element => {
      this.ImageList.push({
        ImageContent:{
          Content:element.Content,
          Extension:element.Extension,
          Size:null,
          Name:element.Name,
        },
        IsDefault:element == this.imageListn[0] ? 1:0
      })
    });
}

ToggleDay(data:number)
{
  if(this.ScheduledDays.includes(data))
  {
    const index = this.ScheduledDays.indexOf(data);
    this.ScheduledDays.splice(index, 1);
  }else{
     this.ScheduledDays.push(data)
  }
}

setDays()
{
  this.ScheduledDays.forEach(element => {
    this.ScheduledDayPairs.push({
      DayOfWeek:element, 
      TypeCode:null, 
      EndHour: this.DealForm.value.EndHour || 0,
      StartHour:this.DealForm.value.EndHour || 24
    })
  });
  this.DealForm.get('Shedule').setValue(this.ScheduledDayPairs)
}

setStoreDetails() {
  let readyStores = [];
  if(!this.StoreListings.length){
    return false
  }
    this.DealForm.value.StoreListing.forEach(formVal => {
        let catIndex = this.StoreListings.findIndex(e => e.id == formVal);
        if (catIndex > -1) {
          readyStores.push({
                ReferenceId: this.StoreListings[catIndex].id,
                ReferenceKey: this.StoreListings[catIndex].key
            })
        }
    });
  return readyStores;
}

submitDeal(data:string){
  this.isLoadingNext = true
    let body ={
      ...this.DealForm.value,
      Packaging:{
        Description:this.DealForm.value.Description,
        Weight:this.DealForm.value.Weight
      },
      Task:'createDeal',
      StatusCode:data,
      StartDate: moment(this.DealForm.value.StartDate).format('YYYY-MM-DD HH:mm'),
      EndDate: moment(this.DealForm.value.EndDate).format('YYYY-MM-DD HH:mm'),
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      Shedule: this.ScheduledDayPairs,
      Images:this.ImageList,
      Locations:this.setStoreDetails()
    }
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, body);
    _OResponse.subscribe(
        _Response => {
          this.isLoadingNext = false
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) { 
                if(data == 'deal.draft'){
                  this._HelperService.NotifySuccess('Deal saved as draft.');
                  this._Router.navigate(['/'])
                }else{
                  this._HelperService.NotifySuccess('Deal sent for approval.');
                  this._Router.navigate(['/'])
                } 
            }else{
              this._HelperService.NotifyError(`Unable to complete that action ${_Response.Message}`)
            }
        }
    )
  }

  SetDeal(data:string)
  {
    this.DealForm.get('TitleTypeId').setValue(data)
  }

  ClearImages()
  {
    this._HelperService.NotifyInfo('Images removed!')
    this.imageListn = []
  }

  goHome()
  {
    this._Router.navigate(['/'])
  }

  enforceValidations()
  {
    switch(this.stepper)
    {
      case 1:
        this.stepper++ 
        this.activateNdValidation()
      break
      case 2:
       this.stepper++ 
       this.activatedRdValidation()
      break
      case 3:
      break
    }
  }

activateNdValidation()
  {
    this.SetImages()
    this.DealForm.get('SettlementTypeCode').setValidators([Validators.required]);
    this.DealForm.get('EndDate').setValidators([Validators.required]);
    this.DealForm.get('StartDate').setValidators([Validators.required]);
    this.DealForm.updateValueAndValidity()
  }
  activatedRdValidation()
  {
    this.actionText = 'Publish';
    this.DealForm.get('CodeUsageTypeCode').setValidators([Validators.required]);
    this.DealForm.get('MaximumUnitSalePerPerson').setValidators([Validators.required]);
    this.DealForm.get('MaximumUnitSale').setValidators([Validators.required]);
    this.DealForm.get('Terms').setValidators([Validators.required]);
    this.DealForm.updateValueAndValidity()
  }

  skipPage(data:number){    
    if(this.stepper < data || !this.DealForm.valid)
      return this._HelperService.NotifyError('Kindly complete the fields before skipping the page')
     this.stepper = data
  }
}
