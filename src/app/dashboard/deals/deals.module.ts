import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DealsComponent } from './deals.component';
import { RouterModule } from '@angular/router';
import { IconsModule } from 'src/app/shared';
import { CreateDealComponent } from './create-deal/create-deal.component';
import { NgxPaginationModule } from "ngx-pagination";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SoldDealsComponent } from './sold-deals/sold-deals.component';
import { RedeemedDealsComponent } from './redeemed-deals/redeemed-deals.component';
import { AllDealsComponent } from './all-deals/all-deals.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgSelectModule } from '@ng-select/ng-select';
import { EditDealComponent } from './edit-deal/edit-deal.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

const DealRoutes = [
  {
    path:'',
    component:DealsComponent,
  },
  {
    path:'add',
    component:CreateDealComponent,
  },
  {
    path:'sold-history',
    component:SoldDealsComponent,
  },
  {
    path:'redeemed-deals',
    component:RedeemedDealsComponent,
  },
  {
    path:'all-deals',
    component:AllDealsComponent,
  },
  {
    path:'editdeal/:referencekey/:referenceid/:accountid/:accountkey',
    component:EditDealComponent,
  },
]

@NgModule({
  declarations: [DealsComponent, CreateDealComponent, SoldDealsComponent, RedeemedDealsComponent, AllDealsComponent, EditDealComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    NgxPaginationModule,
    RouterModule.forChild(DealRoutes),
    IconsModule,
    ImageCropperModule,
    NgSelectModule,
    CKEditorModule
  ]
})
export class DealsModule { }
