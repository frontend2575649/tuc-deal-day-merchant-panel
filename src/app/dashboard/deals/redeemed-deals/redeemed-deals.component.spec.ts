import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RedeemedDealsComponent } from './redeemed-deals.component';

describe('RedeemedDealsComponent', () => {
  let component: RedeemedDealsComponent;
  let fixture: ComponentFixture<RedeemedDealsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RedeemedDealsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RedeemedDealsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
