import { Component, ChangeDetectorRef, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { OResponse } from 'src/app/interfaces';
import { HelperService, DataHelperService, PaginationService } from 'src/app/utils';

@Component({
  selector: 'app-redeemed-deals',
  templateUrl: './redeemed-deals.component.html',
  styleUrls: ['./redeemed-deals.component.css']
})
export class RedeemedDealsComponent implements OnInit {

  _GetoverviewSummary: any;
  public PageRecordLimit:number = 10;
  OrderData: any;
  public TotalRecords = 0;
  public Offset = 0;
  public activePage = 1;
  public pager: any = {};
  SearchParameter:string = '';
  Limit:number = 10;
  ListType: number = 1;
  condition: string;
    
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    private paged: PaginationService
  )
  {}
  ngOnInit(): void {
    this.GetOverview()
    this.FetchOrders()
  }

  GetOverview()
  {
    let body = {
      Task: "getpurchasehistoryoverview",
      TotalRecords: 0,
      Offset: 0,
      Limit: 10,
      RefreshCount: true,
      SearchCondition: `MerchantId = \"${this._HelperService.UserAccount.AccountId}\" AND StatusCode = \"dealcode.used\"`,
      SortExpression: "CreateDate desc",
      Type: null,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SubReferenceId: 0,
      IsDownload: false
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, body);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetoverviewSummary = _Response.Result as any;
          this._ChangeDetectorRef.detectChanges();
          return;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  FetchOrders()
  {
    let body = {
      Task: "getpurchasehistory",
      TotalRecords: 0,
      Offset: 0,
      Limit: this.PageRecordLimit,
      RefreshCount: true,
      SearchCondition: `MerchantId = \"${this._HelperService.UserAccount.AccountId}\" AND StatusCode = \"dealcode.used\"  `,
      SortExpression: "CreateDate desc",
      Type: null,
      ReferenceKey: null,
      ReferenceId: 0,
      SubReferenceId: 0,
      IsDownload: false
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, body);
    _OResponse.subscribe(
        _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
                this.OrderData = _Response.Result.Data as any;
                this.TotalRecords = _Response.Result.TotalRecords;
                this.pager = this.paged.getPager(this.TotalRecords, this.activePage, this.PageRecordLimit);
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
    });
  }

  setPage(page:number){
    this.activePage = page;
    if(page < 1 || page > this.pager.totalPages){
        return
    }
    this.pager = this.paged.getPager(this.TotalRecords, page, this.PageRecordLimit)
    this.FetchOrders()
  }
  WatchChange(data:any)
  {
   this.PageRecordLimit= data
    this.FetchOrders()
  }

}
