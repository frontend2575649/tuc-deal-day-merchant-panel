import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { IconsModule } from '../../shared/index';
import { SettingsComponent } from './settings.component';
import { SecurityTabComponent } from './security-tab/security-tab.component';
import { ProfileTabComponent } from './profile-tab/profile-tab.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GoogleMapsModule } from '@angular/google-maps';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { NgSelectModule } from '@ng-select/ng-select';
import { ChangePasswordModalComponent } from './change-password-modal/change-password-modal.component';

const routes: Routes = [
    {
        path: '',
        component: SettingsComponent,
    }
]
@NgModule({
    declarations: [
        SettingsComponent,
        SecurityTabComponent,
        ProfileTabComponent,
        ChangePasswordModalComponent
    ],
    imports: [
        CommonModule,
        IconsModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),
        GooglePlaceModule,
        GoogleMapsModule,
        NgSelectModule
    ]
})

export class SettingsModule { }