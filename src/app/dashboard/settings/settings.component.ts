import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { OResponse } from "src/app/interfaces";
import { HelperService } from "src/app/utils";

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.css'],
})

export class SettingsComponent implements OnInit {
   

    isLoaded: boolean = false;
    constructor(public _HelperService: HelperService) { }
    
    ngOnInit(): void {
    }
   
}