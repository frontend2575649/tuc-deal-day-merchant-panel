import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { HelperService, OResponse } from 'src/app/utils';

@Component({
  selector: 'app-profile-tab',
  templateUrl: './profile-tab.component.html',
  styleUrls: ['./profile-tab.component.css']
})
export class ProfileTabComponent implements OnInit {
  public _MerchantDetails: any = {
    ReferenceId: null,
    ReferenceKey: null,
    TypeCode: null,
    TypeName: null,
    SubTypeCode: null,
    SubTypeName: null,
    UserAccountKey: null,
    UserAccountDisplayName: null,
    Name: null,
    DisplayName: null,
    Description: null,
    StartDate: null,
    StartDateS: null,
    EndDate: null,
    EndDateS: null,
    SubTypeValue: null,
    MinimumInvoiceAmount: null,
    MaximumInvoiceAmount: null,
    MinimumRewardAmount: null,
    MaximumRewardAmount: null,
    ManagerKey: null,
    ManagerDisplayName: null,
    SmsText: null,
    Comment: null,
    CreateDate: null,
    CreatedByKey: null,
    CreatedByDisplayName: null,
    ModifyDate: null,
    ModifyByKey: null,
    ModifyByDisplayName: null,
    StatusId: null,
    StatusCode: null,
    StatusName: null,
    Latitude: null,
    Longitude: null,
    CreateDateS: null,
    ModifyDateS: null,
    StatusI: null,
    StatusB: null,
    StatusC: null,
    WebsiteUrl: null
  };

  public MerchantContactPerson: any = {};
  public BusinessInfoEditable: boolean = false;
  public ContactInfoEditable: boolean = false;
  public BusinessCategories: any = [];
  public S2BusinessCategories: any = [];
  public ShowCategorySelector: boolean = false;
  MapAddress: any = '';
  center: any = { lat: 9.0338725, lng: 8.677457 };
  zoom = 12;
  options: google.maps.MapOptions = {
    center: { lat: 9.0338725, lng: 8.677457 },
    zoom: 12,
    fullscreenControl: false,
    streetViewControl: false,
    mapTypeControl: false,
    panControl: true
  };
  MerchantAddress: any = {
    Address: null,
    Latitude: null,
    Longitude: null,
    CityAreaId: null,
    CityAreaCode: null,
    CityAreaName: null,
    CityId: null,
    CityCode: null,
    CityName: null,
    StateId: null,
    StateCode: null,
    StateName: null,
    CountryId: null,
    CountryCode: null,
    CountryName: null,
    PostalCode: null,
    MapAddress: null,
  }
  markerOptions: any = { draggable: false };
  markerPositions: any[] = [];
  markerPosition: any;
  loadingAddressData: boolean = false;
  constructor(public _HelperService: HelperService) { }
  ngOnInit(): void {
    this.GetBusinessCategories();
    this.GetMerchantDetails();
  }
  GetBusinessCategories() {
    this._HelperService.IsFormProcessing = true;
    this.ShowCategorySelector = false;
    let PData = {
      Task: this._HelperService.AppConfig.Api.Core.getcategories,
      SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
      SortExpression: 'Name asc',
      Offset: 0,
      Limit: 1000,
    }
    PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
      PData.SearchCondition,
      "TypeCode",
      this._HelperService.AppConfig.DataType.Text,
      this._HelperService.AppConfig.HelperTypes.MerchantCategories,
      "="
    );
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Op, PData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            this.BusinessCategories = _Response.Result.Data;

            this.ShowCategorySelector = false;
            for (let index = 0; index < this.BusinessCategories.length; index++) {
              const element: any = this.BusinessCategories[index];
              this.S2BusinessCategories = [...this.S2BusinessCategories, {
                id: element.ReferenceId,
                key: element.ReferenceKey,
                text: element.Name
              }];
            }
            this.ShowCategorySelector = true;
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      },
      () => this._HelperService.IsFormProcessing = false
    );

  }
  GetMerchantDetails() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchant,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountId: this._HelperService.UserAccount.AccountId,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._MerchantDetails = _Response.Result;
          this._MerchantDetails.StatusI = this._HelperService.GetStatusIcon(this._MerchantDetails.StatusCode);
          this._MerchantDetails.StatusB = this._HelperService.GetStatusBadge(this._MerchantDetails.StatusCode);
          this._MerchantDetails.StatusC = this._HelperService.GetStatusColor(this._MerchantDetails.StatusCode);
          if (this._MerchantDetails.Categories && this._MerchantDetails.Categories.length > 0) {
            this._MerchantDetails.CategoryDetails = this._MerchantDetails.Categories.map(e => e.ReferenceId);
          }
          this.MerchantContactPerson = this._MerchantDetails.ContactPerson;
          if (this._MerchantDetails.AddressComponent != undefined && this._MerchantDetails.AddressComponent != null) {
            this.MerchantAddress = this._MerchantDetails.AddressComponent;
            this.zoom = 18;
            this.center = { lat: this.MerchantAddress.Latitude, lng: this.MerchantAddress.Longitude };
            this.markerPosition = { lat: this.MerchantAddress.Latitude, lng: this.MerchantAddress.Longitude };
          }
          this._MerchantDetails.StartDateS = this._HelperService.GetDateS(this._MerchantDetails.StartDate);
          this._MerchantDetails.EndDateS = this._HelperService.GetDateS(this._MerchantDetails.EndDate);
          this._MerchantDetails.CreateDateS = this._HelperService.GetDateTimeS(this._MerchantDetails.CreateDate);
          this._MerchantDetails.ModifyDateS = this._HelperService.GetDateTimeS(this._MerchantDetails.ModifyDate);
          this._MerchantDetails.StatusI = this._HelperService.GetStatusIcon(this._MerchantDetails.StatusCode);
          this._MerchantDetails.StatusB = this._HelperService.GetStatusBadge(this._MerchantDetails.StatusCode);
          this._MerchantDetails.StatusC = this._HelperService.GetStatusColor(this._MerchantDetails.StatusCode);
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      },
      () => this._HelperService.IsFormProcessing = false
    );
  }

  CategoryChange(event) {
    // console.log(event);
    let categoryD: any = [];
    if (event && event.length > 0) {
      event.forEach(formVal => {
        let catItem = this.S2BusinessCategories.find(e => e.id == formVal);
        if (catItem) {
          categoryD.push({
            Name: catItem.text,
            ReferenceKey: catItem.key,
            ReferenceId: catItem.id
          });
        }
      });
      this._MerchantDetails.Categories = categoryD;
    } else {
      this._MerchantDetails.Categories = [];
    }
  }
  async AddressChange(event: any) {
    this.MerchantAddress.CityAreaId = 0;
    this.MerchantAddress.CityAreaCode = null;
    this.MerchantAddress.CityAreaName = null;

    this.MerchantAddress.CityId = 0;
    this.MerchantAddress.CityCode = null;
    this.MerchantAddress.CityName = null;

    this.MerchantAddress.StateId = 0;
    this.MerchantAddress.StateCode = null;
    this.MerchantAddress.StateName = null;
    this.MerchantAddress.PostalCode = null;

    var tAddress = this._HelperService.GoogleAddressArrayToJson(event.address_components);
    if (tAddress.postal_code != undefined && tAddress.postal_code != null && tAddress.postal_code != "") {
      this.MerchantAddress.PostalCode = tAddress.postal_code;
    }
    if (tAddress.country != undefined && tAddress.country != null && tAddress.country != "") {
      this.MerchantAddress.CountryName = tAddress.country;
    }
    if (tAddress.administrative_area_level_1 != undefined && tAddress.administrative_area_level_1 != null && tAddress.administrative_area_level_1 != "") {
      this.MerchantAddress.StateName = tAddress.administrative_area_level_1;
    }
    if (tAddress.locality != undefined && tAddress.locality != null && tAddress.locality != "") {
      this.MerchantAddress.CityName = tAddress.locality;
    }
    if (tAddress.administrative_area_level_2 != undefined && tAddress.administrative_area_level_2 != null && tAddress.administrative_area_level_2 != "") {
      this.MerchantAddress.CityAreaName = tAddress.administrative_area_level_2;
    }
    if (tAddress.administrative_area_level_2 != undefined && tAddress.administrative_area_level_2 != null && tAddress.administrative_area_level_2 != "") {
      this.MerchantAddress.CityAreaName = tAddress.administrative_area_level_2;
    }
    if (this.MerchantAddress.CountryName != this._HelperService.UserCountry.CountryName) {
      this._HelperService.NotifyError('Currently we’re not serving in this area, please add locality within ' + this._HelperService.UserCountry.CountryName);
      this.HCXLocManager_OpenUpdateManager_Clear();
    }
    else {
      this.MerchantAddress.CountryId = this._HelperService.UserCountry.CountryId;
      this.MerchantAddress.CountryCode = this._HelperService.UserCountry.CountryKey;
      this.MerchantAddress.CountryName = this._HelperService.UserCountry.CountryName;
      await this.HCXLoc_Manager_GetStates();
      this.MapAddress = event.formatted_address;
      this._MerchantDetails.Address = event.formatted_address;
      this.zoom = 18;
      this.center = { lat: event.geometry.location.lat(), lng: event.geometry.location.lng() }
      this.markerPosition = { lat: event.geometry.location.lat(), lng: event.geometry.location.lng() }
      this.MerchantAddress.Latitude = event.geometry.location.lat();
      this.MerchantAddress.Longitude = event.geometry.location.lng();
      this.MerchantAddress.MapAddress = event.formatted_address;
      this.MerchantAddress.Address = event.formatted_address;
    }
  }
  HCXLoc_Manager_GetStates() {
    this.loadingAddressData = true;
    var PData = {
      Task: this._HelperService.AppConfig.Api.Core.getstates,
      ReferenceKey: this.MerchantAddress.CountryCode,
      ReferenceId: this.MerchantAddress.CountryId,
      Offset: 0,
      Limit: 1000,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.State, PData);
    _OResponse.subscribe(
      async (_Response) => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            var _Data = _Response.Result.Data as any[];
            if (_Data.length > 0) {
              let StateItem = _Data.find(e => e.Name == this.MerchantAddress.StateName);
              if (StateItem) {
                this.MerchantAddress.StateId = StateItem.ReferenceId;
                this.MerchantAddress.StateCode = StateItem.ReferenceKey;
                this.MerchantAddress.StateName = StateItem.Name;
              }
              if (this.MerchantAddress.CityName != undefined && this.MerchantAddress.CityName != null && this.MerchantAddress.CityName != "") {
                await this.HCXLoc_Manager_GetStateCity(this.MerchantAddress.CityName);
              }
            }
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }, () => this.loadingAddressData = false);
  }

  HCXLocManager_OpenUpdateManager_Clear() {
    this.MerchantAddress = {
      Latitude: 0,
      Longitude: 0,
      Address: null,
      MapAddress: null,
      CountryId: this._HelperService.UserCountry.CountryId,
      CountryCode: this._HelperService.UserCountry.CountryKey,
      CountryName: this._HelperService.UserCountry.CountryName,
      StateId: 0,
      StateName: null,
      StateCode: null,
      CityId: 0,
      CityCode: null,
      CityName: null,
      CityAreaId: 0,
      CityAreaName: null,
      CityAreaCode: null,
      PostalCode: null,
    }
  }
  async HCXLoc_Manager_GetStateCity(CityName: string) {
    this.loadingAddressData = true;
    let PData: any = {
      Task: this._HelperService.AppConfig.Api.Core.getcities,
      ReferenceKey: this.MerchantAddress.StateCode,
      ReferenceId: this.MerchantAddress.StateId,
      SearchCondition: this._HelperService.GetSearchConditionStrict('', 'Name', 'text', CityName, '='),
      Offset: 0,
      Limit: 1,
    }
    const setDataCity = function (this: ProfileTabComponent, Item: any) {
      this.MerchantAddress.CityId = Item.ReferenceId ?? 0;
      this.MerchantAddress.CityCode = Item.ReferenceKey ?? null;
      this.MerchantAddress.CityName = Item.Name ?? null;
      setTimeout(() => {
        this.loadingAddressData = false;
      }, 500);
    }.bind(this);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.City, PData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            if (_Response.Result.Data && _Response.Result.Data.length > 0) setDataCity(_Response.Result.Data[0]);
            else setDataCity(null);
          }
          else setDataCity(null);
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
  submitForm() {
    let emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if (!this._MerchantDetails.DisplayName) {
      this._HelperService.NotifyError('Business name is required');
    }
    else if (!this._MerchantDetails.ContactNumber || !this.MerchantContactPerson.MobileNumber) {
      this._HelperService.NotifyError('Phone number is required');
    }
    else if (!this._MerchantDetails.EmailAddress || !this.MerchantContactPerson.EmailAddress) {
      this._HelperService.NotifyError('Email address is required');
    }
    else if (!this._MerchantDetails.EmailAddress.match(emailRegex) || !this.MerchantContactPerson.EmailAddress.match(emailRegex)) {
      this._HelperService.NotifyError('Invalid Email address');
    }
    else if (!this._MerchantDetails.AccountCode) {
      this._HelperService.NotifyError('Merchant\'s Id is required');
    }
    else if (!this.MerchantContactPerson.FirstName) {
      this._HelperService.NotifyError('FirstName is required');
    }
    else if (!this.MerchantContactPerson.LastName) {
      this._HelperService.NotifyError('Lastname is required');
    }
    else {
      this._HelperService.IsFormProcessing = true;
      let pData: any = {
        Task: "updatemerchantdetails",
        Name: this._MerchantDetails.Name,
        OwnerName: this._MerchantDetails.OwnerName,
        DisplayName: this._MerchantDetails.DisplayName,
        ContactNumber: this._MerchantDetails.ContactNumber,
        EmailAddress: this._MerchantDetails.EmailAddress,
        GenderCode: 'gender.male',
        DateOfBirth: null,
        ReferralCode: this._MerchantDetails.ReferralCode,
        WebsiteUrl: this._MerchantDetails.WebsiteUrl,
        Categories: this._MerchantDetails.Categories,
        Address: this.MerchantAddress.Address,
        AddressComponent: this.MerchantAddress,
        ContactPerson: this.MerchantContactPerson,
        AccountKey: this._HelperService.AppConfig.AccountKey,
        AccountId: this._HelperService.AppConfig.AccountId
      }
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
      _OResponse.subscribe(
        _Response => {
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this.BusinessInfoEditable = false;
            this.ContactInfoEditable = false;
            this._HelperService.NotifySuccess(_Response.Message);
          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        }, _Error => {
          this._HelperService.HandleException(_Error);
        }, () => this._HelperService.IsFormProcessing = false);
    }
  }
}
