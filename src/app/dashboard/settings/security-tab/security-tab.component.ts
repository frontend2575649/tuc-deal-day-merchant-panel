import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HelperService } from 'src/app/utils';

@Component({
  selector: 'app-security-tab',
  templateUrl: './security-tab.component.html',
  styleUrls: ['./security-tab.component.css']
})
export class SecurityTabComponent {
  constructor(
    public _HelperService: HelperService,
    public modalService: NgbModal,
  ) { }

}
