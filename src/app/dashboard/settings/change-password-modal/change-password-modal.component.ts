import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { HelperService, OResponse } from 'src/app/utils';

@Component({
  selector: 'app-change-password-modal',
  templateUrl: './change-password-modal.component.html',
  styleUrls: ['./change-password-modal.component.css']
})
export class ChangePasswordModalComponent implements OnInit {

  _modal: any;
  ShowPassword1: boolean = false;
  ShowPassword2: boolean = false;
  ShowPassword3: boolean = false;
  ispwdContainsNum: boolean = true;
  ispwdContainsUC: boolean = true;
  ispwdContainsLC: boolean = true;
  IsminLength: boolean = true;
  isPwdContainsSC: boolean = true;
  changePasswordForm: FormGroup;
  @Input('modal') set modal(value) {
    if (value) {
      this.initForm();
      this._modal = value;
    }
  }

  constructor(
    private fb: FormBuilder,
    public _HelperService: HelperService,
  ) { }

  ngOnInit(): void {

  }
  initForm() {
    this.changePasswordForm = this.fb.group({
      oldPassword: [null, Validators.required],
      newPassword: [null, Validators.required],
      confirmPassword: [null, Validators.required],
    });
  }

  ToogleShowHidePassword(val: string): void {
    if (val == "oldPassword") this.ShowPassword1 = !this.ShowPassword1;
    else if (val == "newPassword") this.ShowPassword2 = !this.ShowPassword2;
    else this.ShowPassword3 = !this.ShowPassword3;
  }
  pwdValueChange(event: any) {
    let val = event.target.value;
    this.ispwdContainsNum = /\d/.test(val);
    this.ispwdContainsUC = /[A-Z]/.test(val);
    this.ispwdContainsLC = /[a-z]/.test(val);
    this.IsminLength = val.length >= 8;
    this.isPwdContainsSC = /(?=.*?[#?!@$%^&*-])/.test(val);
  }
  submitForm() {
    let _form = this.changePasswordForm.value;
    if (_form.oldPassword == undefined || _form.oldPassword == null || _form.oldPassword == '') {
      this._HelperService.NotifyError("Enter old password");
    }
    else if (_form.newPassword == undefined || _form.newPassword == null || _form.newPassword == '') {
      this._HelperService.NotifyError("Enter new password");
    }
    else if (_form.confirmPassword == undefined || _form.confirmPassword == null || _form.confirmPassword == '') {
      this._HelperService.NotifyError("Enter confirm password");
    }
    else if (_form.oldPassword.length < 8 || _form.newPassword.length < 8 || _form.confirmPassword.length < 8) {
      this._HelperService.NotifyError("Password should be at least 8 characters");
    }
    else if (_form.newPassword !== _form.confirmPassword) {
      this._HelperService.NotifyError("Password mismatch, new password and confirm password should be same");
    }
    else if (this.changePasswordForm.valid) {
      let pData = {
        Task: "updateuserpassword",
        OperationType: "new",
        ReferenceKey: this._HelperService.AppConfig.AccountKey,
        OldPassword: this.changePasswordForm.value.oldPassword,
        Password: this.changePasswordForm.value.newPassword
      }
      this._HelperService.IsFormProcessing = true;
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
      _OResponse.subscribe(
        _Response => {
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._modal.close();
            this._HelperService.NotifySuccess(_Response.Message || 'Password updated');
          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        }, _Error => {
          this._HelperService.HandleException(_Error);
        }, () => this._HelperService.IsFormProcessing = false);
    }
  }
}
