import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { OResponse } from 'src/app/interfaces';
import { HelperService, DataHelperService, PaginationService } from 'src/app/utils';


@Component({
  selector: 'app-cancelled-orders',
  templateUrl: './cancelled-orders.component.html',
  styleUrls: ['./cancelled-orders.component.css']
})
export class CancelledOrdersComponent implements OnInit {
  OverviewData: any;
  OrderData: any;
  public TotalRecords = 0;
  public Offset = 0;
  public activePage = 1;
  public pager: any = {};
  public PageRecordLimit:number = 10;
  SearchParameter:string = '';
  Limit:number = 10;
  ListType: number = 1;
  condition: string;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    private paged: PaginationService
){ }

ngOnInit() {
  this._HelperService.StopClickPropogation();
  this.GetOverviews()
  this.FetchOrders()
}

  GetOverviews(): any {
    this._HelperService.IsFormProcessing = true;
    var pData = {
        AccountId: this._HelperService.AppConfig.AccountId,
        AccountKey: this._HelperService.AppConfig.AccountKey,
        Task: "getoverview",
        RefreshCount: true,
        SearchCondition: "( ( StatusCode = \"orderstatus.cancelledbyuser\" ) AND StatusCode = \"orderstatus.cancelledbyseller\" ) AND StatusCode = \"orderstatus.cancelledbysystem\" ",
        SortExpression: "CreateDate desc",
        Type: null,
        ReferenceKey: this._HelperService.AppConfig.AccountKey,
        ReferenceId: this._HelperService.AppConfig.AccountId,
        SubReferenceId: 0,
        IsDownload: false
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pData);
    _OResponse.subscribe(
        _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
                this.OverviewData = _Response.Result as any;
            }
            else {
                this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
        });
}

FetchOrders(){
  let body = {
      Task: "getshipments",
      Limit: this.PageRecordLimit,
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      StatusType: "default",
      DefaultSortExpression: "CreateDate desc",
      Offset: 0,
      RefreshCount: true,
      SearchCondition: "( ( StatusCode = \"orderstatus.cancelledbyuser\" ) AND StatusCode = \"orderstatus.cancelledbyseller\" ) AND StatusCode = \"orderstatus.cancelledbysystem\" ",
      SortExpression: "CreateDate desc",
      Type: null,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SubReferenceId: 0,
      IsDownload: false
  }
  let _OResponse: Observable<OResponse>;
  _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, body);
  _OResponse.subscribe(
      _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
              this.OrderData = _Response.Result.Data as any;
              this.TotalRecords = _Response.Result.TotalRecords;
              this.pager = this.paged.getPager(this.TotalRecords, this.activePage, this.PageRecordLimit);
          }
          else {
              this._HelperService.NotifyError(_Response.Message);
          }
      },
      _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
  }); 
}

setPage(page:number){
  this.activePage = page;
  if(page < 1 || page > this.pager.totalPages){
      return
  }
  this.pager = this.paged.getPager(this.TotalRecords, page, this.PageRecordLimit)
  this.FetchOrders()
}
WatchChange(data:any)
{
 this.PageRecordLimit= data
  this.FetchOrders()
}




}
