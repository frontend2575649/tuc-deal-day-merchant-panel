import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllPickupsComponent } from './all-pickups/all-pickups.component';
import { RedeemedOrdersComponent } from './redeemed-orders/redeemed-orders.component';
import { CancelledOrdersComponent } from './cancelled-orders/cancelled-orders.component';


import { RouterModule } from '@angular/router';
import { IconsModule } from '../../shared/index';
import { NgxPaginationModule } from "ngx-pagination";
import { FormsModule } from '@angular/forms';
import { Daterangepicker } from "ng2-daterangepicker";
import { PickupComponent } from './pickup.component';

const PickUpRoutes = [
  {
    path:'all-pickup',
    component:AllPickupsComponent,
  },
  { path:'redemeed-orders', component:RedeemedOrdersComponent },

  { path:'cancelled-orders', component:CancelledOrdersComponent }
]

@NgModule({
  declarations: [
    AllPickupsComponent,
    RedeemedOrdersComponent,
    CancelledOrdersComponent,
    PickupComponent
  ],
  imports: [
    CommonModule,
    IconsModule,
    RouterModule.forChild(PickUpRoutes),
    NgxPaginationModule,
    FormsModule,
    Daterangepicker
  ]
})
export class PickupModule { }
