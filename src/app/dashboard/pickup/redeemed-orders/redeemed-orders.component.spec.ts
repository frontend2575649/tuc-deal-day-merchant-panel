import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RedeemedOrdersComponent } from './redeemed-orders.component';

describe('RedeemedOrdersComponent', () => {
  let component: RedeemedOrdersComponent;
  let fixture: ComponentFixture<RedeemedOrdersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RedeemedOrdersComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RedeemedOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
