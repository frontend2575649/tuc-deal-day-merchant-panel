import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllPickupsComponent } from './all-pickups.component';

describe('AllPickupsComponent', () => {
  let component: AllPickupsComponent;
  let fixture: ComponentFixture<AllPickupsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllPickupsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllPickupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
