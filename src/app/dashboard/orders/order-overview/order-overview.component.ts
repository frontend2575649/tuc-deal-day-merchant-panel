import { ChangeDetectorRef, Component, OnInit, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import cloneDeep from 'lodash/cloneDeep';
import { Observable } from 'rxjs';
// import {  OSalesTrend, ODealDeliveryData,ODealData, OSelect, OList} from '../../../../service/service';

import * as moment from 'moment';
import 'moment-timezone';

import { HelperService, OList, DataHelperService, OResponse } from '../../../utils/index';

@Component({
  selector: "app-order-overview",
  templateUrl: "./order-overview.component.html",
  styles: [
    `
      agm-map {
        height: 300px;
      }
    `
  ]
})
export class OrderOverviewComponent implements OnInit {
  lastdaytext = "LAST DAY";
  lastweektext = "LAST WEEK";
  lastweekCustom = "LAST WEEK";
  lastmonthtext = "LAST MONTH";
  lastyeartext = "LAST YEAR";
  ActualStartDate = moment().startOf("day");
  ActualEndDate = moment().endOf("day");
  Types: any = {
    year: 'year',
    month: 'month',
    week: 'week',
    day: 'day',
    hour: 'hour'
  }
  public SaleAmountData : any;
  public TotalPickUpOrders!: number;
  public PickUpOredersAmount!: number;
  public TotalOredersAmount!: number;
  public TotalDeliveredOrders!: number;
  public DeliveredOredersAmount!: number;
  public _Sale: any = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,
    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,
    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0
  }
  public _Yearly: any = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,

    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,

    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0
  }
  dateRange:any;
  startDate = moment().format('YYYY-MM-DD HH:mm');
  endDate = moment().format('YYYY-MM-DD HH:mm');
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {
  }
  
  //#endregion
  ngOnInit() {
    this._HelperService.FullContainer = false;
    //start time and end time for overview

    this._ActivatedRoute.params.subscribe((params: Params) => {
      this.TUTr_Filter_Stores_Load()
      this.DealsList_Setup()
      this.GetDealSalesOverview()
      this.GetupcomingSaleOverview()
      // this.pData.AccountId = this._HelperService.AppConfig.ActiveMerchantReferenceId;
      // this.pData.AccountKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey;

      this.InitializeDates();

    });

  }


  InitializeDates(): void {

    //#region Sale Dates 

    this._Sale.ActualStartDate = moment().startOf('day');
    this._Sale.ActualEndDate = moment().endOf('day');

    this._Sale.CompareStartDate = moment().subtract(1, 'day').startOf('day');
    this._Sale.CompareEndDate = moment().subtract(1, 'day').endOf('day');

    //#endregion

    //#endregion

    //#region Yearly Dates 

    this._Yearly.ActualStartDate = moment().startOf('year').startOf('day');
    this._Yearly.ActualEndDate = moment().endOf('year').endOf('day');

    this._Yearly.CompareStartDate = moment().subtract(1, 'year').startOf('year').startOf('day');
    this._Yearly.CompareEndDate = moment().subtract(1, 'year').endOf('year').endOf('day');

    //#endregion

    var difference = moment().diff(moment(), 'days');


  }

  public CurrentRange: any = {};
  public DateSelected = moment().startOf("day");
  //#region DateChangeHandler
  DateChanged(event: any, Type: any): void {

    var ev: any = cloneDeep(event);

    this.DateSelected = moment(ev.start).startOf("day");
    this.ActualStartDate = moment(ev.start).startOf("day");
    this.ActualEndDate = moment(ev.end).endOf("day");


    //#region Sale 

    this._Sale.ActualStartDate = moment(ev.start).startOf("day");
    this._Sale.ActualEndDate = moment(ev.end).endOf("day");

    this._Sale.CompareStartDate = moment(ev.start).subtract(1, 'day').startOf("day");
    this._Sale.CompareEndDate = moment(ev.end).subtract(1, 'day').endOf("day");

    //#endregion
    //#region Week 

    //#endregion
    //#region Revenue 
    //#region Yearly 

    this._Yearly.ActualStartDate = moment(ev.start).startOf('year').startOf('day');
    this._Yearly.ActualEndDate = moment(event.end).endOf('year').endOf('day');

    this._Yearly.CompareStartDate = moment(ev.start).subtract(1, 'year').startOf('year').startOf('day');
    this._Yearly.CompareEndDate = moment(ev.end).subtract(1, 'year').endOf('year').endOf('day');

    //#endregion

    var difference = moment(ev.end).diff(moment(ev.start), 'days');
    this.GetupcomingSaleOverview();
    this.GetDealSalesOverview();
  }
  //#endregion

  //#region BarChartConfig 
  public Options: any = {
    cornerRadius: 20,
    responsive: true,
    legend: {
      display: false,
      position: 'right',
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: false
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        // value: 20,
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
    plugins: {
      datalabels: {
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return value;
          } else {
            return value;
          }
        }
      }
    }
  }
  public barChartLabels = [];
  // public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
  public barChartColors = [{ backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    { data: [200], label: 'Remote' },
    { data: [22], label: 'Remote' },
    { data: [22], label: 'Visit' },
    { data: [22], label: 'Visit' },
  ];

  //#endregion

  //#region RevenueBarChartConfig 
  public RevenueOptions: any = {
    cornerRadius: 20,
    responsive: true,
    legend: {
      display: false,
      position: 'right',
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: false
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        // value: 20,
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
    plugins: {
      datalabels: {
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return value;
          } else {
            return value;
          }
        }
      }
    }
  }
  public RevenuebarChartLabels:any = [];
  // public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
  public RevenuebarChartColors = [{ backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }];
  public RevenuebarChartType = 'bar';
  public RevenuebarChartLegend = true;
  public RevenuebarChartData = [
    { data: [200], label: 'Remote' },
    { data: [22], label: 'Remote' },
    { data: [22], label: 'Visit' },
    { data: [22], label: 'Visit' },
  ];


  //#endregion

  //#region Sale Sales Report 
  showSaleChart = true;
  public TotalPublishedDeals!: Number; 
  public TotalPublshedAmount!: Number;




  public _SaleSalesReportReset(): void {


  }
  //#endregion

  //#region Redeem Sales Report 

  public RedeemlineChartLabels = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
  showRedeemChart = true;

  public _RedeemSalesReportReset(): void {


  }


  //#endregion

  //#region Revenue Sales Report 

  showRevenueChart = true;
  //#endregion

  //#region Customer Sales Report 

  public _Customer: any = {
    TotalCustomers: 0
  }

  //#endregion

  public _GetoverviewSummary: any = {};
  public TodayStartTime = null;
  public TodayEndTime = null;
  private pDealData = {
    Task: 'getoverview',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    AccountKey: this._HelperService.UserAccount.AccountKey,
    AccountId: this._HelperService.UserAccount.AccountId,
  };
  GetDealSalesOverview() {

    this._HelperService.IsFormProcessing = true;

    this.pDealData.StartDate = this.startDate;
    this.pDealData.EndDate = this.endDate;

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, this.pDealData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetoverviewSummary = _Response.Result as any;
          return;
        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public _GetPurchaseoverviewSummary: any = {};

  //#region Yearly Sales Report 
  public YearlylineChartLabels = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];
  showYearlyChart = true;

  public _YearlySalesReportReset(): void {


  }


  //#endregion

  //#region Sales History General Method 

  private pSaleData = {
    Task: 'getorderhistory',
    StartDate: null,
    EndDate: null,
    AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: ''
  };

  //#endregion

  computePerc(num: any): any {
    if (num.CompareSalesAmount == 0) {
      return '100 %';
    }
    if (num.ActualSalesAmount > num.CompareSalesAmount) {
      return Math.round(((num.ActualSalesAmount - num.CompareSalesAmount) / num.CompareSalesAmount) * 100) + ' %';
    } else if (num.ActualSalesAmount < num.CompareSalesAmount) {
      return Math.round(((num.CompareSalesAmount - num.ActualSalesAmount) / num.CompareSalesAmount) * 100) + ' %';
    } else {
      return '0 %';
    }
  }

  computeFlag(num: any): number {
    if (num.ActualSalesAmount > num.CompareSalesAmount) {
      return 1;
    } else if (num.ActualSalesAmount < num.CompareSalesAmount) {
      return -1;
    } else {
      return 0;
    }
  }

  getAbsolute(num: number): any {
    // return Math.abs(num);
    return num;
  }



  hideSalePicker: boolean = true;
  hideRedeemPicker: boolean = true;
  hideRevenuePicker: boolean = true;
  hideYearlyPicker: boolean = true;

  ShowHideCalendar(type: string) {
    switch (type) {
      case this._HelperService.AppConfig.DatePickerTypes.hour: {
        this.hideSalePicker = !this.hideSalePicker;
      }

        break;
      case this._HelperService.AppConfig.DatePickerTypes.week: {
        this.hideRedeemPicker = !this.hideRedeemPicker;
      }

        break;
      case this._HelperService.AppConfig.DatePickerTypes.month: {
        this.hideRevenuePicker = !this.hideRevenuePicker;
      }

        break;
      case this._HelperService.AppConfig.DatePickerTypes.year: {
        this.hideYearlyPicker = !this.hideYearlyPicker;
      }
        break;
      default:
        break;
    }
  }


  //#region Sales History General Method 

 
  //#region Store Filter
  public TUTr_Filter_Store_Selected: any;
  public ToggleStoreSelect: boolean = false;
  public TUTr_Filter_Store_Option: any;
  TUTr_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: any = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Store
        }
      ]
    };

    var OwnerKey:any = this._HelperService.UserAccount.AccountId;
    if (this._HelperService.UserAccount.AccountTypeCode != this._HelperService.AppConfig.AccountType.Merchant) {
      OwnerKey = this._HelperService.UserOwner.AccountId;
    }
    _Select.SearchCondition = 
        this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, OwnerKey, '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Store_Option = {
      placeholder: 'Select Store',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Stores_Change(event: any) {
    if (event.value == this.TUTr_Filter_Store_Selected) {
      this.pSaleData.StoreReferenceId = 0;
      this.pSaleData.StoreReferenceKey = null;
      this.TUTr_Filter_Store_Selected = 0;
    }
    else if (event.value != this.TUTr_Filter_Store_Selected) {
      this.pSaleData.StoreReferenceId = event.data[0].ReferenceId;
      this.pSaleData  .StoreReferenceKey = event.data[0].ReferenceKey;

      this.TUTr_Filter_Store_Selected = event.value;
    }
   // this.LoadData();
    setTimeout(() => {
      this._HelperService.ToggleField = false;
    }, 500);
  }
  //#endregion

public DealsData1!: number;
public DealsData:any;
DealsList_Setup(){
    let DealPayload={
      Task: "getdeals",
      TotalRecords: 0,
      Offset: 0,
      Limit: 5,
      RefreshCount: true,
      SearchCondition:this._HelperService.GetSearchConditionStrict('', 'AccountId', this._HelperService.AppConfig.DataType.Number,
       this._HelperService.AppConfig.AccountId, '=='),
      SortExpression: "CreateDate desc",
      Type: "running",
      ReferenceKey: null,
      ReferenceId: 0,
      SubReferenceId: 0,
      ListType: 3,
      IsDownload: false
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, DealPayload);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
         this.DealsData=_Response.Result.Data
         this.DealsData1 = _Response.Result.TotalRecords
          this._ChangeDetectorRef.detectChanges();
          //#endregion
        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
    }
    public _GetoverviewDealSummary: any = {};
    private pData = {
      Task: 'getdealsoverview',
      StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
      EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      StoreReferenceId: 0,
      StoreReferenceKey: null,
    };
    RejectedDeal:number=0
    DisableRejectedDiv:any;
    GetupcomingSaleOverview() {
      this._HelperService.IsFormProcessing = true;
      this.pData.StartDate = this.startDate;
      this.pData.EndDate = this.endDate;
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, this.pData);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._GetoverviewDealSummary = _Response.Result as any;
            this._ChangeDetectorRef.detectChanges();
            return;
          }
          else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        });
    }
    Routeupcoming(){
      this._Router.navigate([
        this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Deals,
        1
        
      ]);
    }
  CloseRowModal(): void { }
}
