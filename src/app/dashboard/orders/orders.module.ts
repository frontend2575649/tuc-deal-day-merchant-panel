import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdersComponent } from './orders.component';
import { Router, RouterModule } from '@angular/router';
import { IconsModule } from '../../shared/index';
import { NgxPaginationModule } from "ngx-pagination";
import { FormsModule } from '@angular/forms';
import { OrdersHistoryComponent } from './order-history/order-history.component';
import { Daterangepicker } from "ng2-daterangepicker";
import { ViewOrderComponent } from './view-order/view-order.component';
import { OrderOverviewComponent } from './order-overview/order-overview.component';
const OrderRoutes = [
  {
    path:'history',
    component:OrdersComponent,
  },
  { path:'management', component:OrdersHistoryComponent },

  { path:'details/:referenceid/:referencekey/:accountid/:accountkey', component: ViewOrderComponent },

  // { path:'overview', component:OrderOverviewComponent }
]
@NgModule({
  declarations: [
    OrdersComponent,
    OrdersHistoryComponent,
    ViewOrderComponent,
    OrderOverviewComponent
  ],
  imports: [
    CommonModule,
    IconsModule,
    RouterModule.forChild(OrderRoutes),
    NgxPaginationModule,
    FormsModule,
    Daterangepicker
  ],
  exports:[
    RouterModule
  ]
})
export class OrdersModule { }
