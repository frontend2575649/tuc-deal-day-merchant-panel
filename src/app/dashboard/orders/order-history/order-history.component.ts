import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ChangeContext } from 'ng5-slider';
import { Observable } from 'rxjs/internal/Observable';
import { HelperService, OList, DataHelperService, OResponse, PaginationService } from '../../../utils/index';


declare var $:any
@Component({
  selector: 'app-orders',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.css']
})
export class OrdersHistoryComponent implements OnInit {
  //@ViewChild('storeAddedSuccessModal', { static: true }) storeAddedSuccessModal:any;
  Data: any;
  OverviewData: any;
  ListType: number | undefined;
  public ConfimationOrderDetails: any;
  dialogType: any;
  public TotalRecords = 0;
  public Offset = 0;
  public activePage = 1;
  public pager: any = {};
  public PageRecordLimit:number = 10;
  OrderData:any;
    viewType: { SearchCondition: string; ListType: number; Type: string;orderName:string};
    actionType: string = 'all';
  constructor(
      private modalService: NgbModal,
      public _Router: Router,
      public _ActivatedRoute: ActivatedRoute,
      public _FormBuilder: FormBuilder,
      public _HelperService: HelperService,
      public _DataHelperService: DataHelperService,
      public _ChangeDetectorRef: ChangeDetectorRef,
      private paged: PaginationService

  ){ }

  public ResetFilterControls: boolean = true;
  public AcquirerId = 0;
  ngOnInit() {
     this.GetOrdersOverview()
     this.setConditions('')
  }

  public _GetOverviewSummary: any = {};

  open(content: any) {
    this.modalService.open(content, { ariaLabelledBy: 'create-store-modal-body', backdrop: 'static', size: 'lg' })
  }
  TUTr_InvoiceRangeMinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
  TUTr_InvoiceRangeMaxAmount: any = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
  TodayStartTime = null;
  TodayEndTime = null;

GetOrdersOverview() {
    this._HelperService.IsFormProcessing = true;
     let pData = {
        Task: "getordersovrview",
        AccountId: this._HelperService.AppConfig.AccountId,
        AccountKey: this._HelperService.AppConfig.AccountKey,
        StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
         EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetOverviewSummary = _Response.Result as any;
          return;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  GetRecords()
  {
    let body =   {
      Task: "getshipments",
      Offset: this.Offset,
      Limit: this.PageRecordLimit,
      TotalRecords:this.TotalRecords,
      RefreshCount: true,
      SearchCondition: this.viewType.SearchCondition,
      ListType: this.viewType.ListType,
      Type: this.viewType.Type,
      SortExpression: "CreateDate desc",
      ReferenceKey: null,
      ReferenceId: 0,
      SubReferenceId: 0,
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      IsDownload: false,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, body);
    _OResponse.subscribe(
        _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
                this.OrderData = _Response.Result.Data as any;
                this.TotalRecords = _Response.Result.TotalRecords;
                this.pager = this.paged.getPager(this.TotalRecords, this.activePage, this.PageRecordLimit);
            }
            else {
                this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
    }); 
  }

  ListTypeChange(action:string)
  {
    this.actionType = action
    this.setConditions(action)
    this.scrollTop()
  }


  setConditions(data){
    switch(data){
      case 'new':
          this.viewType = {
            SearchCondition: `MerchantId == \"${this._HelperService.UserAccount.AccountId}\" AND (StatusCode == \"orderstatus.new\" OR StatusCode == \"orderstatus.pendingconfirmation\") `,
            ListType:1,
            Type:null,
            orderName:'new'   
          }
        break;
      case 'processing':
        this.viewType = {
          SearchCondition: `MerchantId == \"${this._HelperService.UserAccount.AccountId}\" AND StatusCode == \"orderstatus.preparing\" `,
          ListType:1,
          Type:null,
          orderName:'processing'    
        }
        break;
      case 'readyforpickup':
        this.viewType = {
          SearchCondition: `MerchantId == \"${this._HelperService.UserAccount.AccountId}\" AND (StatusCode == \"orderstatus.ready\" OR StatusCode == \"orderstatus.readytopickup\")`,
          ListType:1,
          Type:null,
          orderName:'readyforpickup'     
        }
        break;
      case 'shipped':
        this.viewType = {
          SearchCondition: `MerchantId == \"${this._HelperService.UserAccount.AccountId}\" AND StatusCode == \"orderstatus.delivered\" `,
          ListType:1,
          Type:null,
          orderName:'shipped'  
        }
        break;
      case 'delivered':
        this.viewType = {
          SearchCondition: `MerchantId == \"${this._HelperService.UserAccount.AccountId}\" AND StatusCode == \"orderstatus.delivered\" `,
          ListType:1,
          Type:null,
          orderName:'delivered' 
        }
        break;
      case 'returned':
        this.viewType = {
          SearchCondition: `MerchantId == \"${this._HelperService.UserAccount.AccountId}\" AND StatusCode == \"orderstatus.deliveryfailed\"`,
          ListType:1,
          Type:null,
          orderName:'returned'    
        }
        break;
      case 'cancelled':
        this.viewType = {
          SearchCondition: `MerchantId == \"${this._HelperService.UserAccount.AccountId}\" AND (StatusCode == \"orderstatus.cancelledbyuser\" OR StatusCode == \"orderstatus.cancelledbyseller\" OR StatusCode == \"orderstatus.cancelledbysystem\")`,
          ListType:1,
          Type:null,
          orderName:'cancelled'      
        }
        break;
      default:
        this.viewType = {
          SearchCondition: `MerchantId == \"${this._HelperService.UserAccount.AccountId}\"`,
          ListType:1,
          Type:'',
          orderName:'all'     
        }
      break
    }
    this.GetRecords()
  }

  scrollTop=()=>
  {
    window.scroll({ 
      top: 0, 
      left: 0, 
      behavior: 'smooth' 
    });
  }


ViewOrder(ReferenceData) {
    //#region Save Current Merchant To Storage 
    this._HelperService.AppConfig.AccountId =
        ReferenceData.MerchantId;
    this._HelperService.AppConfig.ActiveOwnerKey = ReferenceData.MerchantKey;
    this._Router.navigate([
        '/orders/details',
        ReferenceData.ReferenceId,
        ReferenceData.ReferenceKey,
        ReferenceData.MerchantId,
        ReferenceData.MerchantKey,
    ]);
}
OpenConfimation(Orderdata, type?) {
    this.dialogType = type;
    this.ConfimationOrderDetails = Orderdata
    this._HelperService.OpenModal('Form_Confirmation');
}

updateOrderStatus() {
    var pData = {
        Task: 'updateorderstatus',
        ReferenceId: this.ConfimationOrderDetails.ReferenceId,
        ReferenceKey: this.ConfimationOrderDetails.ReferenceKey,
        StatusCode: this.StatusCodes(),
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pData);
    _OResponse.subscribe(
        _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.NotifySuccess(_Response.Message);
                setTimeout(() => {
                    window.location.reload()
                }, 900);
            }
            else {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.HandleException(_Error);
        }
    );
}

ArrangeOrder() {
    var pData = {
        Task: 'arrangeshipment',
        ReferenceId: this.ConfimationOrderDetails.ReferenceId,
        ReferenceKey: this.ConfimationOrderDetails.ReferenceKey,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pData);
    _OResponse.subscribe(
        _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.NotifySuccess("Order Arranged Successfully");
                this._ChangeDetectorRef.detectChanges();
            }
            else {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.HandleException(_Error);
        }
    );
}

StatusCodes() {
    let StatusCode1 = ''
    if (this.ConfimationOrderDetails.StatusName == 'New' || this.ConfimationOrderDetails.StatusName == 'Pending Confirmation') {
        StatusCode1 = "orderstatus.confirmed"
    } else {
        StatusCode1 = "orderstatus.readytopickup"
    }
    return StatusCode1
}
cancellationModal(Orderdata) {
    this.ConfimationOrderDetails = Orderdata
    this._HelperService.OpenModal('Form_Cancellation');
}

OpenReadyModal(Orderdata) {
    this.ConfimationOrderDetails = Orderdata
    this._HelperService.OpenModal('Form_Ready');
}
setPage(page:number){
    if(page < 1 || page > this.pager.totalPages){
        return
    }
    this.pager = this.paged.getPager(this.TotalRecords, page, this.PageRecordLimit)
    this._HelperService.scrollTop()
    if(page < this.activePage)
    {
      this.Offset -= this.PageRecordLimit
    }else{
      this.Offset += this.PageRecordLimit
    }
    this.activePage = page;
    this.GetRecords()
  }
  
WatchChange(data:any)
{
 this.PageRecordLimit = data
 this.activePage = 1
 this._HelperService.scrollTop()
 this.Offset = 0;
 this.GetRecords()
}


StyleAmount(data:string)
{
  switch(data)
  {
    case 'orderstatus.new':
       'text-warning'
    break;
    case 'orderstatus.shipped':
        'text-success'
    break;
    case 'orderstatus.failed':
       'text-danger'
    break;
  }
}

CancelOrder() {
        var pData = {
            Task: 'updateorderstatus',
            ReferenceId: this.ConfimationOrderDetails.ReferenceId,
            ReferenceKey: this.ConfimationOrderDetails.ReferenceKey,
            StatusCode: "orderstatus.cancelledbysystem",
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.IsFormProcessing = false;
                    this.GetRecords();
                    this._ChangeDetectorRef.detectChanges();
                    //#endregion
                }
                else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }   

}
