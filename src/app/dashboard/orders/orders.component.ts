import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/internal/Observable';
import { HelperService, OList, DataHelperService, OResponse, PaginationService } from '../../utils/index';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  //@ViewChild('storeAddedSuccessModal', { static: true }) storeAddedSuccessModal:any;
  Data: any;
  OverviewData: any;
  OrderData: any;
  public TotalRecords = 0;
  public Offset = 0;
  public activePage = 1;
  public pager: any = {};
  public PageRecordLimit:number = 10;
  SearchParameter:string = ''
  constructor(
      private modalService: NgbModal,
      public _Router: Router,
      public _ActivatedRoute: ActivatedRoute,
      public _FormBuilder: FormBuilder,
      public _HelperService: HelperService,
      public _DataHelperService: DataHelperService,
      public _ChangeDetectorRef: ChangeDetectorRef,
      private paged: PaginationService
  ){ }

  public ResetFilterControls: boolean = true;
  public AcquirerId = 0;
  Limit:number = 10
  ngOnInit() {
    this._HelperService.StopClickPropogation();
    this.FetchOrders()
  }

  open(content: any) {
    this.modalService.open(content, { ariaLabelledBy: 'create-store-modal-body', backdrop: 'static', size: 'lg' })
  }

timeout:any;
  GetOverviews(ListOptions: any, Task: string): any {
    this._HelperService.IsFormProcessing = true;
    var pData = {
        Task: Task,
        Limit: ListOptions.PageRecordLimit,
        AccountId: this._HelperService.AppConfig.AccountId,
        AccountKey: this._HelperService.AppConfig.AccountKey,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pData);
    _OResponse.subscribe(
        _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
                this.OverviewData = _Response.Result as any;
            }
            else {
                this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
        });
}

ToggleItems()
{

}
FetchOrders(){
    let body = {
        Task: "getorders",
        Limit: this.PageRecordLimit,
        AccountId: this._HelperService.UserAccount.AccountId,
        AccountKey: this._HelperService.UserAccount.AccountKey,
        Offset:this.activePage - 1
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, body);
    _OResponse.subscribe(
        _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
                this.OrderData = _Response.Result.Data as any;
                this.TotalRecords = _Response.Result.TotalRecords;
                this.pager = this.paged.getPager(this.TotalRecords, this.activePage, this.PageRecordLimit);
            }
            else {
                this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
    }); 
}

setPage(page:number){
    this.activePage = page;
    if(page < 1 || page > this.pager.totalPages){
        return
    }
    this.pager = this.paged.getPager(this.TotalRecords, page, this.PageRecordLimit)
    this.FetchOrders()
}
WatchChange(data:any)
{
   this.PageRecordLimit= data
    this.FetchOrders()
}
ApplyFilters()
{}
ResetFilters()
{}
RunSearch(data)
{}



}
