import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { LayoutModule } from '../layout/layout.module';
import { RouterModule } from '@angular/router';
import { IconsModule } from '../shared/index';
import { DashboardRoutingModule } from './dashboard.routes';

@NgModule({
  declarations: [
    DashboardComponent,
  ],
  imports: [
    CommonModule,
    LayoutModule,
    RouterModule,
    IconsModule,
    DashboardRoutingModule
  ],
  exports:[
    RouterModule
  ]
})
export class DashboardModule { }
