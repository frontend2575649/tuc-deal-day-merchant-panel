import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { IconsModule, SharedModule } from '../../shared/index';
import { NgChartsModule } from 'ng2-charts';

const Routes  = [
  {
    path:'',
    component:DashboardComponent,
    pathmatch:'full'
  }
]
@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(Routes),
    IconsModule,
    SharedModule,
    NgChartsModule
  ]
})
export class DashboardModule { }
