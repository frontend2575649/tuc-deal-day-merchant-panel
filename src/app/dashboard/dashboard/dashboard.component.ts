import { ChangeDetectorRef,Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ChartConfiguration, ChartOptions, ChartType } from "chart.js";
import { HelperService, OList, DataHelperService, OResponse, OSalesTrend, ExcelService } from '../../utils/index';
import * as moment from 'moment';
import { Observable, Subscription } from 'rxjs';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  @ViewChild('storeAddedSuccessModal', { static: true }) storeAddedSuccessModal:any;
  public lineChartData: ChartConfiguration<'line'>['data'] = {
    labels: [
      'Mon',
      'Tue',
      'Wed',
      'Thur',
      'Fri',
      'Sat',
      'Sun'
    ],
    datasets: [
      {
        data: [ 65, 59, 80, 81, 56, 55, 40 ],
        label: 'Thursday',
        fill: true,
        tension: 0.5,
        borderColor: '#5041BC',
        backgroundColor: 'rgba(255, 225, 225, 0.54)'
      }
    ]
  };
  public lineChartOptions: ChartOptions<'line'> = {
    elements: {
      point:{
          radius: 0
      },
  },
    responsive: false,
    interaction: {
      mode: 'point'
  }
  };
  public lineChartLegend = true;
  StoresList: any;
  todayDate:string;
  PurchaseData:any;
  constructor(
    public modalService: NgbModal,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,

    private excel: ExcelService) {
      this.InitializeDates();
  }
  ngOnInit(): void {
    // open success modal after add store
    // this.modalService.open(this.storeAddedSuccessModal, { ariaLabelledBy: 'create-store-modal-body', backdrop: 'static', size: 'sm' })
    this.TUTr_Setup(this._Sale.ActualStartDate, this._Sale.ActualEndDate);
    this.GetSalesOverview()
    this.FetchStores()
    this.todayDate = moment().format('MMMM Do YYYY')
    this.getPurchase()
  }
  open(content: any) {
    this.modalService.open(content, { ariaLabelledBy: 'create-store-modal-body', backdrop: 'static', size: 'lg' })
  }


  public _GetoverviewSummary: any = {};
  public TodayStartTime = null;
  public TodayEndTime = null;
  private pData = {
    Task: 'getdealsoverview',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    AccountId: this._HelperService.AppConfig.AccountId,
    AccountKey: this._HelperService.AppConfig.AccountKey,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };

  RejectedDeal:number=0
  DisableRejectedDiv:any;

  public TUTr_Config: any;
  TUTr_Setup(StartDateTime, EndDateTime) {

    this.TUTr_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetPurchaseHistory,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Deals,
      Title: "Purchase History",
      StatusType: "DealCode",
      StartDate: this._HelperService.DateInUTC(StartDateTime),
      EndDate: this._HelperService.DateInUTC(EndDateTime),

      DefaultSortExpression: "CreateDate desc",
      SearchBaseCondition: this._HelperService.
          GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number,
               this._HelperService.AppConfig.AccountId, '='),
      TableFields: [
        {
          DisplayName: "Title",
          SystemName: "Title",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Contact No",
          SystemName: "AccountMobileNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Customer Name",
          SystemName: "AccountDisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Sold Date",
          SystemName: "StartDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
        {
          DisplayName: "Expiry Date",
          SystemName: "EndDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
        {
          DisplayName: "Amount",
          SystemName: "Amount",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "text-center",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
      ]
    };
    this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.SoldHistory,
      this.TUTr_Config
    );
    this.TUTr_GetData();
  }

  TUTr_GetData() {
    var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
    this.TUTr_Config = TConfig;
  }

  GetSalesOverview() {
    this._HelperService.IsFormProcessing = true;
    this.pData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetoverviewSummary = _Response.Result as any;
          this.RejectedDeal=_Response.Result.Rejected;
          if(this.RejectedDeal!=0)
          {
            this.DisableRejectedDiv=true;
          }
          else{
            this.DisableRejectedDiv=false;
          }
          this._ChangeDetectorRef.detectChanges();
          return;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }


  InitializeDates(): void {
    //#region Sale Dates 
    this._Sale.ActualStartDate = moment().startOf('day');
    this._Sale.ActualEndDate = moment().endOf('day');
    this._Sale.CompareStartDate = moment().subtract(1, 'day').startOf('day');
    this._Sale.CompareEndDate = moment().subtract(1, 'day').endOf('day');
    //#endregion
    //#region Redeem Dates 
    this._Redeem.ActualStartDate = moment().startOf('week').startOf('day');
    this._Redeem.ActualEndDate = moment().endOf('day');
    this._Redeem.CompareStartDate = moment().subtract(1, 'week').startOf('week').startOf('day');
    this._Redeem.CompareEndDate = moment().subtract(1, 'week').endOf('week').endOf('day');
    //#endregion

    //#region Revenue Dates 
    this._Revenue.ActualStartDate = moment().startOf('month').startOf('day');
    this._Revenue.ActualEndDate = moment().endOf('month').endOf('day');
    this._Revenue.CompareStartDate = moment().subtract(1, 'months').startOf('month').startOf('day');
    this._Revenue.CompareEndDate = moment().subtract(1, 'months').endOf('month').endOf('day');
    //implenting later
   // this.RevenuebarChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Revenue.ActualStartDate), moment(this._Revenue.ActualEndDate));
    //#endregion
    //#region Yearly Dates 
    this._Yearly.ActualStartDate = moment().startOf('year').startOf('day');
    this._Yearly.ActualEndDate = moment().endOf('year').endOf('day');

    this._Yearly.CompareStartDate = moment().subtract(1, 'year').startOf('year').startOf('day');
    this._Yearly.CompareEndDate = moment().subtract(1, 'year').endOf('year').endOf('day');
    //#endregion
    var difference = moment().diff(moment(), 'days');

    // implementation pending
    // if (difference <= 1) {
    //   this.CurrentRange = this._Sale;
    //   this._SaleDailyReportGetActualData(this.Types.hour);
    // } else if (difference <= 7) {
    //   this.CurrentRange = this._Redeem;
    //   this._SaleDailyReportGetActualData(this.Types.week);
    // } else if (difference <= 30) {
    //   this.CurrentRange = this._Revenue;
    //   this._SaleDailyReportGetActualData(this.Types.month);
    // } else {
    //   this.CurrentRange = this._Yearly;
    //   this._SaleDailyReportGetActualData(this.Types.year);
    // }
  }


  public _Sale: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,
    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,
    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0
  }
  public _Redeem: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,
    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,
    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0
  }
  public _Revenue: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,
    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,
    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0
  }
  public _Yearly: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,
    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,
    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0
  }

  FetchStores()
  {
    let body = {
      Id: null,
      Sort: null,
      Task: "getstores",
      Location: "api/v3/deals/store/",
      Title: "Available Stores",
      StatusType: "default",
      DefaultSortExpression: "CreateDate desc",
      SearchBaseCondition: `MerchantId = \"${this._HelperService.UserAccount.AccountId}\" `,
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      IsDownload: false,
      Offset: 0,
      Limit: 20,
      SearchCondition:`MerchantId = \"${this._HelperService.UserAccount.AccountId}\" `,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(body.Location, body);
    _OResponse.subscribe(
        _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
               this.StoresList = _Response.Result.Data
            }
            else {
                this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
    });
  }

  exportToExcel()
  {
    if(!this.StoresList.length){
      this._HelperService.NotifyError('No Data to export!')
      return
    }
    const exportFile = this.StoresList.map((res)=>{
      return {
        "DisplayName":res.DisplayName,
        "Address":res.Address,
        "CreateDate":res.CreateDate
      }
    });
    this.excel.exportTableElmToExcel(
      exportFile, "Stores-report" +
      new Date().toJSON().slice(0,10).split("-").reverse().join("/")
    )
  }
  getPurchase()
  {
    let body = {
        Task: "getpurchasehistoryoverview",
        TotalRecords: 0,
        Offset: 0,
        Limit: 10,
        RefreshCount: true,
        SearchCondition: `MerchantId = \"${this._HelperService.UserAccount.AccountId}\" `,
        SortExpression: "CreateDate desc",
        Type: null,
        ReferenceKey: null,
        ReferenceId: 0,
        SubReferenceId: 0,
        IsDownload: false
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, body);
    _OResponse.subscribe(
        _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
               this.PurchaseData= _Response.Result
            }
            else {
                this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
    });

  }


  
}
