import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { StoresComponent } from './stores.component';
import { RouterModule, Routes } from '@angular/router';
import { IconsModule } from 'src/app/shared';
import { NgxPaginationModule } from "ngx-pagination";

const routes: Routes = [
  {
    path: '',
    component: StoresComponent
  }
]
@NgModule({
  declarations: [
    StoresComponent
  ],
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(routes),
    IconsModule,
    NgxPaginationModule
  ]
})
export class StoresModule { }
