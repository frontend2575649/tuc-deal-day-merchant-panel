import { ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ChangeContext } from 'ng5-slider';
import { Observable } from 'rxjs/internal/Observable';
import { HelperService, OList, DataHelperService, OResponse } from '../../utils/index';
import swal from 'sweetalert2';

@Component({
    selector: 'app-stores',
    templateUrl: './stores.component.html',
    styleUrls: ['./stores.component.css']
})
export class StoresComponent implements OnInit {
    @ViewChild('storeAddedSuccessModal', { static: true }) storeAddedSuccessModal: any;
    Data: any;
    OverviewData: any;
    @Input() editStoreItem:any;
    constructor(
        private modalService: NgbModal,
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,

    ) { }

    public ResetFilterControls: boolean = true;
    public AcquirerId = 0;
    ngOnInit() {
        this._HelperService.StopClickPropogation();
        this.TUTr_Setup();
    }

    open(content: any, TItem: any = undefined) {
        this.editStoreItem = TItem;
        this.modalService.open(content, { ariaLabelledBy: 'create-store-modal-body', backdrop: 'static', size: 'lg' })
    }
    TUTr_InvoiceRangeMinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
    TUTr_InvoiceRangeMaxAmount: any = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
    TodayStartTime = null;
    TodayEndTime = null;
    TUTr_InvoiceRange_OnChange(changeContext: ChangeContext): void {
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'Amount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
        this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        this.TUTr_InvoiceRangeMinAmount = changeContext.value;
        this.TUTr_InvoiceRangeMaxAmount = changeContext.highValue;
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'Amount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
        if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        }
        else {
            this.TUTr_Config.SearchBaseConditions.push(SearchCase);
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    TUTr_Setup() {
        this.TUTr_Config = {
            Id: null,
            Sort: null,
            Task: "getstores",
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Store,
            Title: "Available Stores",
            StatusType: "default",
            DefaultSortExpression: "CreateDate desc",
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.AccountId, '='),
            TableFields: [
                {
                    DisplayName: "Display Name",
                    SystemName: "DisplayName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "Contact No",
                    SystemName: "ContactNumber",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "text-center",
                    Show: true,
                    Search: true,
                    Sort: false,
                    ResourceId: null,
                    DefaultValue: "ThankUCash",
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "Email Address",
                    SystemName: "EmailAddress",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: false,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "Store Category",
                    SystemName: "CatagoryName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: false,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
                    SystemName: "CreateDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date text-right",
                    Show: true,
                    IsDateSearchField: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                },
            ]
        };

        this.TUTr_Config = this._DataHelperService.List_Initialize_ForStores(this.TUTr_Config);
        this._HelperService.Active_FilterInit(
            this._HelperService.AppConfig.FilterTypeOption.Stores,
            this.TUTr_Config
        );
        this.TUTr_GetData();
    }
    TUTr_ToggleOption_Date(event: any, Type: any) {
        this.TUTr_ToggleOption(event, Type);
    }
    TUTr_ToggleOption(event: any, Type: any) {
        if (Type == "date") {
            this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
            this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
        }
        if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
            event.data = {
                SalesMin: this.TUTr_InvoiceRangeMinAmount,
                SalesMax: this.TUTr_InvoiceRangeMaxAmount,
            }
        }
        this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);

        this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
        this._DataHelperService.List_OperationsForTransCounts(this.Data, event, Type);

        this.TodayStartTime = this.TUTr_Config.StartTime;
        this.TodayEndTime = this.TUTr_Config.EndTime;
        if (event != null) {
            for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
                const element = this.TUTr_Config.Sort.SortOptions[index];
                if (event.SystemName == element.SystemName) element.SystemActive = true;
                else element.SystemActive = false;
            }
        }

        if (
            (this.TUTr_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)
        ) {
            this.TUTr_GetData();
        }
    }

    timeout: any;
    TUTr_ToggleOptionSearch(event: any, Type: any) {

        clearTimeout(this.timeout);

        this.timeout = setTimeout(() => {
            if (Type == 'date') {
                event.start = this._HelperService.DateInUTC(event.start);
                event.end = this._HelperService.DateInUTC(event.end);
            }
            this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);
            this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
            this._DataHelperService.List_OperationsForTransCounts(this.Data, event, Type);

            this.TodayStartTime = this.TUTr_Config.StartTime;
            this.TodayEndTime = this.TUTr_Config.EndTime;
            if (event != null) {
                for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
                    const element = this.TUTr_Config.Sort.SortOptions[index];
                    if (event.SystemName == element.SystemName) element.SystemActive = true;
                    else element.SystemActive = false;
                }
            }
            if (
                (this.TUTr_Config.RefreshData == true)
                && this._HelperService.DataReloadEligibility(Type)
            ) {
                this.TUTr_GetData();
            }
        }, this._HelperService.AppConfig.SearchInputDelay);


    }

    public TUTr_Config: any;
    public TuTr_Columns =
        {
            Status: true,
            Date: true,
            Account: true,
            AccountName: true,
            Mob: true,
            Biller: true,
            Amount: true,
            Source: true,
            PaymentRef: true
        }
    public TuTr_Filters_List =
        {
            Search: false,
            Date: false,
            Sort: false,
            Status: false,
            Biller: false
        }

    TUTr_GetData() {
        var TConfig = this._DataHelperService.List_GetDataTur(this.TUTr_Config);
        this.TUTr_Config = TConfig;
    }
    deleteStore(TItem){
        swal.fire({
            title: "<h6>Are You Sure You want to delete this store?</h6>",
            customClass: "",
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Continue",
            cancelButtonText: "Cancel"
        }).then((result) => {
            if (result.value) {
                var pData = {
                    Task: this._HelperService.AppConfig.Api.ThankUCash.DeleteStore,
                    ReferenceId: TItem.ReferenceId,
                    ReferenceKey: TItem.ReferenceKey,
                };

                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Store, pData);
                _OResponse.subscribe(
                    _Response => {
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this.TUTr_Setup();
                            this._HelperService.NotifySuccess(_Response.Message);
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);

                    });
            }
        });
    }
    changeStatus(TItem){
        let pData = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.UpdateStoreStatus,
            ReferenceId: TItem.ReferenceId,
            ReferenceKey: TItem.ReferenceKey,
            StatusCode: TItem.StatusCode == this._HelperService.AppConfig.Status.Active ? this._HelperService.AppConfig.Status.Inactive : this._HelperService.AppConfig.Status.Active
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Store, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.TUTr_Setup();
                    this._HelperService.NotifySuccess(_Response.Message);
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }, () => this._HelperService.IsFormProcessing = false);
    }
    reloadList(event){
        if(event && event.reload){
            this._HelperService.NotifySuccess(event.message)
            this.TUTr_Setup();
        }
    }
}
