import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaqsComponent } from './faqs/faqs.component';
import { DownloadsComponent } from './downloads/downloads.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';


const routes: Routes = [
  {
    path: 'faqs',
    component: FaqsComponent
  },
  {
    path:'downloads',
    component:DownloadsComponent
  }
]

@NgModule({
  declarations: [
    FaqsComponent,
    DownloadsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule
  ]
})
export class UtilitiesModule { }
