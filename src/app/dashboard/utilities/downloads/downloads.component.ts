import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { OResponse } from 'src/app/interfaces';
import { HelperService, DataHelperService,PaginationService } from 'src/app/utils';

@Component({
  selector: 'app-downloads',
  templateUrl: './downloads.component.html',
  styleUrls: ['./downloads.component.css']
})
export class DownloadsComponent implements OnInit {

  DownloadData: any;
  public TotalRecords = 0;
  public activePage = 1;
  public pager: any = {};
  public PageRecordLimit:number = 10;
  SearchParameter:string = ''
  constructor(
      public _Router: Router,
      public _ActivatedRoute: ActivatedRoute,
      public _FormBuilder: FormBuilder,
      public _HelperService: HelperService,
      public _DataHelperService: DataHelperService,
      public _ChangeDetectorRef: ChangeDetectorRef,
      private paged: PaginationService
  ){ }
  ngOnInit(): void {
    
    this.fetchRecords()
  }

  fetchRecords(){
     let body = {
      Task: "getuserparameters",
      TotalRecords: 0,
      Offset: this.activePage-1,
      Limit: this.PageRecordLimit,
      RefreshCount: true,
      SearchCondition: `( TypeCode = \"downloads\" ) AND UserAccountKey = \"${this._HelperService.UserAccount.AccountKey}\" `,
      SortExpression: "CreateDate desc",
      Type: null,
      ReferenceKey: null,
      ReferenceId: 0,
      SubReferenceId: 0,
      IsDownload: false
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, body);
    _OResponse.subscribe(
        _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
                this.DownloadData = _Response.Result.Data as any;
                this.TotalRecords = _Response.Result.TotalRecords;
                this.pager = this.paged.getPager(this.TotalRecords, this.activePage, this.PageRecordLimit);
            }
            else {
                this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
    }); 

  }


setPage(page:number){
  this.activePage = page;
  if(page < 1 || page > this.pager.totalPages){
      return
  }
  this.pager = this.paged.getPager(this.TotalRecords, page, this.PageRecordLimit)
  this.fetchRecords()
}
WatchChange(data:any)
{
 this.PageRecordLimit= data
  this.fetchRecords()
}

}
