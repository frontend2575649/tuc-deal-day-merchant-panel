import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { IconsModule } from '../../shared/index';
import { RatingsComponent } from './ratings.component';

const RatingsRoutes = [
  {
    path:'',
    component:RatingsComponent,
  }
]
@NgModule({
  declarations: [
    RatingsComponent
  ],
  imports: [
    CommonModule,
    IconsModule,
    RouterModule.forChild(RatingsRoutes)
  ]
})
export class RatingsModule { }
