import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { addPaymentComponent } from 'src/app/shared/components/add-payment/add-payment.component';
import { withdrawModalComponent } from 'src/app/shared/components/withdraw/withdraw.component';
import { ExcelService, HelperService, OResponse, PaginationService } from 'src/app/utils';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  todayDate:string;
  PurchaseOverview:any;
  dateObj :{startDate:string, endDate:string}
  PaymentData:any;
  public PageRecordLimit:number = 10;
  public TotalRecords = 0;
  public activePage = 1;
  public pager: any = {};
  Offset:number = 0;
  bsRangeValue: any[];
  WalletOverview:any;
  constructor(
    public modalService: NgbModal,
    public _HelperService: HelperService,
    private paged: PaginationService,
    private excel: ExcelService
  )
  {
    this.dateObj= {
      startDate: moment().startOf('month').format('YYYY-MM-DD hh:mm:ss'),
      endDate: moment().format('YYYY-MM-DD hh:mm:ss')
     }
  }
  ngOnInit(): void {
    this.todayDate = moment().format('Do MMMM YYYY')
    this.InitPage()
  }

  withdrawModal() {
    this.modalService.open(withdrawModalComponent, 
      { ariaLabelledBy: 'create-store-modal-body', backdrop: 'static', size: 'md'})
  }

  addPayment() {
    this.modalService.open(addPaymentComponent, 
      { ariaLabelledBy: 'create-store-modal-body', backdrop: 'static', size: 'md' })
  }
  GetOverview()
  {
    let body =   {
      Task: "getpurchasehistoryoverview",
      TotalRecords: 0,
      Offset: 0,
      Limit: 10,
      RefreshCount: true,
      SearchCondition: `MerchantId = \"${this._HelperService.UserAccount.AccountId}\"`,
      SortExpression: "CreateDate desc",
      Type: null,
      ReferenceKey: null,
      ReferenceId: 0,
      SubReferenceId: 0,
      IsDownload: false
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, body);
    _OResponse.subscribe(
        _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
                this.PurchaseOverview = _Response.Result as any;
            }
            else {
                this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
    }); 
  }

  FetchPaymentHistory(){
    let pData = {
      Task: "getpurchasehistory",
      Offset: this.Offset,
      Limit: this.PageRecordLimit,
      TotalRecords:this.TotalRecords,
      RefreshCount: true,
      SearchCondition: `MerchantId = \"${this._HelperService.UserAccount.AccountId}\" AND (DealPurchaseDate > \"${this.dateObj.startDate}\" AND DealPurchaseDate < \"${this.dateObj.endDate}\")`,
      SortExpression: "StartDate desc",
      ReferenceKey: null,
      ReferenceId: 0,
      IsDownload: false
    }
    console.log(pData)
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, pData);
    _OResponse.subscribe(
        _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
                this.PaymentData = _Response.Result.Data as any;
                this.TotalRecords = _Response.Result.TotalRecords;
                this.pager = this.paged.getPager(this.TotalRecords, this.activePage, this.PageRecordLimit);
            }
            else {
                this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
    }); 
  }

  onValueChange(data){
    let parsedDate = data
     this.dateObj = {
      startDate : moment(parsedDate[0]).format('YYYY-MM-DD hh:mm:ss'),
      endDate : moment(parsedDate[1]).format('YYYY-MM-DD hh:mm:ss')
     }
     this._HelperService.NotifyInfo('Refreshing data with selected date range')
     this.FetchPaymentHistory()
  }

  InitPage()
  {
    this.GetOverview()
    this.FetchPaymentHistory()
    this.GetWalletBalance()
  }
  WatchChange(data:any)
  {
   this.PageRecordLimit = data
   this.activePage = 1
   this._HelperService.scrollTop()
   this.Offset = 0;
   this.FetchPaymentHistory()
  }
  exportToExcel()
  {
    if(!this.PaymentData.length){
      this._HelperService.NotifyError('No Data to export!')
      return
    }
    const exportFile = this.PaymentData.map((res)=>{
      return {
        "MerchantDisplayName":res.MerchantDisplayName,
        "Title":res.Title,
        "DealTypeName":res.DealTypeName,
        "MerchantAmount":res.MerchantAmount,
        "Amount":res.Amount,
        "PaybleAmount":res.PaybleAmount,
        "Quantity":res.Quantity,
        "DealPurchaseDate":res.DealPurchaseDate
      }
    });
    this.excel.exportTableElmToExcel(
      exportFile, "Stores-report" +
      new Date().toJSON().slice(0,10).split("-").reverse().join("/")
    )
  }

  GetWalletBalance()
  {
    let body =   {
      Task: "getaccountbalance",
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey:this._HelperService.UserAccount.AccountKey,
      Source: "transaction.source.merchant"
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Balance, body);
    _OResponse.subscribe(
        _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
                this.WalletOverview = _Response.Result as any;
            }
            else {
                this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
    }); 
  }

  setPage(page:number){
    if(page < 1 || page > this.pager.totalPages){
        return
    }
    this.pager = this.paged.getPager(this.TotalRecords, page, this.PageRecordLimit)
    this._HelperService.scrollTop()
    if(page < this.activePage)
    {
      this.Offset -= this.PageRecordLimit
    }else{
      this.Offset += this.PageRecordLimit
    }
    this.activePage = page;
    this.FetchPaymentHistory()
  }
  CastColour(data?:string)
  {
    switch(data)
    {
      case 'Unused':
          return 'text-warning'
      break;
      case 'Expired':
         return 'text-danger'
      break;
      case 'Blocked':
        return 'text-primary'
     break;
      default:
          return 'text-success'
      break
    }
  }
}
