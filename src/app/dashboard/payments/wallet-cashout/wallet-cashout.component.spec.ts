import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WalletCashoutComponent } from './wallet-cashout.component';

describe('WalletCashoutComponent', () => {
  let component: WalletCashoutComponent;
  let fixture: ComponentFixture<WalletCashoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WalletCashoutComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WalletCashoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
