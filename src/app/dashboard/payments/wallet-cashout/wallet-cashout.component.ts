import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { addPaymentComponent } from 'src/app/shared/components/add-payment/add-payment.component';
import { withdrawModalComponent } from 'src/app/shared/components/withdraw/withdraw.component';
import { HelperService, PaginationService, OResponse, ExcelService } from 'src/app/utils';
import { Observable } from 'rxjs/internal/Observable';
import { TransactionViewComponent } from 'src/app/shared/components/transaction-view/transaction-view.component';

@Component({
  selector: 'app-wallet-cashout',
  templateUrl: './wallet-cashout.component.html',
  styleUrls: ['./wallet-cashout.component.css']
})
export class WalletCashoutComponent implements OnInit {
  todayDate:string;
  public PageRecordLimit:number = 10;
  public TotalRecords = 0;
  public activePage = 1;
  public pager: any = {};
  viewType: {SearchCondition:string, ListType: number; Type?: string; };
  CashOutData:any;
  Offset:number = 0;
  WalletOverview:any;
  dateObj :{startDate:string, endDate:string}
  BalanceOverview: any;
  constructor(
    public modalService: NgbModal,
    public _HelperService: HelperService,
    private paged: PaginationService,
    private excel: ExcelService
  ){
    this.dateObj= {
      startDate: moment().startOf('month').format('YYYY-MM-DD HH:mm:ss'),
      endDate: moment().format('YYYY-MM-DD HH:mm:ss')
     }
  }

  ngOnInit(): void {
    this.todayDate = moment().format('Do MMMM YYYY')
    this.InitPage()
  }

  withdrawModal() {
    this.modalService.open(withdrawModalComponent, 
      { ariaLabelledBy: 'create-store-modal-body', backdrop: 'static', size: 'md' })
  }

  addPayment() {
    this.modalService.open(addPaymentComponent, 
      { ariaLabelledBy: 'create-store-modal-body', backdrop: 'static', size: 'md' })
  }
  FetchCashoutHistory(){
    let pData = {
      Task: "getcashouts",
      Offset: this.Offset,
      Limit: this.PageRecordLimit,
      TotalRecords:this.TotalRecords,
      RefreshCount: true,
      SearchCondition: `AccountTypeCode = \"thankumerchant\" AND AccountId = \"${this._HelperService.AppConfig.AccountId}\" AND (StartDate >= \"${this.dateObj.startDate}\" AND StartDate <= \"${this.dateObj.endDate}\") `,
      SortExpression: "StartDate desc",
      ReferenceKey: null,
      ReferenceId: 0,
      IsDownload: false
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.CashoutHistory, pData);
    _OResponse.subscribe(
        _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
                this.CashOutData = _Response.Result.Data as any;
                this.TotalRecords = _Response.Result.TotalRecords;
                this.pager = this.paged.getPager(this.TotalRecords, this.activePage, this.PageRecordLimit);
            }
            else {
                this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
    }); 

  }

  setPage(page:number){
    if(page < 1 || page > this.pager.totalPages){
        return
    }
    this.pager = this.paged.getPager(this.TotalRecords, page, this.PageRecordLimit)
    this._HelperService.scrollTop()
    if(page < this.activePage)
    {
      this.Offset -= this.PageRecordLimit
    }else{
      this.Offset += this.PageRecordLimit
    }
    this.activePage = page;
    this.FetchCashoutHistory()
  }

  WatchChange(data:any)
  {
   this.PageRecordLimit= data
   this.activePage = 1
   this._HelperService.scrollTop()
   this.Offset = 0;
   this.FetchCashoutHistory()
  }

  PreviewItem(item:any){
    const previewData = 
    this.modalService.open(TransactionViewComponent, 
      { ariaLabelledBy: 'create-store-modal-body', backdrop: 'static', size: 'md',windowClass: 'right-modal' })
      previewData.componentInstance.item = item;
  }

  GetOverview()
  {
    let body =   {
      Task: "getcashoutsoverview",
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey:this._HelperService.UserAccount.AccountKey,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.CashOut, body);
    _OResponse.subscribe(
        _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
                this.WalletOverview = _Response.Result as any;
            }
            else {
                this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
    }); 
  }

  GetWalletBalance()
  {
    let body =   {
      Task: "getaccountbalance",
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey:this._HelperService.UserAccount.AccountKey,
      Source: "transaction.source.merchant"
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Balance, body);
    _OResponse.subscribe(
        _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
                this.BalanceOverview = _Response.Result as any;
            }
            else {
                this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
    }); 
  }
  onValueChange(data){
    let parsedDate = data
     this.dateObj = {
      startDate : moment(parsedDate[0]).format('YYYY-MM-DD hh:mm:ss'),
      endDate : moment(parsedDate[1]).format('YYYY-MM-DD hh:mm:ss')
     }
     this._HelperService.NotifyInfo('Refreshing data with selected date range')
     this.FetchCashoutHistory()
  }

  InitPage()
  {
    this.GetOverview()
    this.FetchCashoutHistory()
    this.GetWalletBalance()
  }

  exportToExcel()
  {
    if(!this.CashOutData.length){
      this._HelperService.NotifyError('No Data to export!')
      return
    }
    const exportFile = this.CashOutData.map((res)=>{
      return {
        "BankAccountName":res.BankAccountName,
        "BankName":res.BankName,
        "CreateDate":res.StartDate,
        "BankAccountNumber":res.BankAccountNumber,
        "EmailAddress":res.EmailAddress,
        "StoreDisplayName": res.AccountDisplayName,
        "AccountMobileNumber":res.AccountMobileNumber,
        "Amount":res.Amount,
        "Charge":res.Charge,
        "CommissionAmount":res.CommissionAmount,
        "TotalAmount":res.TotalAmount,
        "StatusName":res.StatusName,
      }
    });
    this.excel.exportTableElmToExcel(
      exportFile, "Cashout-report" +
      new Date().toJSON().slice(0,10).split("-").reverse().join("/")
    )
  }
  
  CastColour(data?:string)
  {
    switch(data)
    {
      case 'Pending':
          return 'text-warning'
      break;
      case 'Rejected':
         return 'text-danger'
      break;
      case 'Blocked':
        return 'text-primary'
     break;
      default:
          return 'text-success'
      break
    }
  }

}
