import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentComponent } from './payment/payment.component';
import { WalletCashoutComponent } from './wallet-cashout/wallet-cashout.component';

import { RouterModule } from '@angular/router';
import { IconsModule } from 'src/app/shared';
import { NgxPaginationModule } from "ngx-pagination";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

const PaymentRoutes = [
  {
    path:'payment',
    component:PaymentComponent,
  },
  {
    path:'wallet-cashout',
    component: WalletCashoutComponent,
  },
]


@NgModule({
  declarations: [
    PaymentComponent,
    WalletCashoutComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    BsDatepickerModule.forRoot(),
    RouterModule.forChild(PaymentRoutes),
    IconsModule,
  ]
})
export class PaymentsModule { }
