import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';

const DashboardRoutes: Routes = [
      {
        path: '',
        redirectTo:'dashboard',
        pathMatch:'full'
      },
      {
        path: 'dashboard',
        component:DashboardComponent,
        loadChildren:()=> import('./dashboard/dashboard.module').then(m=>m.DashboardModule),
      },
      {
        path: 'orders',
        loadChildren:()=> import('./orders/orders.module').then(m=>m.OrdersModule),
      },
      {
        path: 'stores',
        loadChildren:()=> import('./stores/stores.module').then(m=>m.StoresModule),
      },
      {
        path: 'settings',
        loadChildren:()=> import('./settings/settings.module').then(m=>m.SettingsModule),
      },
      {
        path: 'deals',
        loadChildren:()=> import('./deals/deals.module').then(m=>m.DealsModule),
      },
      {
        path: 'ratings',
        loadChildren:()=> import('./ratings/ratings.module').then(m=>m.RatingsModule)
      },
      {
        path: 'pickups',
        loadChildren:()=> import('./pickup/pickup.module').then(m=>m.PickupModule)
      },
      {
        path: 'utilities',
        loadChildren:()=> import('./utilities/utilities.module').then(m=>m.UtilitiesModule)
      },
      {
        path: 'payments',
        loadChildren:()=> import('./payments/payments.module').then(m=>m.PaymentsModule)
      },
];


@NgModule({
  imports: [RouterModule.forChild(DashboardRoutes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
