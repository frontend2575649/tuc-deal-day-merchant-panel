import { Component } from '@angular/core';
import { HelperService } from '../utils';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  constructor(
    public HelperService: HelperService
  ) {
    !this.HelperService.isLoggedIn() ? this.HelperService._Router.navigate(['/']) : ''; 
   }
  ngOnInit(): void {
  }
}
