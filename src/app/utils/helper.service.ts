import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, throwError, Subscription } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { HttpClient, HttpErrorResponse, HttpHeaders, } from "@angular/common/http";
import { OResponse, ONetworkResponse, OAccessUser, OAccessUserAccount, OAccessUserAccountOwner, OAccessUserCountry, OAccessUserLocation, OAccessUserDevice, OAccessUserAccountRole } from "../interfaces";
import { Router } from "@angular/router";
import notie from 'notie';
import { isNull, isUndefined } from "is-what";
import * as lodash from 'lodash';
var changesets = require('json-diff-ts');
import { OStorageContent } from "./object.service";
import { ImageCroppedEvent } from "ngx-image-cropper";
declare var $: any;

import * as moment from 'moment';
import 'moment-timezone';

import * as SystemHelper from "../../assets/js/systemhelper.js";

// declare var moment: any;
declare var $: any;
declare var SystemHelper: any;
@Injectable()
export class HelperService {
  FilterSnapTemprary: any;
  FilterSnap: any;
  CleanConfig: any;
  FilterSnapPrestine: any;
  FilterSnapPrev: any = {};
  CurrentType!: string;
  FilterState: any;
  Active_FilterOptions!: any[];
  SortedTerminals: any = {
    All: [],
    Active: [], //Last Transaction in last 7 days
    Idle: [], //Last transcation  before last 7 days
    Dead: [], //Last transaction before 1 month
    UnUsed: [], //no transaction on this terminal
  };

  POSActivityStatus: any = {
    Active: "active", //Last Transaction in last 7 days
    Idle: "idle", //Last transcation  before last 7 days
    Dead: "dead", //Last transaction before 1 month
    UnUsed: "unused", //no transaction on this terminal
  };
  constructor(
    private _Http: HttpClient,
    public _Router: Router,
  ) {
    this.RefreshHelper();
  }
  public StatusSuccess = "Success";
  public StatusError = "Error";
  public ToggleField = false;
  public FullContainer = false;
  public _Assets = {
    Box: {
      root: "card shadow-none",
      header:
        "card-header d-flex pd-y-15  align-items-center justify-content-between",
      headerTitle: "h6  m-0 mt-1 mb-1",
      filterIcons: "tx-12",
      headerTitleRight: "d-flex",
      headerTitleRightItem: "link-03 lh-0 mg-l-10",
      bodyFilter: "card-body pd-y-10",
      bodyContent: "card-body p-0",
      ShowIcon:
        "wd-35 ht-35 align-items-center justify-content-center op-6 d-none d-sm-flex",
      TextCenter: "text-center",
    },
  };
  public SystemName = [];
  public Center: any = "center";
  public AppConfig = {
    Center: "center",
    Alert_Position: this.Center,
    Alert_Animation: "",
    Alert_AllowAnimation: false,
    Alert_AllowOutsideClick: false,
    Alert_AllowEscapeKey: false,
    DealMinimumLimit: 0,
    DealMaximumLimit: 10000000,
    DealPurchaseMinimumLimit: 0,
    DealPurchaseMaximumLimit: 10000000,
    DefaultStartTimeAll: new Date(2017, 0, 1, 0, 0, 0, 0),
    DefaultEndTimeToday: moment().endOf("day"),
    EmailPattern: '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$',
    PayStackKey: "pk_test_4cca266d686312285a54570e20b46a808ae0d0f6",
    MerchantCode: null as any,
    MerchantPayMentId: null as any,
    MerchantPayMode: null as any,
    Client: null as any,
    HostType: HostType.Live,
    Host: "",
    AppKey: "33f51da41c95415fba82ff8a9f4981a5",
    AppVersionKey: "18da648a59024f2ba93e2e07319b1764",
    ActiveOwnerId: null,
    ActiveOwnerKey: '',
    ActiveReferenceId: null,
    ActiveReferenceKey: null,
    ActiveAccountKey: null,
    ActiveAccountId: null,
    ActiveMerchantReferenceKey: null,
    ActiveMerchantReferenceId: null,
    ActiveReferenceListType: null,
    ActiveReferenceDisplayName: null,
    ActiveBankCode: null,
    AccountId: '',
    AccountKey: '',
    Color_Green: "#22b66e",
    Color_Red: "#f14d4d",
    Color_Blue: "#0168fa",
    Color_Grey: "#d1dade",
    ActivePasswordResetRequest: null as any,
    RangeInvoiceAmountMinimumLimit: 0,
    RangeInvoiceAmountMaximumLimit: 10000000,
    DateYearFormat: "YYYY",
    DateFormat: "DD-MM-YYYY",
    DateTimeFormat: "DD-MM-YYYY hh:mm a",
    TimeFormat: "h:mm a",
    TimeZone: "Africa/Lagos",
    SearchInputDelay: 1000,
    CurrencySymbol: null,
    CurrencySymbolCustom: '&#8358;',
    CurrencySymbolText: '₦',
    CurrencyNotation: 'NGN',
    DropDownListLimit: 1000,
    TablesConfig: {
      DefaultClass: "table  table-hover mb-0",
      InfoClass: "fa fa-info-circle text-warn",
    },
    FilterStates: {
      Default: 'default',
      NoChange: 'nochange',
      Modified: 'modified'
    },
    DataType: {
      Text: "text",
      Number: "number",
      Date: "date",
      Decimal: "decimal",
    },
    StatusList: {
      defaultaccountitem1: 'deal.rejected',
      defaultitem: 0,
      default: [
        {
          id: 0,
          text: "All Status",
          code: "",
        },
        {
          id: 1,
          text: "Inactive",
          code: "default.inactive",
        },
        {
          id: 2,
          text: "Active",
          code: "default.active",
        },
        {
          id: 3,
          text: "Blocked",
          code: "default.blocked",
        },
        {
          id: 4,
          text: "Suspended",
          code: "default.suspended",
        },
      ],
      CashOut: [
        {
          id: 0,
          text: "All Status",
          code: "",
        },
        {
          id: 678,
          text: "Pending",
          code: "cashoutstatus.initialized",
        },
        // {
        //   id: 679,
        //   text: "Processing",
        //   code: "cashoutstatus.processing",
        // },
        {
          id: 680,
          text: "Approved",
          code: "cashoutstatus.success",
        },
        // {
        //   id: 681,
        //   text: "Failed",
        //   code: "cashoutstatus.failed",
        // },
        {
          id: 682,
          text: "Rejected",
          code: "cashoutstatus.rejected",
        }
      ],
      Bulk: [
        {
          id: 0,
          text: "All Status",
          code: "",
        },
        {
          id: 1,
          text: "Pending",
        },
        {
          id: 2,
          text: "Success",
        },
        {
          id: 3,
          text: "Error",
        },
      ],
      Bulkdefaultitem: 3,
      BulkFiles: [
        {
          id: 0,
          text: "All Status",
          code: ''
        },
        {
          id: 1,
          text: "Pending",
          code: 'bulk.pending'

        },
        {
          id: 2,
          text: "Processing",
          code: 'bulk.processing'

        },

        {
          id: 3,
          text: "Complete",
          code: 'bulk.complete'

        },
        {
          id: 4,
          text: "Error",
          code: 'bulk.error'

        },
      ],
      DealCode: [
        {
          id: 0,
          text: "All Status",
          code: "",
        },
        {
          id: 621,
          text: "Used",
          code: "dealcode.used",
        },
        {
          id: 620,
          text: "Unused",
          code: "dealcode.unused",
        },
        {
          id: 622,
          text: "Expired",
          code: "dealcode.expired",
        },
        {
          id: 623,
          text: "locked",
          code: "dealcode.blocked",
        },
      ],
      transactiondefaultitem: 'transaction.success',
      Pendingtransactiondefaultitem: 29,
      OnHoldTransaction: 695,
      transaction: [
        {
          id: 0,
          text: "All Status",
          code: "",
        },
        {
          id: 27,
          text: "Processing",
          code: "transaction.processing",
        },
        {
          id: 28,
          text: "Success",
          code: "transaction.success",
        },
        {
          id: 29,
          text: "Pending",
          code: "transaction.pending",
        },
        {
          id: 631,
          text: "Initialized",
          code: "transaction.initialized",
        },
        {
          id: 30,
          text: "Failed",
          code: "transaction.failed",
        },
        {
          id: 31,
          text: "Cancelled",
          code: "transaction.cancelled",
        },
        {
          id: 32,
          text: "Error",
          code: "transaction.error",
        },
        {
          id: 695,
          text: "On Hold",
          code: "transaction.onhold",
        },
      ],
      invoice: [
        {
          id: 0,
          text: "All Status",
          code: "",
        },
        {
          id: 38,
          text: "Pending",
          code: "invoice.pending",
        },
        {
          id: 39,
          text: "Paid",
          code: "invoice.paid",
        },
        {
          id: 40,
          text: "Cancelled",
          code: "invoice.cancelled",
        },
      ],
      workhorsestatus: [
        {
          id: 0,
          text: "All Status",
          code: "",
        },
        {
          id: 415,
          text: "New",
          code: "workhorsestatus.new",
        },
        {
          id: 416,
          text: "Acknowledged",
          code: "workhorsestatus.acknowledged",
        },
        {
          id: 417,
          text: "Rejected",
          code: "workhorsestatus.rejected",
        },
        {
          id: 418,
          text: "Approved",
          code: "workhorsestatus.approved",
        },
        {
          id: 419,
          text: "In progress",
          code: "workhorsestatus.inprogress",
        },
        {
          id: 420,
          text: "Finalizing",
          code: "workhorsestatus.finalizing",
        },
        {
          id: 421,
          text: "Beta Testing",
          code: "workhorsestatus.betatesting",
        },
        {
          id: 422,
          text: "Ready To Release",
          code: "workhorsestatus.readytorelease",
        },
        {
          id: 423,
          text: "Released",
          code: "workhorsestatus.released",
        },
        {
          id: 424,
          text: "Cancelled",
          code: "workhorsestatus.cancelled",
        },
        {
          id: 437,
          text: "Duplicate",
          code: "workhorsestatus.duplicate",
        },
      ],
      productcodestatus: [
        {
          id: 0,
          text: "All Status",
          code: "",
        },
        {
          id: 461,
          text: "Unused",
          code: "productcodestatus.unused",
        },
        {
          id: 463,
          text: "Used",
          code: "productcodestatus.used",
        },
        {
          id: 462,
          text: "Blocked",
          code: "productcodestatus.blocked",
        },
        {
          id: 464,
          text: "Expired",
          code: "productcodestatus.expired",
        },
      ],
      Campaign: [
        {
          id: 0,
          text: "All Status",
          code: "",
        },
        {
          id: 710,
          text: "Creating",
          code: "creating",
        },
        {
          id: 711,
          text: "Deleting",
          code: "deleting",
        },
        {
          id: 712,
          text: "Draft",
          code: "draft",
        },
        {
          id: 713,
          text: "Waitingforreview",
          code: "waitingforreview",
        },
        {
          id: 714,
          text: "Inreview",
          code: "inreview",
        },
        {
          id: 715,
          text: "Approved",
          code: "approved",
        },
        {
          id: 716,
          text: "Rejected",
          code: "rejected",
        },
        {
          id: 717,
          text: "Published",
          code: "published",
        },
        {
          id: 718,
          text: "Running",
          code: "running",
        },
        {
          id: 719,
          text: "Paused",
          code: "paused",
        },
        {
          id: 720,
          text: "Cancelled",
          code: "cancelled",
        },
        {
          id: 721,
          text: "Expired",
          code: "expired",
        },
        {
          id: 722,
          text: "Completed",
          code: "completed",
        },

      ],
      orderstatus: [
        {
          id: 0,
          text: 'All Status',
          code: ''
        },
        {
          id: 486,
          text: 'New',
          code: 'orderstatus.new'
        },
        {
          id: 487,
          text: 'Pending Confirmation',
          code: 'orderstatus.pendingconfirmation'
        },
        {
          id: 488,
          text: 'Confirmed',
          code: 'orderstatus.confirmed'
        },
        {
          id: 489,
          text: 'Preparing',
          code: 'orderstatus.preparing'
        },
        {
          id: 490,
          text: 'Ready To Deliver',
          code: 'orderstatus.ready'
        },
        {
          id: 491,
          text: 'Ready To Pickup',
          code: 'orderstatus.readytopickup'
        },
        {
          id: 492,
          text: 'Out For Delivery',
          code: 'orderstatus.outfordelivery'
        },
        {
          id: 493,
          text: 'Delivery Failed',
          code: 'orderstatus.deliveryfailed'
        },
        {
          id: 494,
          text: 'Cancelled By User',
          code: 'orderstatus.cancelledbyuser'
        },
        {
          id: 495,
          text: 'Cancelled By Seller',
          code: 'orderstatus.cancelledbyseller'
        },
        {
          id: 511,
          text: 'Cancelled By System',
          code: 'orderstatus.cancelledbysystem'
        },
        {
          id: 512,
          text: 'Delivered',
          code: 'orderstatus.delivered'
        }

      ],
      Product: [
        {
          id: 547,
          text: 'Inactive',
          code: 'product.inactive'
        },
        {
          id: 548,
          text: 'Active',
          code: 'product.active'
        },
        {
          id: 549,
          text: 'Out Of Stock ',
          code: 'product.outofstock'
        },
        {
          id: 550,
          text: 'Not Available',
          code: 'product.notavailable'
        },
      ],
      Terminal: [
        {
          id: 0,
          text: 'All',
          code: ''
        },
        {
          id: 547,
          text: 'Active',
          code: 'terminal.active'
        },
        {
          id: 548,
          text: 'Idle',
          code: 'terminal.idle'
        },
        {
          id: 549,
          text: 'Dead',
          code: 'terminal.dead'
        },
        {
          id: 550,
          text: 'Unused',
          code: 'terminal.unused'
        },
      ],
      DateRange: [
        {
          id: 0,
          text: 'day'
        }, {
          id: 1,
          text: 'week'
        }, {
          id: 2,
          text: 'month'
        },
        {
          id: 3,
          text: 'year'
        }
      ],
    },
    ListRecordLimit: [10, 25, 30, 50, 90, 100],
    ClientHeader: {
      "Content-Type": "application/json; charset=utf-8",
      hcak: "",
      hcavk: "",
      hctk: null as any,
      hcudlt: "",
      hcudln: "",
      hcuak: "",
      hcupk: "",
      hcudk: "",
      hcuata: "",
      hcuatp: "",
    },
    HostConnect: "",
    Storage: {
      Device: "hcscd",
      Credentials: "credentials",
      Account: "hca",
      OReqH: "hcscoreqh",
      Location: "hcsl",
      Permissions: "hcap",
      DataReference: 'hcreference',
      ActiveCustomer: "hcactivecustomer",
    },
    HelperTypes: {
      MerchantCategories: "merchantcategories",
    },
    Status: {
      Active: "default.active",
      Inactive: "default.inactive",
    },
    NetworkLocation: {
      V1: {
        System: "api/v1/system/",
        ThankU: "api/v1/thanku/",
        acquirer: "api/v1/acquirer/",
      },
      V2: {
        System: "api/v2/system/",
        ThankU: "api/v2/thankucash/",
      },
      V3: {
        Deals: "api/v3/plugins/deals/",
        State: "api/v3/tuc/state/",
        City: "api/v3/tuc/city/",
        Account: "api/v3/merchant/accounts/",
        Accounts: {
          Manage: "api/v3/acc/manage/"
        },
        Lsshipment: "api/v3/plugins/lsshipment/",
        SystemMange: "api/v3/system/manage/",
        Op: "api/v3/tuc/op/",
        MOnBoard: "api/v3/deals/account/",
        Store: 'api/v3/deals/store/',
        SystemFaq: "api/v3/sys/faq/",
        Banks: "api/v3/tuc/bankop/",
        CashOut: "api/v3/merchant/payments/",
        CashoutHistory: "api/v3/con/co/",
        Balance:"api/v3/merchant/subscription/"
      }
    },
    AccountType: {
      Merchant: "thankumerchant",
      SubAccount: "merchantsubaccount",
      Customer: "customer",
      Store: "merchantstore",
    },
    MerchantPanelURL: '',
    Pages: {
      System: {
        Login: "/auth/login",
        ForgotPassword: '/auth/forgotpassword',
        verifymail: "/account/verifyemail",
        NotFound: "/system/notfound",
        Register: "/auth/register",

      },
      ThankUCash: {
        Dashboard: {
          PTSP: "/provider/dashboard",
          Merchant: "/m/dashboard",
          MerchantSales: "/m/salesdashboard",
          MerchantRewards: "/m/rewardsdashboard",
          Acquirer: "/acquirer/dashboard",
          Rm: "/rm/dashboard",
          Store: "/store/dashboard",
          Console: "/system/dashboard",
          DealsDashboard: "/m/deal/dashboard",
          LoyaltyDashboard: "/m/loyalty/dashboard",
        },
        PanelSMS: {
          CustomerGroup: "/console/merchant/customergroup",
          AllCustomers: "/console/merchant/customergroup/allcustomers",
          AllSmsCampaign: "/console/smscampaigns",
          AllSmsOverview: "/console/smsoverview",
          AddCampaign: "/console/addcampaigns",
          SmsOverview: "/console/smsoverview",
          CampaignDetail: "/console/campaigndetail",
          Merchant: "/console/merchantsms",
          SmsCampaign: "/console/smscampaign/analytics",
          CustomersCampaign: "/console/smscampaign/customerscampaign",

          MerchantCredits: "/console/smsparentmerchantdetail/smsmerchantdetail/merchantcredits",
          MerchantGroups: "/console/smsparentmerchantdetail/smsmerchantdetail/merchantgroups",
          MerchantOverview: "/console/smsparentmerchantdetail/smsmerchantdetail/msmsoverview",
          MerchantCampaigns: "/console/smsparentmerchantdetail/smsmerchantdetail/msmscampaigns",
          CreditH: "/console/credithistory",
        },
        PTSP: {
          Terminals: "/provider/terminals",
          Merchants: "/provider/merchants",
          RewardHistory: "/provider/rewardhistory",
          RedeemHistory: "/provider/redeemhistory",
          CommissionHistory: "/provider/commissionhistory",
          RequestHistory: "/provider/requesthistory",
          Profile: "/provider/profile",
        },
        PanelConsole: {
          FAQs: "/m/faqs",
          Profile: "/system/profile",
          LiveTerminals: "/system/liveterminals",
          Merchants: "/system/accounts/merchants",
          Stores: "/system/accounts/stores",
          Terminals: "/system/accounts/terminals",
          Acquirers: "/system/accounts/acquirers",
          Customers: "/system/accounts/customers",
          SuspendedCustomers: "/system/accounts/suspendedcustomers",
          BlockedCustomers: "/system/accounts/blockedcustomers",
          PgAccounts: "/system/accounts/gatewayproviders",
          PosAccounts: "/system/accounts/terminalprovider",
          Cashier: "/system/account/cashier",
          Transactions: "/system/transactions/all",
          Reward: "/system/transactions/rewardhistory",
          RewardClaim: "/system/transactions/rewardclaimhistory",
          Redeem: "/system/transactions/redeemhistory",
          Sale: "/system/sales/salehistory",
          Downloads: "/system/downloads",
          AppUser: {
            Dashboard: "/system/account/customer/dashboard",
            Reward: "/system/account/customer/transactions/rewardhistory",
            RewardClaim:
              "/system/account/customer/transactions/rewardclaimhistory",
            Redeem: "/system/account/customer/transactions/redeemhistory",
            Sale: "/system/account/customer/sales/salehistory",
            ManageAccount: "/system/account/customer/manage",
          },
          Merchant: {
            Dashboard: "/system/account/merchant/dashboard",
            Dashboards: {
              TodaysOverview: "/system/account/merchant/todaysoverview",
              Sales: "/system/account/merchant/salesdashboard",
              Rewards: "/system/account/merchant/rewardsdashboard",
            },
            // ManageAccount: "/system/account/merchant/manage",
            ManageAccount: {
              Overview: "/system/account/merchantmanager/overview",
              BusinessProfile:
                "/system/account/merchantmanager/businessprofile",
              Account: "/system/account/merchantmanager/account",
              Terminals: "/system/account/merchantmanager/terminals",
              Stores: "/system/account/merchantmanager/stores",
              SubAccounts: "/system/account/merchantmanager/managers",
              Cashiers: "/system/account/merchantmanager/cashiers",
              Configurations: "/system/account/merchantmanager/configurations",
              Products: "/system/account/merchantmanager/products",
            },
            SubAccounts: {
              Customers: "/system/account/merchant/customers",
              Stores: "/system/account/merchant/stores",
              Terminals: "/system/account/merchant/terminals",
              Managers: "/system/account/merchant/managers",
              Cashiers: "/system/account/merchant/cashiers",
            },
            GiftPoints: "/system/account/merchant/transactions/giftpoints",
            Product: {
              GiftCards: {
                QuickGiftCards:
                  "/system/account/merchant/product/guickgiftcards",
                AddBulkGiftCards:
                  "/system/account/merchant/product/creategiftcard",
                Wallet: "/system/account/merchant/product/giftcards/wallet",
                Manage: "/system/account/merchant/product/giftcards/manage",
                Codes:
                  "/system/account/merchant/product/giftcards/saleshistory",
                GiftCards: "/system/account/merchant/product/giftcards",
              },
            },
            Reward: "/system/account/merchant/transactions/rewardhistory",
            RewardClaim:
              "/system/account/merchant/transactions/rewardclaimhistory",
            Redeem: "/system/account/merchant/transactions/redeemhistory",
            Sale: "/system/account/merchant/sales/salehistory",
          },
          Store: {
            Dashboard: "/system/account/store/dashboard",
            ManageAccount: "/system/account/store/manage",
            Reward: "/system/account/store/transactions/rewardhistory",
            RewardClaim:
              "/system/account/store/transactions/rewardclaimhistory",
            Redeem: "/system/account/store/transactions/redeemhistory",
            Sale: "/system/account/store/sales/salehistory",
            SubAccounts: {
              Customers: "/system/account/store/customers",
              Terminals: "/system/account/store/terminals",
            },
          },
          Acquirer: {
            Dashboard: "/system/account/acquirer/dashboard",
            ManageAccount: "/system/account/acquirer/manage",
            Reward: "/system/account/acquirer/transactions/rewardhistory",
            RewardClaim:
              "/system/account/acquirer/transactions/rewardclaimhistory",
            Redeem: "/system/account/acquirer/transactions/redeemhistory",
            Sale: "/system/account/acquirer/sales/salehistory",
            SubAccounts: {
              Terminals: "/system/account/acquirer/terminals",
            },
          },
          PgAccount: {
            Dashboard: "/system/account/gatewayprovider/dashboard",
            ManageAccount: "/system/account/gatewayprovider/manage",
            Reward:
              "/system/account/gatewayprovider/transactions/rewardhistory",
            RewardClaim:
              "/system/account/gatewayprovider/transactions/rewardclaimhistory",
            Redeem:
              "/system/account/gatewayprovider/transactions/redeemhistory",
            Sale: "/system/account/gatewayprovider/sales/salehistory",
          },
          PosAccount: {
            Dashboard: "/system/account/terminalprovider/dashboard",
            ManageAccount: "/system/account/terminalprovider/manage",
            Reward:
              "/system/account/terminalprovider/transactions/rewardhistory",
            RewardClaim:
              "/system/account/terminalprovider/transactions/rewardclaimhistory",
            Redeem:
              "/system/account/terminalprovider/transactions/redeemhistory",
            Sale: "/system/account/terminalprovider/sales/salehistory",

            SubAccounts: {
              Terminals: "/system/account/terminalprovider/terminals",
            },
          },
          Terminal: {
            Dashboard: "/system/account/terminal/dashboard",
            ManageAccount: "/system/account/terminal/manage",
            Reward: "/system/account/terminal/transactions/rewardhistory",
            RewardClaim:
              "/system/account/terminal/transactions/rewardclaimhistory",
            Redeem: "/system/account/terminal/transactions/redeemhistory",
            Sale: "/system/account/terminal/sales/salehistory",
            SubAccounts: {
              Customers: "/system/account/terminal/customers",
            },
          },
          UserOnboarding: {
            Merchant: "/system/onboarding/merchant",
            PendingMerchant: "/system/onboarding/pendingmerchantonboarding",
            PendingMerchantManager: "/system/onboarding/pendingmerchantmanager",
            Customer: "/system/onboarding/customer",
            Acquirer: "/system/onboarding/acquirer",
            PgAccount: "/system/onboarding/gatewayprovider",
            PosAccount: "/system/onboarding/terminalprovider",
          },

          Invoices: {
            All: "/system/invoices",
            Details: "/system/invoice",
            Invoice: "/m/bnpl/invoice",
          },
          Analytics: {
            Home: "/system/analytics/home",
            TodaysOverview: "/system/analytics/todaysoverview",
            SystemOverview: "/system/analytics/systemoverview",
            GrowthMap: "/system/analytics/growthmap",
            CustomerAnalytics: "/system/analytics/customeranalytics",
            Rewards: {
              DailyRewards: {
                Merchants: "/system/analytics/dailyrewards/merchants",
                Stores: "/system/analytics/dailyrewards/stores",
                Terminals: "/system/analytics/dailyrewards/terminals",
              },
              RewardsSummary: {
                Merchants: "/system/analytics/rewardssummary/merchants",
                Stores: "/system/analytics/rewardssummary/stores",
                Terminals: "/system/analytics/rewardssummary/terminals",
              },
            },
            Sales: {
              // DailySales: "/system/analytics/dailysales",

              DailySales: {
                Merchants: "/system/analytics/dailysales/merchants",
                Stores: "/system/analytics/dailysales/stores",
                Terminals: "/system/analytics/dailysales/terminals",
              },
              SalesSummary: {
                Merchants: "/system/analytics/salessummary/merchants",
                Stores: "/system/analytics/salessummary/stores",
                Terminals: "/system/analytics/salessummary/terminals",
              },
            },
          },
          Administration: {
            AppRequesthistory: "/m/app/requesthistory",
            AppSlider: "/system/control/appslider",
            AdministrationHome: "/system/control/administration",
            ApiRequestHistory: "/system/control/apirequesthistory",
            Categories: "/system/control/categories",
            Roles: "/system/control/roles",
            AdminFeatures: "/system/control/features",
            ResponseCodes: "/system/control/responsecodes",
            SystemLog: "/system/control/systemlog",
            Apps: "/system/control/apps",
            Configurations: "/system/control/configurations",
            ConfigurationManager: "/system/control/configurationmanager",
            Verifications: "/system/control/verifications",
            UserSessions: "/system/control/usersessions",
            AdminUsers: "/system/control/adminusers",
            AddAdminUsers: "/system/control/addadminuser",
            AdminUser: {
              Dashboard: "/system/control/adminuser/dashboard",
              Sessions: "/system/control/adminuser/loginhistory",
            },
            WorkHorse: {
              SystemUpdates: "/system/control/workhorse/systemupdates",
              UpdateFeatureRequests:
                "/system/control/workhorse/updatefeaturerequests",
            },
          },
          Deals: {
            AddDeals: "/m/deal/adddeals",
            Deals: "/m/deal/deals",
            Deals1: "/m/deals",
            Deal: "/m/deal",
            DealsOrders: "/m/deal/dealsorders",
            FlashDeals: "/m/deal/flashdeals",
            EditDeal: "/m/deal/editdeal",
            DealsOverview: "/m/deal/dealsoverview",
            DealsOvervieww: "/m/deal/dealsovervieww",
            Analytics: "/console/deal/analytics",
            SoldHistory: "/m/deal/soldhistory",
            RedeemHistory: "/m/deal/dealredeemhistory",
            DealOrderHistory: "/m/dealorderhistory",
            AllPickups: {
              PendingPickup: '/m/deal/allpickup/pendingpickup',
              CancelledPickup: "/m/deal/allpickup/cancelled",
              ReturnedRequest: "/m/deal/allpickup/returnedrequest"
            },
            RedeemedOrders: "/m/deal/redeemedorders",
            ReturnedOrders: "/m/deal/returnedorders",
            CancelledOrders: "/m/deal/cancelledorders",

          },
          Delivery: {

            OngoingOrders: {
              NewOrders: "/m/deal/ongoingorders",
              Processing: "/m/ongoingorders/processing",
              Ready: "/m/ongoingorders/ready",
              Dispatch: "/m/ongoingorders/dispatch",
              Failed: "/m/ongoingorders/failed",
              Cancelled: "/m/ongoingorders/cancelled",
              ReturnRequested: "/m/ongoingorders/returnrequested"
            },

          },
        },
        Partner: {
          BulkMerchantUpload: "/acquirer/merchantupload",
          BulkMerchants: "/acquirer/bulkmerchants",
          BulkCustomers: "/m/customers",
          customerrewards: "/m/customerrewards",
          Customers: "/m/allcustomers",
          AllCustomers: "/m/allmcustomers",
          bulkcustomerrewards: "/m/bulkcustomerrewards",
          BulkCustomerUpload: "/m/bulkcustomerupload",

        },
        PanelAcquirer: {
          SalesHistory: "/acquirer/saleshistory",
          Activity: "/acquirer/activity",
          SalesReport: "/acquirer/salesreport",
          RMReport: "/acquirer/rmreports",
          SubAccounts: "/acquirer/subaccounts",
          Terminals: "/acquirer/terminals",
          Campaigns: {
            Dashboard: "/acquirer/campaign/dashboard",
            Campaign: "/acquirer/campaign",
            Campaigns: "/acquirer/campaigns",
            AddCampaign: "/acquirer/addcampaign",
            Sales: "/acquirer/campaign/sales/saleshistory",
          },
          PanelMerchant: {
            SalesHistory: "/acquirer/saleshistory",
            PendingTransaction: "/acquirer/pendingtransaction",



            Activity: "/acquirer/activity",
            SalesReport: "/acquirer/salesreport",
            RMReport: "/acquirer/rmreports",
            SubAccounts: "/acquirer/subaccounts",
            Terminals: "/acquirer/terminals",
            Apps: "/console/apps",
            App: "/console/apps/app",

          },
          Accounts: {
            Branch: "/acquirer/branch",
            Rms: "/acquirer/rms",
            Subadmins: "/acquirer/subadmins",
          },
          AddAccount: {
            AddRm: "/acquirer/addrm",
            AddBranch: "/acquirer/addbranch",
          },
          AccountDetails: {
            BranchDetails: "/acquirer/branchdetails",
            Rm: "/acquirer/rm",
            SubAdminDetails: "/acquirer/subadmindetails",
            Branch: "/acquirer/branchs",
          },

          Analytics: {
            Sales: "/acquirer/salesanalytics",
          },
          Live: {
            Terminals: "/acquirer/live/terminals",
            Merchants: "/acquirer/live/merchants",
          },

          Merchants: "/acquirer/merchants",
          MerchantOnboarding: "/acquirer/merchantonboarding",
          MerchantUpload: "/acquirer/merchantupload",
          MerchantManager: "/acquirer/merchantmanager",
          AppUser: {
            Dashboard: "/acquirer/customer/dashboard",
            UpdateStore: "/acquirer/customer/updatestore",
            RewardHistory: "/acquirer/customer/rewardhistory",
            TUCPlusRewardHistory: "/acquirer/customer/tucplusrewardhistory",
            TUCPlusRewardClaimHistory:
              "/acquirer/customer/tucplusrewardclaimhistory",
            RedeemHistory: "/acquirer/customer/redeemhistory",
          },

          TerminalRewardHistory: "/acquirer/terminal/rewardhistory",
          TerminalRedeemHistory: "/acquirer/terminal/redeemhistory",
          Pssp: "/acquirer/gateways",

          // PaymentsHistory: "/acquirer/paymentshistory",

          TUCPlusRewardHistory: "/acquirer/tucplusrewardhistory",
          TUCPlusRewardClaimHistory: "/acquirer/tucplusrewardclaimhistory",
          RewardHistory: "/acquirer/rewardhistory",
          RedeemHistory: "/acquirer/redeemhistory",

          CommissionHistory: "/acquirer/commissionhistory",
          DailyCommissionHistory: "/acquirer/dailycommissionhistory",
          StoreSale: "/acquirer/storesale",
          Profile: "/acquirer/profile",

          RewardInvoices: "/acquirer/rewardinvoices",
          RedeemInvoices: "/acquirer/redeeminvoices",
        },
        PanelRm: {
          Live: {
            Terminals: "/rm/live/terminals",
          },
          Accounts: {
            Terminals: "/rm/terminals",
            Stores: "/rm/stores",
          },
        },
        PanelMerchant: {
          Banks: "/m/bankmanager",
          AddBanks: "/m/banks",
          CashOuts: "/m/cashouts",
          Wallet: "/m/wallet",
          RewardWallet: "/m/wallet/reward",
          RedeemWallet: "redeem",
          RedeemWalletDeals: "/m/dealswallet/redeem",
          RedeemWalletBNPL: "/m/bnplwallet/redeem",
          BulkTerminal: "/m/bulkterminals",
          Transaction: "/m/sales/saleshistory",
          PendingTransaction: "/m/sales/pendingtransaction",

          TransactionHistory: {
            PurchaseHistory: '/m/transaction/saleshistory',
            PendingHistory: '/m/transaction/pendingtransaction',
          },


          RewardHistory: "/m/loyalty/sales/rewardhistory",
          PendingRewardHistory: "/m/loyalty/sales/pendingrewardhistory",
          RedeemHistory: "/m/loyalty/sales/redeemhistory",
          RewardClaim: "/m/sales/rewardclaim",
          Bank: "bank",
          Upgrade: "upgrade",
          MerchantSubscription: "/account/updatesubscription",
          MerchantTSubscription: "/account/updatetsubscription",
          UpgradePlan: "/account/upgradeplan",
          MerchantTSubscriptionOnBoard: "/account/updatetsubscriptiononboard",
          AllPlans: "/account/allplans",
          Apps: "/m/apps",
          App: "/m/apps/app",

          System: {
            Register: "/account/register",
            MerchantRegister: "/account/merchantregister",
          },
          Product: {
            Order: "orders",
            OngoingOrder: "ongoingorders",
            OrderDetail: "/m/orderhistory",
            Products: "products",
            AddProducts: "/m/addproduct",
            EditProducts: "/m/editproduct",
            Inventory: "/m/product/inventory",
            OverviewProduct: "/m/product/dashboard",
            Variants: "/m/product/variants"
          }
          ,
          MyBuisness: {
            SalesTrends: "/m/salestrend",
            SalesSummary: "/m/salesummary",
          },
          GiftPoints: {
            Dashboard: "/m/giftpoints",
            Reward: "/m/giftpointsales/rewardhistory",
            History: "/m/giftpointsales/balancehistory",
            Redeem: "/m/giftpointsales/redeemhistory",

          },
          GiftCard: {
            Dashboard: "/m/giftcard",
            Reward: "/m/giftcardsales/rewardhistory",
            History: "/m/giftcardsales/balancehistory",
            Redeem: "/m/giftcardsales/redeemhistory",

          },
          Accounts: {
            Subadmins: "/m/invoices",
            Stores: "stores",
            Terminals: "settings/terminals",
            RewardPercentage: "rewardpercentage",
            AccSettingRewardPercent: "settings/rewardpercentage",
            SubAccounts: "settings/subaccounts",
            TopUp: "settings/topup",
            PaymentMethod: "settings/banks",
            Cashouts: "settings/cashouts",
            AppsKeys: "settings/apps",
            EditProfile: "/m/settings/editprofile",
          },
          CashierDetails: {
            SalesHistory: "/m/cashier/sales/saleshistory",
            Overview: "/m/cashier/dashboard",
            Terminals: "/m/cashier/terminals",
            Credentials: "/m/cashier/credentials",

            CashierAnalytics: "/m/cashieranalyis",



          },
          SubAccountDetails: {
            SalesHistory: "/m/subaccount/sales/saleshistory",

          },
          Campaign: {
            Dashboard: "/m/campaign/dashboard",
            SalesHistory: "/m/campaign/sales/saleshistory",
          },
          Loyality: {
            Overview: "/m/overview",
            Visits: "/m/loyalty/visits",
            Customers: "/m/loyalty/customers",
            Campaign: "/m/campaigns",
            CustomerDashboard: "/m/customer",
            RedeemHistory: "/m/redeemhistory",
            RewardHistory: "/m/rewardhistory",
            MySales: '/m/loyalty/mysales',
            Subscription: '/m/loyalty/subscription'
          },
          AccountSettings: {
            Cashiers: 'settings/cashiers',
          },
          SurveyCampaign: "/m/loyalty/surveycampaigns",
          SMSMarketing: "/m/loyalty/smscampaigns",
          SmsCampaign: "/m/smscampaign/analytics",
          AddCampaign: "/m/loyalty/addsmscampaigns",
          CustomersCampaign: "/m/smscampaign/customerscampaign",
          EmailMarketing: "/m/loyalty/emailcampaigns",
          MerchantUpload: "/m/merchantupload",
          Settings: "settings/stores",
          AccountSetting: "/m/settings",
          Invoices: "/m/invoices",
          Invoice: "/m/invoice",
          Cashiers: "/m/cashiers",
          Cashier: "/m/cashier",
          Managers: "/m/managers",
          Stores: "/m/stores",
          Terminals: "/m/terminals",
          Customers: "/m/customers",
          // GiftPoints: "/m/giftpoints",
          GiftCards: "/m/giftcards",
          Products: "/m/products",
          Downloads: "/m/downloads",
          FAQs: "/m/faqs",
          FAQCategories: "/m/faqcategories",
          Profile: "/m/profile",
          Analytics: {
            StoresTucSummary: "/m/analytics/storestucsummary",
            Live: "/m/live",
            TerminalLive: "/m/analytics/terminalstracker",
            DailySales: "/m/analytics/dailysales",
            StoreBankCollection: "/m/analytics/bankcollections",
          },
          Transactions: {
            Sales: "/m/transactions/saleshistory",
            Rewards: "/m/transactions/rewards",
            RewardClaims: "/m/transactions/rewardclaims",
            Redeem: "/m/transactions/redeem",
          },
          Store: {
            Dashboard: "/m/store/dashboard",
            Terminals: "/m/store/terminals",
            Cashiers: "/m/store/cashiers",
            RewardHistory: "/m/store/transactions/rewardhistory",
            RewardClaimHistory: "/m/store/transactions/rewardclaimhistory",
            RedeemHistory: "/m/store/transactions/redeemhistory",
            SalesHistory: "/m/store/sales/saleshistory",
            // UpdateStore: "/merchant/store/updatestore",
            // RewardHistory: "/merchant/store/rewardhistory",
            // TUCPlusRewardHistory: "/merchant/store/tucplusrewardhistory",
            // TUCPlusRewardClaimHistory:"/merchant/store/tucplusrewardclaimhistory",
            // RedeemHistory: "/merchant/store/redeemhistory"
          },
          Terminal: {
            Dashboard: "/m/terminal/dashboard",
            RewardHistory: "/m/terminal/transactions/rewardhistory",
            RewardClaimHistory: "/m/terminal/transactions/rewardclaimhistory",
            RedeemHistory: "/m/terminal/transactions/redeemhistory",
            SalesHistory: "/m/terminal/sales/saleshistory",
          },
          Customer: {
            Dashboard: "/m/loyalty/customer/dashboard",
            RewardHistory: "/m/loyalty/customer/sales/rewardhistory",
            RewardClaimHistory: "/m/loyalty/customer/sales/rewardclaimhistory",
            RedeemHistory: "/m/loyalty/customer/sales/redeemhistory",
            SalesHistory: "/m/loyalty/customer/sales/salehistory",
            DealHistory: {
              PurchaseHistory: '/m/loyalty/customer/sales/dealhistory/purchasehistory',
              RedeemHistory: '/m/loyalty/customer/sales/dealhistory/redeemhistory',
            },
          },
          // Customer: "/merchant/customer/dashboard",
          SubAccounts: "/merchant/subaccounts",
          // RewardHistory: "/merchant/rewardhistory",
          RewardClaimHistory: "/merchant/rewardclaimhistory",
          // RedeemHistory: "/merchant/redeemhistory",
          SalesHistory: "/merchant/saleshistory",
          AppUser: {
            Dashboard: "/merchant/customer/dashboard",
            RewardHistory: "/merchant/customer/rewardhistory",
            RewardClaimHistory: "/merchant/customer/rewardclaimhistory",
            RedeemHistory: "/merchant/customer/redeemhistory",
            SalesHistory: "/merchant/customer/saleshistory",
            // Dashboard: "/merchant/customer/dashboard",
            // UpdateStore: "/merchant/customer/updatestore",
            // RewardHistory: "/merchant/customer/rewardhistory",
            // TUCPlusRewardHistory: "/merchant/customer/tucplusrewardhistory",
            // TUCPlusRewardClaimHistory:
            //   "/merchant/customer/tucplusrewardclaimhistory",
            // RedeemHistory: "/merchant/customer/redeemhistory"
          },
          Pssp: "/merchant/gateways",
          CommissionHistory: "/merchant/commissionhistory",
          StoreSale: "/merchant/storesale",
          Merchants: "merchants",
          // Profile: "/merchant/profile",
          RewardInvoices: "/merchant/rewardinvoices",
          RedeemInvoices: "/merchant/redeeminvoices",

          bnpl: {
            BnplOverview: "/m/bnpl/overview",
            PendingLoans: "/m/bnpl/pending-loans",
            RedeemedLoans: "/m/bnpl/redeemed-loans",
            Settlements: "/m/bnpl/settlements",
            PendingSettlements: "/m/bnpl/settlements/pending",
            ClearSettlements: "/m/bnpl/settlements/clear",
            Transaction: "/m/bnpl/settlements/transaction"
          }
        },
        PanelMerchantRewards: {
          Invoices: "/mr/invoices",
          Invoice: "/mr/invoice",
          Cashiers: "/mr/cashiers",
          Cashier: "/mr/cashier",
          Managers: "/mr/managers",

          Transactions: {
            Sales: "/mr/transactions/saleshistory",
          },
          Stores: "/mr/stores",
          Store: {
            Dashboard: "/merchant/store/dashboard",
            Terminals: "/merchant/store/terminals",
            RewardHistory: "/merchant/store/rewardhistory",
            RewardClaimHistory: "/merchant/store/rewardclaimhistory",
            RedeemHistory: "/merchant/store/redeemhistory",
            SalesHistory: "/merchant/store/saleshistory",
          },
          Terminals: "/m/terminals",
          Terminal: {
            Dashboard: "/merchant/terminal/dashboard",
            RewardHistory: "/merchant/terminal/rewardhistory",
            RewardClaimHistory: "/merchant/terminal/rewardclaimhistory",
            RedeemHistory: "/merchant/terminal/redeemhistory",
            SalesHistory: "/merchant/terminal/saleshistory",
          },

          Live: "/merchant/live",
          TerminalLive: "/merchant/terminallive",
          Customers: "/merchant/customers",
          Customer: "/merchant/customer/dashboard",
          SubAccounts: "/merchant/subaccounts",

          RewardHistory: "/merchant/rewardhistory",
          RewardClaimHistory: "/merchant/rewardclaimhistory",
          RedeemHistory: "/merchant/redeemhistory",

          SalesHistory: "/merchant/saleshistory",
          StoreBankCollection: "/merchant/storebankcollection",

          AppUser: {
            Dashboard: "/merchant/customer/dashboard",
            RewardHistory: "/merchant/customer/rewardhistory",
            RewardClaimHistory: "/merchant/customer/rewardclaimhistory",
            RedeemHistory: "/merchant/customer/redeemhistory",
            SalesHistory: "/merchant/customer/saleshistory",
          },
          RewardInvoices: "/merchant/rewardinvoices",
          RedeemInvoices: "/merchant/redeeminvoices",
        },
        Customers: "/system/customers",
        AppUsers: "/system/appusers",
        AppUser: {
          Dashboard: "/system/appuser/dashboard",
        },
        AddMerchant: "/system/addmerchant",
        Merchants: "/system/merchants",
        Merchant: {
          Dashboard: "/system/merchant/dashboard",
        },
        Stores: "/system/stores",
        AddStore: "/system/addstore",
        Store: {
          Dashboard: "/system/store/dashboard",
          UpdateStore: "/system/store/updatestore",
        },
        AddCashier: "/system/addcashier",
        Cashiers: "/system/cashiers",
        Cashier: {
          Dashboard: "/system/cashier/dashboard",
        },
        AddAcquirer: "/system/addacquirer",
        Acquirers: "/system/acquirers",
        Acquirer: {
          Dashboard: "/system/acquirer/dashboard",
        },
        AddPgAccount: "/system/addpgaccount",
        PgAccounts: "/system/pgaccounts",
        PgAccount: {
          Dashboard: "/system/pgaccount/dashboard",
        },
        AddPosAccount: "/system/addposaccount",
        PosAccounts: "/system/posaccounts",
        PosAccount: {
          Dashboard: "/provider/terminals",
        },
        AddPosTerminal: "/system/addposterminal",
        PosTerminals: "/system/posterminals",
        PosTerminal: {
          Dashboard: "/system/posterminal/dashboard",
        },
        Transactions: {
          Reward: "/system/transactions/reward",
          Redeem: "/system/transactions/redeem",
          Change: "/system/transactions/change",
          Gift: "/system/transactions/gift",
          Payments: "/system/transactions/payment",
        },
      },
    },
    Api: {
      Login: "login",
      Logout: "logout",
      Core: {
        getcategories: "getcategories",
        getstates: "getstates",
        getcities: "getcities",
        GetUserInvoices: "getuserinvoices",
        GetUserAccountsLite: "getuseraccountslite",
      },
      ThankUCash: {
        updatedealstatus: "updatedealstatus",
        deletedeal: "deletedeal",
        Getdeals: "getdeals",
        SaveStore: "savestore",
        UpdateStore: "updatestore",
        GetStores: "getstores",
        UpdateStoreStatus: "updatestorestatus",
        DeleteStore: "deletestore",
        GetMerchant: "getmerchant",
        GetPurchaseHistory: "getpurchasehistory",
      },
      Product: {
        updateproductrewardpercentage: "updateproductrewardpercentage",
        getproductbalance: "getproductbalance",
        creditproductaccount: "creditproductaccount",
        saveproduct: "saveproduct",
        getproductcodes: "getproductcodes",
        getproducts: "getproducts",
        updateorderstatus: "updateorderstatus",
        getcategories: "getcategories",
      },
    },
    ListToggleOption: {
      SalesRange: 'salesrange',
      SortApply: "sortapply",
      SortOrder: "sortorder",
      CancelFilter: "cancelfilter",
      ApplyFilter: "applyfilter",
      SortReset: "sortreset",
      ToggleFilter: "togglefilter",
      Visiblity: "visiblity",
      Limit: "limit",
      Refresh: "refresh",
      ResetOffset: "resetoffset",
      Reset: "reset",
      Sort: "sort",
      Status: "status",
      Page: "page",
      Search: "search",
      Date: "date",
      Csv: "csv",
      SearchColumnChange: "searchcolumnchange",
      ColumnSearch: "columnsearch",
      MultiSelect: "multiselect",
      DateRange: "daterange",
      Other: "other",
      loanAmountRange: 'loanAmountRange'
    },
    FilterTypeOption: {
      Usage: 'adminUsage',
      Version: 'adminVersion',
      Merchant: "merchantMerchants",
      Terminal: "merchantTerminal",
      Cashout: "cashout",
      Stores: "merchantStores",
      Branch: "merchantBranch",
      Manager: "merchantManager",
      Campaign: "merchantCampaign",
      MerchantSales: "merchantMerchantSales",
      SubAccount: "merchantSubAccount",
      Variant: 'merchantVariant',
      Cashier: 'merchantCashier',
      Customer: 'merchantCustomer',
      RewardClaim: 'merchantRewardClaim',
      Order: 'merchantOrder',
      Product: 'merchantProduct',
      Inventory: 'merchantInventory',
      Deals: 'merchantDeals',
      FaqCategory: 'adminFaqCategory',
      Faq: 'adminFaq',
      PendingLoans: 'pendingLoans',
      RedeemedLoans: 'redeemedLoans',
      PendingSettlements: 'pendingSettlements',
      ClearSettlements: 'clearSettlements',
      SoldHistory: 'merchantSoldHistory',
      GetShipment: 'getshipment',
      OrderHistory: 'getordersovrview',
      Transaction: 'transaction',
      ReedemHistory: 'reedemhistory',
      RewardHistory: 'rewardhistory',
      CashierSales: 'cashiersales',
      CustomerSales: 'customersales',
      CustomerRewardHistory: 'customerrewardhistory',
      DealHistory: 'dealhistory',
      Balance: 'balance',
      GiftCardReedem: 'giftcardreedem',
      GiftCardReward: 'giftcardreward',
      GiftPointBalance: 'giftpointbalance',
      GiftPointReedem: 'giftpointreedem',
      GiftPointReward: 'giftpointreward',
      MerchantTransaction: 'merchanttransaction',
      PendingRewrdHistory: 'pendingrewrdhistory',
      StoreSales: 'storesales',
      MerchantTerminal: 'merchantterminal',
      Subscription: 'subscription'
    },
    OtherFilters: {
      Merchant: {
        Owner: "owner"
      },
      Terminal: {
        Merchant: "merchant",
        Store: "store",
        Provider: "provider",
        Bank: 'bank'
      },
      Stores: {
        Owner: "owner"
      },
      Branch: {

      },
      Manager: {
        Branch: "branch",
        Owner: "owner"
      },
      Campaign: {

      },

      MerchantSales: {
        Merchant: "merchant",
        Store: "store",
        CardBank: "cardbank",
        TransactionType: "transactiontype",
        CardBrand: "cardbrand",
        Provider: "provider",
        Cashier: "cashier",
        Terminal: "terminal",
        Bank: "bank"
      }
    },
    DateRangeOptions: {
      parentEl: '#FilterPeriod',
      locale: { format: "DD-MM-YYYY" },
      alwaysShowCalendars: false,
      startDate: moment().startOf("day"),
      endDate: moment().endOf("day"),
      dateLimit: {
        month: 1,
      },
      ranges: {
        Today: [moment(), moment()],
        Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
        "This Week": [moment().startOf("week").add("day", 1), moment().endOf("week").add("day", 1)],
        "Last Week": [
          moment().subtract(1, "weeks").startOf("isoWeek"),
          moment().subtract(1, "weeks").endOf("isoWeek"),
        ],
        "Last 7 Days": [moment().subtract(6, "days"), moment()],
        "Last 15 Days": [moment().subtract(14, "days"), moment()],
        "Last 30 Days": [moment().subtract(29, "days"), moment()],
        "This Month": [moment().startOf("month"), moment().endOf("month")],
        "Last Month": [
          moment().startOf("month").subtract(1, "month"),
          moment().startOf("month").subtract(1, "days"),
        ],
        All: [new Date(2017, 0, 1, 0, 0, 0, 0), moment()],
      },
      Core: {
        getcategories: "getcategories",
        getstates: "getstates",
        getcities: "getcities",
      },
      ThankUCash: {
        savestore: "savestore",
      }
    },
    DateRangeOptionsCustom: {
      locale: { format: "DD-MM-YYYY" },
      alwaysShowCalendars: false,
      showCustomRangeLabel: true,
      parentEl: "#DateRangeOptionsCustom_Date",
      startDate: moment().startOf("day"),
      endDate: moment().endOf("day"),
      dateLimit: {
        month: 1,
      },
      ranges: {
        Today: [moment(), moment()],
        Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
        "This Week": [moment().startOf("week").add("day", 1), moment().endOf("week").add("day", 1)],
        "Last Week": [
          moment().subtract(1, "weeks").startOf("isoWeek"),
          moment().subtract(1, "weeks").endOf("isoWeek"),
        ],
        "This Month": [moment().startOf("month"), moment().endOf("month")],
        "Last Month": [
          moment().startOf("month").subtract(1, "month"),
          moment().startOf("month").subtract(1, "days"),
        ],
      },
    },
    DatePickerTypes: {
      year: 'year',
      Rewardyear: 'rewardyear',
      month: 'month',
      week: 'week',
      day: 'day',
      hour: 'hour'
    },
    Icon_Cropper_Options: {
      MaintainAspectRatio: "true",
      AspectRatio: "1 / 1",
      MinimumWidth: 128,
      MinimumHeight: 128,
      ResizeToWidth: 128,
      Format: "png",
    },
    CommonResource: {
      Logout: 'Logout',
      SaveFlashTitle: "Flash Deal",
      SaveFlashHelp: "Click Continue To Make Flash Deal",
      DeleteDeal: "Delete Deal ?",
      DeleteDealHelp: "Click continue to delete deal ",
      RemoveFlasTitle: "UnMark Flash Deal",
      RemoveFlashHelp: "Click Continue To UnMark Flash Deal",
      SaveFilterTitle: "Save This Filter View",
      SaveFilterHelp: "Enter Filter Name",
      LogoutTitle: "Are You Sure You want to Logout ?",
      ReferenceId: "Reference Id",
      ReferenceKey: " Reference Key",
      ReferenceCode: " Reference Code",
      SystemName: " Reference Code",
      DuplicateCampaign: "Duplicate Campaign ?",
      Name: "Name",
      Select: "Select",
      Cancel: "Cancel",
      Continue: "Continue",
      Status: "Status",
      CreateDate: "Added On",
      CreatedBy: "Added By",
      ModifyDate: "Updated On",
      ModifyBy: "Updated By",
      AccessPin: "4 digit pin",
      FilterName: "filter name",
      UpdateTitle: "Update details ?",
      UpdateHelp: "Enter 4 digit pin to update details",
      ContinueUpdateHelp: "Click Continue to update details",

      DeleteTitle: "Delete View ?",
      DeleteView: "Delete View ?",

      DeleteBankTitle: "Delete Bank Account ?",
      RequestCashOut: "Initialize Cashout ?",


      ResetKey: "Reset Key?",
      ResetKeyHelp:
        "Press continue to reset app keys?",
      DeleteHelp:
        "Details can not be recovered once deleted. Do you want to continue ?",
      InvalidImage: "Invalid image dimension. Please select valid image",
      DeleteBankHelp:
        "Bank account can not be recovered once deleted. Do you want to continue ?",
      RequestCashoutHelp:
        "Initialize Cashout. Do you want to continue ?",
    },
    ShowHeader: true,
    ListType: {
      All: "all",
      Owner: "owner",
    },
  }
  public IsFormProcessing = false;
  public templocmerchant: boolean = true;
  public MerchantReferenceKey: string = '';
  public MerchantReferenceId: string = '';
  public MerchantToken: string = '';
  public AccessKey!: string;
  public PublicKey!: string;
  public LoginTime!: string;
  public User!: OAccessUser;
  public UserAccount!: OAccessUserAccount;
  public UserOwner!: OAccessUserAccountOwner;
  public UserCountry!: OAccessUserCountry;
  public UserLocation!: OAccessUserLocation;
  public UserDevice!: OAccessUserDevice;
  public UserRoles!: Array<OAccessUserAccountRole>;


  _Icon_Cropper_ChangedEvent: any = '';
  _Icon_Cropper_BaseImage: string = '';
  _Icon_Cropper_Image: any = '';
  _Multiple_Image_Data: any = [];
  _Icon_Cropper_Data: OStorageContent = {
    Name: null,
    Content: null,
    Extension: null,
    TypeCode: null,
    Height: 400,
    Width: 800
  };

  PreventText(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  GetSearchConditionStrict(BaseString: string, ColumnName: string, ColumnType: any, ColumnValue: string, Condition: any) {
    if (BaseString == undefined || BaseString == null) {
      BaseString = "";
    }
    var SearchExpression = BaseString;
    if (
      ColumnName != undefined &&
      ColumnType != undefined &&
      // ColumnValue != undefined &&
      ColumnName != null &&
      ColumnType != null &&
      //ColumnValue != null &&
      ColumnName != "" &&
      ColumnType != "" //&&
      // ColumnValue != ''
    ) {
      if (ColumnType == "text") {
        if (
          SearchExpression != undefined &&
          SearchExpression != null &&
          SearchExpression != ""
        ) {
          SearchExpression = "( " + SearchExpression + ") AND ";
        }
        // SearchExpression += ColumnName + '.Contains("' + ColumnValue + '")';
        if (ColumnValue == "") {
          SearchExpression += ColumnName + " " + Condition + ' "" ';
        } else if (ColumnValue != null && ColumnValue != "") {
          SearchExpression +=
            ColumnName + " " + Condition + ' "' + ColumnValue + '" ';
        } else {
          SearchExpression +=
            "( " + ColumnName + " " + Condition + " null " + ")";
        }
      } else if (ColumnType == "number") {
        if (
          SearchExpression != undefined &&
          SearchExpression != null &&
          SearchExpression != ""
        ) {
          SearchExpression = "( " + SearchExpression + ") AND ";
        }
        if (ColumnValue != null && ColumnValue != "") {
          SearchExpression +=
            ColumnName + " " + Condition + ' "' + ColumnValue + '" ';
        } else {
          SearchExpression += "( " + ColumnName + " " + Condition + " " + ")";
        }
      }

      if (BaseString != undefined && BaseString != null && BaseString != "") {
        BaseString += " OR ";
      }

      //  BaseString += SearchExpression;
      return SearchExpression;
    } else {
      return BaseString;
    }
  }

  GetSearchConditionRange(BaseString: any, ColumnName: any, StartRange: any, EndRange: any) {
    if (BaseString == undefined || BaseString == null) {
      BaseString = "";
    }
    StartRange = StartRange;
    EndRange = EndRange + 1;
    var SearchExpression = BaseString;
    if (
      ColumnName != undefined &&
      StartRange != undefined &&
      EndRange != undefined &&
      ColumnName != null &&
      StartRange != null &&
      EndRange != null &&
      ColumnName != "" &&
      EndRange != ""
    ) {
      if (
        SearchExpression != undefined &&
        SearchExpression != null &&
        SearchExpression != ""
      ) {
        SearchExpression = "( " + SearchExpression + ") AND ";
      }
      SearchExpression +=
        "( " +
        ColumnName +
        ' > "' +
        StartRange +
        '" AND ' +
        ColumnName +
        ' < "' +
        EndRange +
        '")';
      if (BaseString != undefined && BaseString != null && BaseString != "") {
        BaseString += " AND ";
      }
      BaseString += SearchExpression;
      return SearchExpression;
    } else {
      return BaseString;
    }
  }

  GetSortCondition(SortConditions) {
    var NewSortCondition = "";
    if (
      SortConditions != undefined ||
      SortConditions != null ||
      SortConditions.length != 0
    ) {
      for (let index = 0; index < SortConditions.length; index++) {
        const element = SortConditions[index];
        if (index == SortConditions.length - 1) {
          NewSortCondition = NewSortCondition + element;
        } else {
          NewSortCondition = NewSortCondition + element + ",";
        }
      }
    }
    return NewSortCondition;
  }
  GetSearchCondition(BaseString: any, ColumnName: any, ColumnType: any, ColumnValue: any) {
    if (BaseString == undefined || BaseString == null) {
      BaseString = "";
    }
    var SearchExpression = BaseString;
    if (
      ColumnName != undefined &&
      ColumnType != undefined &&
      ColumnValue != undefined &&
      ColumnName != null &&
      ColumnType != null &&
      ColumnValue != null &&
      ColumnName != "" &&
      ColumnType != "" &&
      ColumnValue != ""
    ) {
      if (ColumnType == "text") {
        if (
          SearchExpression != undefined &&
          SearchExpression != null &&
          SearchExpression != ""
        ) {
          SearchExpression += " OR ";
        }
        SearchExpression += ColumnName + '.Contains("' + ColumnValue + '")';
      } else if (ColumnType == "number") {
        if (isNaN(ColumnValue) == false) {
          if (
            SearchExpression != undefined &&
            SearchExpression != null &&
            SearchExpression != ""
          ) {
            SearchExpression = "( " + SearchExpression + ") AND ";
          }
          SearchExpression += ColumnName + " = " + ColumnValue + " ";
        }
      }

      if (BaseString != undefined && BaseString != null && BaseString != "") {
        BaseString += " OR ";
      }

      //  BaseString += SearchExpression;
      return SearchExpression;
    } else {
      return BaseString;
    }
  }

  RemoveItemFromArray(ItemToRemove: any, DataSet: any[]) {
    var Index = DataSet.indexOf(ItemToRemove, 0);
    if (Index > -1) {
      DataSet.splice(Index, 1);
    }
    return DataSet;
  }

  GetPercentage(BigAmount, SmallAmount) {
    let percentage = 0;
    if (BigAmount && SmallAmount) {
      percentage = Math.round((((BigAmount - SmallAmount) * 100) / BigAmount));
    }
    return percentage;
  }

  RefreshHelper() {
    this.AppConfig.Client = this.GetStorage(this.AppConfig.Storage.OReqH);
    this.AppConfig.Host = window.location.host;
    if (this.AppConfig.Client != null) {
      this.AppConfig.ClientHeader = {
        "Content-Type": "application/json; charset=utf-8",
        hcak: this.AppConfig.Client.hcak,
        hcavk: this.AppConfig.Client.hcavk,
        hctk: null,
        hcudlt: this.AppConfig.Client.hcudlt,
        hcudln: this.AppConfig.Client.hcudln,
        hcuak: this.AppConfig.Client.hcuak,
        hcupk: this.AppConfig.Client.hcupk,
        hcudk: this.AppConfig.Client.hcudk,
        hcuata: "",
        hcuatp: "",
      };
    }

    if (this.AppConfig.Host == "merchant.dealday.africa") {
      this.AppConfig.HostConnect = "https://webconnect.thankucash.com/";
      this.AppConfig.PayStackKey = "pk_live_4acd36db0e852af843e16e83e59e7dc0f89efe12";
      this.AppConfig.MerchantCode = "MX37487";
        this.AppConfig.MerchantPayMentId = "Default_Payable_MX37487";
        this.AppConfig.MerchantPayMode = "LIVE"
      this.AppConfig.HostType = HostType.Live;
      this.AppConfig.MerchantPanelURL = "https://merchant.thankucash.com";
    }
    else if (this.AppConfig.Host == 'merchant-dealday.thankucash.dev') {
      this.AppConfig.HostConnect = "https://webconnect.thankucash.dev/";
      this.AppConfig.PayStackKey = "pk_test_4cca266d686312285a54570e20b46a808ae0d0f6";
      this.AppConfig.MerchantCode = "MX51041";
        this.AppConfig.MerchantPayMentId = "Default_Payable_MX51041";
        this.AppConfig.MerchantPayMode = "TEST";
        this.AppConfig.HostType = HostType.Dev;
      this.AppConfig.MerchantPanelURL = "https://merchant.thankucash.dev";
    }
    else if (this.AppConfig.Host == 'merchant-dealday.thankucash.tech') {
      this.AppConfig.HostConnect = "https://webconnect.thankucash.tech/";
      this.AppConfig.PayStackKey = "pk_test_4cca266d686312285a54570e20b46a808ae0d0f6";
      this.AppConfig.MerchantCode = "MX51041";
        this.AppConfig.MerchantPayMentId = "Default_Payable_MX51041";
        this.AppConfig.MerchantPayMode = "TEST";
        this.AppConfig.HostType = HostType.Tech;
      this.AppConfig.MerchantPanelURL = "https://merchant.thankucash.tech";
    }
    else if (
      this.AppConfig.Host == "beta-merchant-dealday.thankucash.com") {
      this.AppConfig.HostConnect = "https://beta-webconnect.thankucash.com/";
      this.AppConfig.PayStackKey = "pk_live_4acd36db0e852af843e16e83e59e7dc0f89efe12";
      this.AppConfig.MerchantCode = "MX37487";
      this.AppConfig.MerchantPayMentId = "Default_Payable_MX37487";
      this.AppConfig.MerchantPayMode = "LIVE";
      this.AppConfig.HostType = HostType.Live;
      this.AppConfig.MerchantPanelURL = "https://beta-merchant.thankucash.com";
    }
    else if (
      this.AppConfig.Host == "localhost:4444") {
      this.AppConfig.HostConnect = "https://webconnect.thankucash.dev/";
      this.AppConfig.PayStackKey = "pk_test_4cca266d686312285a54570e20b46a808ae0d0f6";
      this.AppConfig.MerchantCode = "MX51041";
      this.AppConfig.MerchantPayMentId = "Default_Payable_MX51041";
      this.AppConfig.MerchantPayMode = "TEST";
      this.AppConfig.HostType = HostType.Dev;
      this.AppConfig.MerchantPanelURL = "https://merchant.thankucash.dev";
    }
    else {
      // this.AppConfig.HostConnect = "https://webconnect.thankucash.dev/";
      this.AppConfig.HostConnect = "https://localhost:5001/";
      this.AppConfig.PayStackKey = "pk_test_4cca266d686312285a54570e20b46a808ae0d0f6";
      this.AppConfig.MerchantCode = "MX51041";
      this.AppConfig.MerchantPayMentId = "Default_Payable_MX51041";
      this.AppConfig.MerchantPayMode = "TEST";
      this.AppConfig.HostType = HostType.Dev;
      this.AppConfig.MerchantPanelURL = "https://merchant.thankucash.dev";
    }

    var UserAccountInformation = this.GetStorage(
      this.AppConfig.Storage.Account
    );
    if (UserAccountInformation != null) {
      this.AppConfig.AccountId = UserAccountInformation.UserAccount.AccountId;
      this.AppConfig.AccountKey = UserAccountInformation.UserAccount.AccountKey;
      this.AccessKey = UserAccountInformation.AccessKey;
      this.PublicKey = UserAccountInformation.PublicKey;
      this.LoginTime = UserAccountInformation.LoginTime;
      this.User = UserAccountInformation.User;
      this.UserAccount = UserAccountInformation.UserAccount;
      this.UserOwner = UserAccountInformation.UserOwner;
      this.UserCountry = UserAccountInformation.UserCountry;
      this.UserRoles = UserAccountInformation.UserRoles;
      if (UserAccountInformation.Subscription) {
        this.UserAccount.SystemSubscription = UserAccountInformation.Subscription
      }
      else {
        this._Router.navigate([this.AppConfig.Pages.System.Login]);

      }
    }
  }

  /**
   * check user is logged in or not
   * @returns {boolean} true if Account details found, otherwise false
   */
  isLoggedIn(): boolean {
    if (!this.GetStorage(this.AppConfig.Storage.Account)) {
      this._Router.navigate(['/auth/login'])
    }
    return !!this.GetStorage(this.AppConfig.Storage.Account);
  }
  // localstorage services
  SaveStorage(StorageName: any, StorageValue: any) {
    try {
      var StringV = btoa(JSON.stringify(StorageValue));
      localStorage.setItem(StorageName, StringV);
      return true;
    } catch (e) {
      alert(e);
      return false;
    }
  }
  SaveStorageValue(StorageName: any, StorageValue: any) {
    try {
      localStorage.setItem(StorageName, btoa(StorageValue));
      return true;
    } catch (e) {
      alert(e);
      return false;
    }
  }
  GetStorage(StorageName: any) {
    var StorageValue = localStorage.getItem(StorageName);
    if (StorageValue != undefined) {
      if (StorageValue != null) {
        return JSON.parse(atob(StorageValue));
      } else {
        return null;
      }
    } else {
      return null;
    }
  }
  GetStorageValue(StorageName: any) {
    var StorageValue = localStorage.getItem(StorageName);
    if (StorageValue != undefined) {
      if (StorageValue != null) {
        return atob(StorageValue);
      } else {
        return null;
      }
    } else {
      return null;
    }
  }
  DeleteStorage(StorageName: any) {
    localStorage.removeItem(StorageName);
    return true;
  }

  // notify toastr
  NotifySuccess(Message: string) {
    notie.alert({
      type: 1,
      text: Message,
    });
  }
  NotifyWarning(Message: string) {
    notie.alert({
      type: 2,
      text: Message,
    });
  }
  NotifyError(Message: string) {
    notie.alert({
      type: 3,
      text: Message,
    });
  }
  NotifyInfo(Message: string) {
    notie.alert({
      type: 4,
      text: Message,
    });
  }

  HandleException(Exception: HttpErrorResponse) {
    this.IsFormProcessing = false;
    if (
      Exception.status != undefined &&
      Exception.error != null &&
      Exception.status == 401
    ) {
      var ResponseData = JSON.parse(atob(Exception.error.zx)) as OResponse;
      this.NotifyError(ResponseData.Message);
      setTimeout(() => {
        window.location.href = this.AppConfig.Pages.System.Login;
      }, 1500);
    } else {
      if (Exception.error instanceof Error) {
        this.NotifyError("Sorry, error occured while connecting server:" + Exception.error.message);
      } else {
        this.NotifyError(
          "Sorry, error occured while connecting server : " +
          Exception.message +
          " Response Code -" +
          Exception.status
        );
      }
    }
  }
  newGuid() {
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (
      c
    ) {
      var r = (Math.random() * 16) | 0,
        v = c == "x" ? r : (r & 0x3) | 0x8;
      return v.toString(16);
    });
  }
  GoogleAddressArrayToJson(address_components: any): any {
    var addressJson: any = {};
    for (let index = 0; index < address_components.length; index++) {
      const element = address_components[index];
      addressJson[element.types[0]] = element.long_name;
    }
    return addressJson;
  }

  // file upload functionality
  public _SetFirstImageOrNone(files: any[]): void {
    if (files.length > 0) {
      let reader: any = new FileReader();
      reader.onloadend = (e: any) => {
        this._Icon_Cropper_Data.Name = files[0].name;
        this._Icon_Cropper_BaseImage = reader.result.toString();
      }
      reader.readAsDataURL(files[0]);
    } else {
      this.Icon_Crop_Clear();
    }
  }

  Icon_Crop_Failed(source = 'none') {
    this.NotifyWarning('Unable to load image. Please make sure valid image selected or refresh page and try again');
  }
  Icon_Crop_Loaded() {
    $("#_Icon_Cropper_Modal").modal("show");
  }
  Icon_Crop_Clear(_Icon_Cropper_Modal_Reference?: any) {
    this._Icon_Cropper_ChangedEvent = '';
    this._Icon_Cropper_Image = '';
    this._Icon_Cropper_BaseImage = '';
    this._Icon_Cropper_Data.Name = null;
    this._Icon_Cropper_Data.Content = null;
    this._Icon_Cropper_Data.Extension = null;
    this._Icon_Cropper_Data.TypeCode = null;
    $("#_Icon_Cropper_Modal").modal("hide");
  }
  Icon_Change(event: any): void {
    this._Icon_Cropper_ChangedEvent = event;
  }
  Icon_Cropped(event: ImageCroppedEvent) {
    this._Icon_Cropper_Image = event.base64;
    var ImageDetails = this.GetImageDetails(this._Icon_Cropper_Image);
    this._Icon_Cropper_Data.Content = ImageDetails.Content;
    this._Icon_Cropper_Data.Extension = ImageDetails.Extension;
  }
  GetImageDetails(ImageBase64: any) {
    var _ImageContent: OStorageContent = {
      Name: null,
      Content: null,
      Extension: null,
      TypeCode: null,
      Width: 0,
      Height: 0,
    };

    if (ImageBase64 != null && ImageBase64 != "") {
      var ImageContent = ImageBase64;
      var ImageExtension = "jpg";
      var ImageBase = ImageContent.replace("data:image/png;base64,", "")
        .replace("data:image/jpg;base64,", "")
        .replace("data:image/jpeg;base64,", "")
        .replace("data:image/gif;base64,", "");
      if (ImageContent.includes("data:image/png;base64,")) {
        ImageExtension = "png";
      } else if (ImageContent.includes("data:image/jpg;base64,")) {
        ImageExtension = "jpg";
      } else if (ImageContent.includes("data:image/jpeg;base64,")) {
        ImageExtension = "jpeg";
      } else if (ImageContent.includes("data:image/gif;base64,")) {
        ImageExtension = "gif";
      } else {
        ImageExtension = "jpg";
      }
      _ImageContent.Content = ImageBase;
      _ImageContent.Extension = ImageExtension;
      return _ImageContent;
    } else {
      return _ImageContent;
    }
  }
  GetFileFromB64URL(url: string, filename: any, mimeType: any) {
    return (fetch(url)
      .then((res) => { return res.arrayBuffer(); })
      .then((buf) => { return new File([buf], filename, { type: mimeType }); })
    );
  }
  SetNetwork() {
    var DeviceSerialNumber = null;
    var _Storage_UserDevice = this.GetStorage(this.AppConfig.Storage.Device);
    if (_Storage_UserOReqH == null) {
      var DeviceInformation = {
        SerialNumber: this.newGuid(),
      };
      this.SaveStorage(this.AppConfig.Storage.Device, DeviceInformation);
    } else {
      DeviceSerialNumber = _Storage_UserDevice.SerialNumber;
    }
    var _Storage_UserOReqH = this.GetStorage(this.AppConfig.Storage.OReqH);
    if (_Storage_UserOReqH == null) {
      var ehcak = btoa(this.AppConfig.AppKey);
      var ehcavk = btoa(this.AppConfig.AppVersionKey);
      var ehctk = btoa("na");
      var ehcudv = btoa(DeviceSerialNumber!);
      var ehcudlt = btoa("0");
      var ehcudln = btoa("0");
      var ekuak = "";
      var ehcupk = btoa("0");
      var ehcudk = btoa("0");
      var OReqH = {
        "Content-Type": "application/json; charset=utf-8",
        hcak: ehcak,
        hcavk: ehcavk,
        hctk: ehctk,
        hcudv: ehcudv,
        hcudlt: ehcudlt,
        hcudln: ehcudln,
        hcuak: ekuak,
        hcupk: ehcupk,
        hcudk: ehcudk,
        hcuata: "",
        hcuatp: "",
      };
      this.AppConfig.ClientHeader = OReqH;
      this.SaveStorage(this.AppConfig.Storage.OReqH, OReqH);
    }
  }
  //
  PostData(PostLocation: string, Data: any): Observable<OResponse> {
    this.AppConfig.ClientHeader.hcuata = "";
    this.AppConfig.ClientHeader.hcuatp = "";
    var NetworkLocation = this.AppConfig.HostConnect + PostLocation + Data.Task;
    this.AppConfig.ClientHeader.hctk = window.btoa(Data.Task);
    if (
      Data.AuthPin != undefined &&
      Data.AuthPin != null &&
      Data.AuthPin != ""
    ) {
      this.AppConfig.ClientHeader.hcuata = btoa(Data.AuthPin);
    }
    if (
      Data.AuthPassword != undefined &&
      Data.AuthPassword != null &&
      Data.AuthPassword != ""
    ) {
      this.AppConfig.ClientHeader.hcuatp = btoa(Data.AuthPassword);
    }
    let _Headers = new HttpHeaders(this.AppConfig.ClientHeader);
    var ORequest = {
      zx: btoa(unescape(encodeURIComponent(JSON.stringify(Data)))),
    };
    var RequestData = JSON.stringify(ORequest);
    return this._Http.post<ONetworkResponse>(NetworkLocation, RequestData,
      {
        headers: _Headers,
      })
      .pipe(map((_Response) => JSON.parse(atob(_Response.zx)) as OResponse))
      .pipe(catchError((error: HttpErrorResponse) => throwError(error)));
  }

  StopClickPropogation(): void {
    $(".dropdown-menu").click(() => {
      event?.stopPropagation();
    });
  };

  Update_CurrentFilterSnap(event: any, Type: any, List_Config?: any, SubType?: any) {
    var ResetOffset = false;
    this.FilterSnapTemprary.RefreshData = false;
    this.FilterSnapTemprary.RefreshCount = false;
    if (
      !(
        isNull(List_Config.SearchBaseConditions) ||
        isUndefined(List_Config.SearchBaseConditions)
      )
    ) {
      this.FilterSnapTemprary.SearchBaseConditions = List_Config.SearchBaseConditions;
    }
    if (Type == this.AppConfig.ListToggleOption.Limit) {
      this.FilterSnapTemprary.PageRecordLimit = event;
      ResetOffset = true;
      this.FilterSnapTemprary.RefreshData = true;
      this.FilterSnapTemprary.RefreshCount = true;
    } else if (Type == this.AppConfig.ListToggleOption.Search) {
      if (event != null) {
        this.FilterSnapTemprary.SearchParameter = event.target.value;
      }
      ResetOffset = true;
      this.FilterSnapTemprary.RefreshData = true;
      this.FilterSnapTemprary.RefreshCount = true;
    } else if (Type == this.AppConfig.ListToggleOption.Date) {
      if (
        this.FilterSnapTemprary.StartDate == undefined &&
        this.FilterSnapTemprary.EndDate == undefined
      ) {
        this.FilterSnapTemprary.StartTime = event.start;
        this.FilterSnapTemprary.EndTime = event.end;
      }
      ResetOffset = true;
      this.FilterSnapTemprary.RefreshData = false;
      this.FilterSnapTemprary.RefreshCount = false;
    } else if (Type == this.AppConfig.ListToggleOption.Status) {

      this.FilterSnapTemprary.StatusName = event.data[0].text;
      if (this.CurrentType == this.AppConfig.FilterTypeOption.MerchantSales) {
        this.FilterSnapTemprary.Status = event.data[0].id;
      } else {
        this.FilterSnapTemprary.Status = event.data[0].code;
      }

      this.FilterSnapTemprary.RefreshData = false;
      this.FilterSnapTemprary.RefreshCount = false;
      ResetOffset = false;
    } else if (Type == this.AppConfig.ListToggleOption.Sort) {
      if (isUndefined(this.FilterSnapTemprary.Sort)) {
        this.FilterSnapTemprary.Sort = {};
      }
      this.FilterSnapTemprary.Sort.SortColumn = event.SystemName;
      this.FilterSnapTemprary.RefreshData = true;
      this.FilterSnapTemprary.RefreshCount = true;
      ResetOffset = true;
    } else if (Type == this.AppConfig.ListToggleOption.SortOrder) {
      if (isUndefined(this.FilterSnapTemprary.Sort)) {
        this.FilterSnapTemprary.Sort = {};
      }
      if (this.FilterSnapTemprary.Sort.SortOrder == "asc") {
        this.FilterSnapTemprary.Sort.SortOrder = "desc";
      } else {
        this.FilterSnapTemprary.Sort.SortOrder = "asc";
      }
      this.FilterSnapTemprary.RefreshData = true;
      this.FilterSnapTemprary.RefreshCount = true;
      ResetOffset = true;
    } else if (Type == this.AppConfig.ListToggleOption.SortApply) {
      this.FilterSnapTemprary.RefreshData = true;
      this.FilterSnapTemprary.RefreshCount = true;
      ResetOffset = true;
    } else if (Type == this.AppConfig.ListToggleOption.SortReset) {
      if (isUndefined(this.FilterSnapTemprary.Sort)) {
        this.FilterSnapTemprary.Sort = {};
      }

      this.FilterSnapTemprary.Sort.SortOrder = "desc";
      this.FilterSnapTemprary.Sort.SortColumn = null;
      this.FilterSnapTemprary.RefreshData = true;
      this.FilterSnapTemprary.RefreshCount = true;
      ResetOffset = true;
    } else if (Type == this.AppConfig.ListToggleOption.Page) {
      this.FilterSnapTemprary.ActivePage = event;
      this.FilterSnapTemprary.RefreshData = true;
      this.FilterSnapTemprary.RefreshCount = false;
    } else if (Type == this.AppConfig.ListToggleOption.MultiSelect) {
      this.FilterSnapTemprary.ActivePage = event;
      this.FilterSnapTemprary.RefreshData = true;
      this.FilterSnapTemprary.RefreshCount = true;
    } else if (Type == this.AppConfig.ListToggleOption.Refresh) {
      this.FilterSnapTemprary.RefreshData = true;
      this.FilterSnapTemprary.RefreshCount = true;
    } else if (Type == this.AppConfig.ListToggleOption.Refresh) {
      this.FilterSnapTemprary.RefreshData = true;
      this.FilterSnapTemprary.RefreshCount = true;
    } else if (Type == this.AppConfig.ListToggleOption.ResetOffset) {
      this.FilterSnapTemprary.RefreshData = true;
      this.FilterSnapTemprary.RefreshCount = true;
      ResetOffset = true;
    } else if (Type == this.AppConfig.ListToggleOption.ApplyFilter) {
      ResetOffset = true;
      this.FilterSnapTemprary.RefreshData = true;
      this.FilterSnapTemprary.RefreshCount = true;
    } else if (Type == this.AppConfig.ListToggleOption.CancelFilter) {
    }

    else if (Type == this.AppConfig.ListToggleOption.SalesRange) {
      this.FilterSnapTemprary.SalesRange = event.data;
    }
    else if (Type == this.AppConfig.ListToggleOption.Other) {
      var data: any = lodash.cloneDeep(event.data[0]);
      data.element = undefined;
      data.OtherType = SubType;

      var CurrentIndex = this.FilterSnapTemprary.OtherFilters.findIndex((filter: any) => (filter.data[0].OtherType == SubType));
      if (CurrentIndex == -1) {
        this.FilterSnapTemprary.OtherFilters.push({ data: [data], value: event.value });
      } else {
        this.FilterSnapTemprary.OtherFilters[CurrentIndex] = { data: [data], value: event.value };
      }

      CurrentIndex = data;

    }
    if (ResetOffset == true) {
      this.FilterSnapTemprary.ActivePage = 1;
      this.FilterSnapTemprary.TotalRecords = 0;
      this.FilterSnapTemprary.ShowingStart = 0;
      this.FilterSnapTemprary.ShowingEnd = 0;
    }
    this.ComputeFilterState();
  }

  ComputeFilterState(): void {
    //#region clone FilterSnap
    var FilterSnapClone: any = lodash.cloneDeep(this.FilterSnap);
    FilterSnapClone.text = null;
    FilterSnapClone.id = null;
    FilterSnapClone.Badges = null;
    FilterSnapClone.disabled = null;
    FilterSnapClone.element = null;
    FilterSnapClone.selected = null;
    //#endregion

    //#region clone CleanConfig
    var CleanConfigClone: any = lodash.cloneDeep(this.CleanConfig);
    CleanConfigClone.text = null;
    CleanConfigClone.id = null;
    CleanConfigClone.Badges = null;
    CleanConfigClone.disabled = null;
    CleanConfigClone.element = null;
    CleanConfigClone.selected = null;
    CleanConfigClone.selected = null;
    //#endregion

    if (this.AreTwoFilterSnapEqual(FilterSnapClone, CleanConfigClone, 'check for Default')) {
      this.FilterState = this.AppConfig.FilterStates.Default;
    } else if (this.AreTwoFilterSnapEqual(this.FilterSnap, this.FilterSnapPrestine, 'check for nochange')) {
      this.FilterState = this.AppConfig.FilterStates.NoChange;
    } else {
      this.FilterState = this.AppConfig.FilterStates.Modified;
    }
  }
  public AreTwoFilterSnapEqual(FilterSnapClone: any, ElementClone: any, print?: string): boolean {
    var FilterSnapClone_C = lodash.cloneDeep(FilterSnapClone);
    var ElementClone_C = lodash.cloneDeep(ElementClone);

    FilterSnapClone_C.RefreshCount = 0;
    ElementClone_C.RefreshCount = 0;

    FilterSnapClone_C.RefreshData = undefined;
    ElementClone_C.RefreshData = undefined;

    FilterSnapClone_C.TotalRecords = 0;
    ElementClone_C.TotalRecords = 0;

    FilterSnapClone_C.SearchBaseConditions = undefined;
    ElementClone_C.SearchBaseConditions = undefined;
    if (FilterSnapClone_C.Sort) {
      FilterSnapClone_C.Sort.SortDefaultName = undefined;
    }
    if (ElementClone_C.Sort) {
      ElementClone_C.Sort.SortDefaultName = undefined;
    }

    try {

      var compare = changesets(lodash.omit(FilterSnapClone_C, lodash.functions(FilterSnapClone_C)), lodash.omit(ElementClone_C, lodash.functions(ElementClone_C)));
      if (compare == undefined) {
        return true;
      }
      else {
        return false;
      }

    } catch (error) {
      return false;
    }
  }


  public DataReloadEligibility(Type: any): boolean {
    return (Type == this.AppConfig.ListToggleOption.Search
      || Type == this.AppConfig.ListToggleOption.Page
      || Type == this.AppConfig.ListToggleOption.Refresh
      || Type == this.AppConfig.ListToggleOption.Limit
      || Type == this.AppConfig.ListToggleOption.Csv);
    ;
  }

  GetDateCondition(BaseString: any, ColumnName: any, StartTime: any, EndTime: any) {
    if (BaseString == undefined || BaseString == null) {
      BaseString = "";
    }

    var SearchExpression = BaseString;
    if (
      ColumnName != undefined &&
      StartTime != undefined &&
      EndTime != undefined &&
      ColumnName != null &&
      StartTime != null &&
      EndTime != null &&
      ColumnName != "" &&
      StartTime != "" &&
      EndTime != ""
    ) {
      var FSd = new Date(StartTime);
      var TStartDateM = moment(FSd).format("YYYY-MM-DD HH:mm:ss");
      var ESd = new Date(EndTime);
      var TEndTimeM = moment(ESd).format("YYYY-MM-DD HH:mm:ss");
      if (
        SearchExpression != undefined &&
        SearchExpression != null &&
        SearchExpression != ""
      ) {
        SearchExpression = "( " + SearchExpression + ") AND ";
      }
      SearchExpression +=
        "( " +
        ColumnName +
        ' > "' +
        TStartDateM +
        '" AND ' +
        ColumnName +
        ' < "' +
        TEndTimeM +
        '")';
      if (BaseString != undefined && BaseString != null && BaseString != "") {
        BaseString += " AND ";
      }
      //  BaseString += SearchExpression;
      return SearchExpression;
    } else {
      return BaseString;
    }
  }

  HCXGetDateComponentList(list: object[]) {
    list.forEach((item: any) => {
      var keys: any = Object.keys(item);
      keys.forEach((key: any) => {
        const objectvaluetype = item[key];
        if (typeof objectvaluetype == 'string') {
          const dateFormat = 'DD-MM-YYYY';
          const toDateFormat = moment(new Date(item[key])).format(dateFormat);
          var isValid = moment(toDateFormat, dateFormat, true).isValid();
          if (isValid) {
            item[key + 'Part'] =
            {
              Date: this.GetDateS(objectvaluetype),
              Time: this.GetTimeS(objectvaluetype),
              DateTime: this.GetDateTimeS(objectvaluetype),
              Object: this.GetDateTime(objectvaluetype),
            };
          }
        }
      });
    });
    return list;
  }


  GetDateTime(Date: any) {
    return this.GetDateTimes(Date, this.AppConfig.TimeZone);
  }
  GetTimeS(Date: any) {
    return this.GetTimeSs(
      Date,
      this.AppConfig.TimeZone,
      this.AppConfig.TimeFormat
    );
  }

  GetDateTimeS(Date) {
    return this.GetDateTimeSs(
      Date,
      this.AppConfig.TimeZone,
      this.AppConfig.DateTimeFormat
    );
  }
  Active_FilterInit(filterType: string, InitializeConfig: any): void {
    this.CurrentType = filterType;
    this.CleanConfig = lodash.cloneDeep(InitializeConfig);
    this.CleanConfig.OtherFilters = [];
    this.CleanConfig.SalesRange = {
      SalesMin: 0,
      SalesMax: 10000000,
      RewardMin: 0,
      RewardMax: 10000000,
      RedeemMin: 0,
      RedeemMax: 10000000,
      DealMin: 0,
      DealMax: 10000000,
      DealPurchaseMin: 0,
      DealPurchaseMax: 10000000,
      minLoanAmount: 0,
      maxLoanAmount: 10000000,
      minSettlementAmount: 0,
      maxSettlementAmount: 10000000,
    };
    if (this.CleanConfig.Sort) {
      this.CleanConfig.Sort.SortColumn = null;
    }

    this.Active_FilterOptions = [];

    //#region Create Default Option

    var temp: any = lodash.cloneDeep(InitializeConfig);
    temp.id = -1;
    temp.text = "Default View";
    temp.Sort.SortDefaultName = temp.Sort.SortDefaultName + " desc";
    temp.OtherFilters = [];
    temp.SalesRange = {
      SalesMin: 0,
      SalesMax: 10000000,
      RewardMin: 0,
      RewardMax: 10000000,
      RedeemMin: 0,
      RedeemMax: 10000000,
      DealMin: 0,
      DealMax: 10000000,
      DealPurchaseMin: 0,
      DealPurchaseMax: 10000000,
      minLoanAmount: 0,
      maxLoanAmount: 10000000,
      minSettlementAmount: 0,
      maxSettlementAmount: 10000000,
    };
    this.Active_FilterOptions.push(temp);

    this.FilterSnap = lodash.cloneDeep(temp);
    this.FilterSnap.OtherFilters = [];
    this.FilterSnap.SalesRange = {
      SalesMin: 0,
      SalesMax: 10000000,
      RewardMin: 0,
      RewardMax: 10000000,
      RedeemMin: 0,
      RedeemMax: 10000000,
      DealMin: 0,
      DealMax: 10000000,
      DealPurchaseMin: 0,
      DealPurchaseMax: 10000000,
      minLoanAmount: 0,
      maxLoanAmount: 10000000,
      minSettlementAmount: 0,
      maxSettlementAmount: 10000000,
    };
    this.FilterSnap.Badges = [];
    this.FilterSnapTemprary = lodash.cloneDeep(this.FilterSnap);
    this.FilterSnapPrestine = lodash.cloneDeep(this.FilterSnap);

    var filterData = {
      Task: "getliststates",
      "ReferenceId": this.UserAccount?.AccountId,
      "ReferenceKey": this.UserAccount?.AccountKey,
      "SubReferenceKey": filterType,
      "Offset": 0,
      "Limit": 100
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this.PostData(this.AppConfig.NetworkLocation.V3.SystemMange, filterData);
    _OResponse.subscribe(
      (_Response) => {
        this.IsFormProcessing = false;
        if (_Response.Status == this.StatusSuccess) {
          var _ResponseData = _Response.Result as any;

          var FilterArray = null;
          if (_ResponseData.Data.length > 0) {
            FilterArray = JSON.parse(_ResponseData.Data[0].Data);
          }
          if (FilterArray != null) {
            this.Active_FilterOptions = this.Active_FilterOptions.concat(
              FilterArray
            );
          }
        } else {
          this.NotifyError(_Response.Message);
        }
      },
      (_Error) => {
        this.IsFormProcessing = false;
        this.HandleException(_Error);
      }
    );
    this.ComputeFilterState();
  }

  GetFilterDate(Date: any) {
    if (Date != undefined && Date != null) {
      return moment(Date).format("DD-MM-YYYY")
    } else {
      return Date
    }
  }

  GetCampaignTimeS(Date: any) {
    return moment(Date).format("hh:mm a")
  }

  GetCampaignDateS(Date: any) {
    return moment(Date).format("DD-MM-YYYY")
  }

  GetDateS(Date: any) {
    if (Date != undefined && Date != null) {
      return this.GetDateSs(
        Date,
        this.AppConfig.TimeZone,
        this.AppConfig.DateFormat
      );
    }
    else {
      return Date;
    }
  }

  GetStatusIcon(StatusCode: any) {
    var StatusIcon = "fa fa-circle text-default f-s-11 p-r-5 ";
    if (isNaN(StatusCode) == false) {
      if (
        StatusCode == 1 ||
        StatusCode == 4 ||
        StatusCode == 29 ||
        StatusCode == 450 ||
        StatusCode == 452 ||
        StatusCode == 461 ||
        StatusCode == 27 ||
        StatusCode == 38 ||
        // order status - ready to ship
        StatusCode == 490 ||
        StatusCode == 547
      ) {
        StatusIcon = "fa fa-circle text-warning f-s-11 p-r-5 ";
      }
      if (
        StatusCode == 2 ||
        StatusCode == 463 ||
        StatusCode == 451 ||
        StatusCode == 28 ||
        StatusCode == 39 ||
        StatusCode == 512 ||
        StatusCode == 548

      ) {
        StatusIcon = "fa fa-circle text-success f-s-11 p-r-5";
      }
      if (
        StatusCode == 3 ||
        StatusCode == 32 ||
        StatusCode == 462 ||
        StatusCode == 464 ||
        StatusCode == 453 ||
        StatusCode == 454 ||
        StatusCode == 455 ||
        StatusCode == 30 ||
        StatusCode == 31 ||
        StatusCode == 40 ||

        StatusCode == 549 ||
        StatusCode == 494 || StatusCode == 495 || StatusCode == 511
      ) {
        StatusIcon = "fa fa-circle text-danger f-s-11 p-r-5";
      } if (
        //orderstatus - active and out for delivery
        StatusCode == 486 || StatusCode == 487 || StatusCode == 488 || StatusCode == 489 || StatusCode == 491 || StatusCode == 492
      ) {
        StatusIcon = "fa fa-circle text-primary f-s-11 p-r-5";
      }
      if (
        //orderstatus - delivery failed
        StatusCode == 493 ||
        StatusCode == 550

      ) {
        StatusIcon = "fa fa-circle text-dark f-s-11 p-r-5";
      }
      return StatusIcon;
    } else {
      if (
        StatusCode == "default.suspended" ||
        StatusCode == "default.inactive" ||
        StatusCode == "transaction.pending" ||
        StatusCode == "transaction.processing" ||
        StatusCode == "product.inactive" ||
        StatusCode == "invoice.pending" ||
        StatusCode == "deal.approvalpending" || StatusCode == "deal.paused" ||
        // order status
        StatusCode == "orderstatus.ready" || StatusCode == "orderstatus.readytopickup" || StatusCode == "orderstatus.outfordelivery"
      ) {
        StatusIcon = "fa fa-circle text-warning f-s-11 p-r-5 ";
      }
      if (
        StatusCode == "default.active" ||
        StatusCode == "transaction.success" ||
        StatusCode == "orderstatus.delivered" ||
        StatusCode == "invoice.cancelled" ||
        StatusCode == "invoice.paid" ||
        StatusCode == "deal.published" || StatusCode == "deal.approved"
      ) {
        StatusIcon = "fa fa-circle text-success f-s-11 p-r-5";
      }
      if (
        StatusCode == "default.disabled" ||
        StatusCode == "default.blocked" ||
        StatusCode == "transaction.error" ||
        StatusCode == "transaction.failed" ||
        StatusCode == "transaction.cancelled" ||
        StatusCode == "product.outofstock" ||
        StatusCode == "invoice.cancelled" ||
        StatusCode == "invoice.expired"
        || StatusCode == "orderstatus.cancelledbyuser" || StatusCode == "orderstatus.cancelledbyseller" || StatusCode == "orderstatus.cancelledbysystem"
      ) {
        StatusIcon = "fa fa-circle text-danger f-s-11 p-r-5";
      } if (StatusCode == "orderstatus.new" || StatusCode == "orderstatus.pendingconfirmation" || StatusCode == "orderstatus.confirmed" || StatusCode == "orderstatus.preparing"
      ) {
        StatusIcon = "fa fa-circle text-primary f-s-11 p-r-5";
      }
      if (StatusCode == "orderstatus.deliveryfailed" ||
        StatusCode == "product.notavailable" ||
        StatusCode == "product.draft"

      ) {
        "fa fa-circle text-dark f-s-11 p-r-5"
      }
      return StatusIcon;
    }
  }

  GetStatusBadge(StatusCode: any) {
    var ColorCyan = "badge cyan";
    var ColorSuccess = "badge badge-success";
    var ColorWarning = "badge badge-warning";
    var ColorDanger = "badge badge-danger";
    var ColorPrimary = "badge badge-primary";
    var ColorDark = "badge badge-dark";
    var Colorteal = "badge badge-teal"
    var StatusIcon = "";
    if (isNaN(StatusCode) == false) {
      if (
        StatusCode == 293 ||
        StatusCode == 294 ||
        StatusCode == 1 ||
        StatusCode == 450 ||
        StatusCode == 452 ||
        StatusCode == 4 ||
        StatusCode == 299 ||
        StatusCode == 298 ||
        StatusCode == 29 ||
        StatusCode == 27 ||
        StatusCode == 461 ||
        StatusCode == 38 ||
        // order status - ready to ship
        StatusCode == 490 ||
        // product status - inactive
        StatusCode == 547 ||
        StatusCode == 557 ||
        StatusCode == 631 ||
        StatusCode == 717 ||
        StatusCode == 678
      ) {
        StatusIcon = ColorWarning;
      }
      if (
        StatusCode == 2 ||
        StatusCode == 28 ||
        StatusCode == 451 ||
        StatusCode == 295 ||
        StatusCode == 297 ||
        StatusCode == 463 ||
        StatusCode == 39 ||
        //order status - delivered
        StatusCode == 512 ||
        // product status - Success
        StatusCode == 548 ||
        StatusCode == 559 ||

        StatusCode == 680 ||
        StatusCode == 722

      ) {
        StatusIcon = ColorSuccess;
      }
      if (
        StatusCode == 3 ||
        StatusCode == 32 ||
        StatusCode == 30 ||
        StatusCode == 453 ||
        StatusCode == 454 ||
        StatusCode == 455 ||
        StatusCode == 31 ||
        StatusCode == 296 ||
        StatusCode == 300 ||
        StatusCode == 462 ||
        StatusCode == 464 ||
        StatusCode == 301 ||
        StatusCode == 40 ||
        StatusCode == 681 ||
        StatusCode == 721 ||
        StatusCode == 720 ||
        //orderstatus - cancelled
        StatusCode == 494 || StatusCode == 495 || StatusCode == 511 ||
        // product status - OutOfStock
        StatusCode == 549 || StatusCode == 493 || StatusCode == 560
      ) {
        StatusIcon = ColorDanger;
      }
      if (
        //orderstatus - active and out for delivery
        StatusCode == 679 ||

        StatusCode == 486 || StatusCode == 487 || StatusCode == 488 || StatusCode == 489 || StatusCode == 491 || StatusCode == 492
        || StatusCode == 558 ||
        StatusCode == 718
      ) {
        StatusIcon = ColorPrimary;
      }
      if (
        //orderstatus - delivery failed
        StatusCode == 550 ||
        StatusCode == 682 ||
        StatusCode == 712 ||

        StatusCode == 695
      ) {
        StatusIcon = ColorDark;
      }
      return StatusIcon;
    } else {

      if (
        StatusCode == "campaign.paused" ||
        StatusCode == "campaign.creating" ||
        StatusCode == "campaign.underreview" ||
        StatusCode == "campaign.lowbalance" ||
        StatusCode == "default.inactive" ||
        StatusCode == "default.suspended" ||
        StatusCode == "transaction.pending" ||
        StatusCode == "invoice.pending" ||
        StatusCode == "transaction.processing" ||
        StatusCode == "transaction.initialized" ||
        StatusCode == "cashoutstatus.initialized" ||
        StatusCode == "paymentstatus.processing" ||
        StatusCode == "published" ||
        // order status
        StatusCode == "orderstatus.ready" || StatusCode == "orderstatus.readytopickup" || StatusCode == "orderstatus.outfordelivery" ||
        //product status
        StatusCode == "product.inactive" ||
        //Deal status
        StatusCode == "deal.paused" ||

        StatusCode == "dealcode.used" ||
        StatusCode == "paymentstatus.initialized" ||
        StatusCode == "paymentstatus.processing"


      ) {
        StatusIcon = ColorWarning;
      }
      if (
        StatusCode == "campaign.approved" ||
        StatusCode == "campaign.published" ||
        StatusCode == "default.active" ||
        StatusCode == "default.padi" ||
        StatusCode == "transaction.success" ||
        StatusCode == "orderstatus.delivered" ||
        StatusCode == "product.active" ||
        //Deal status
        StatusCode == "deal.published" ||
        StatusCode == "cashoutstatus.success" ||

        StatusCode == "dealcode.unused" ||
        StatusCode == "completed" ||
        StatusCode == "paymentstatus.success" ||
        StatusCode == "sent"

      ) {
        StatusIcon = ColorSuccess;
      }
      if (
        StatusCode == "campaign.expired" ||
        StatusCode == "campaign.archived" ||
        StatusCode == "campaign.rejected" ||
        StatusCode == "default.disabled" ||
        StatusCode == "default.blocked" ||
        StatusCode == "transaction.error" ||
        StatusCode == "transaction.failed" ||
        StatusCode == "transaction.cancelled" ||
        StatusCode == "invoice.cancelled" ||
        StatusCode == "product.outofstock" ||
        StatusCode == "cancelled" ||
        StatusCode == "rejected" ||
        StatusCode == "expired" ||
        StatusCode == "orderstatus.deliveryfailed" || StatusCode == "orderstatus.cancelledbyuser" || StatusCode == "orderstatus.cancelledbyseller" || StatusCode == "orderstatus.cancelledbysystem"
        ||
        StatusCode == "cashoutstatus.failed" ||
        //Deal status
        StatusCode == "deal.paused"
        ||
        //Deal status
        StatusCode == "deal.expired" ||
        StatusCode == "dealcode.blocked" ||
        StatusCode == "paymentstatus.failed" ||
        StatusCode == "cashoutstatus.rejected" ||
        StatusCode == "dealcode.expired" || StatusCode == "deal.rejected"


      ) {
        StatusIcon = ColorDanger;
      }

      if (

        StatusCode == "deal.approvalpending"
      ) {
        StatusIcon = ColorWarning;
      }
      if (StatusCode == "orderstatus.new" || StatusCode == "orderstatus.pendingconfirmation" || StatusCode == "orderstatus.confirmed" || StatusCode == "orderstatus.preparing"
        ||
        //Deal status
        StatusCode == "deal.draft" ||
        StatusCode == "running" ||

        StatusCode == "cashoutstatus.processing"
      ) {
        StatusIcon = ColorPrimary;
      }
      if (StatusCode == "product.notavailable" ||
        StatusCode == "draft" ||
        StatusCode == "deal.approved") {
        StatusIcon = ColorDark;
      }

      return StatusIcon;
    }
  }
  GetStatusColor(StatusCode: any) {
    var ColorSuccess = "#20ab68";
    var ColorWarning = "#fda61c";
    var ColorDanger = "#e9223f";
    var ColorPrimary = "#3735ff";
    var ColorDark = "#1e2435";
    var Colorteal = "#00cccc"

    var StatusIcon = "";
    if (isNaN(StatusCode) == false) {
      if (
        StatusCode == 1 ||
        StatusCode == 4 ||
        StatusCode == 450 ||
        StatusCode == 452 ||
        StatusCode == 29 ||
        StatusCode == 461 ||
        StatusCode == 27 ||
        // order status - ready to ship
        StatusCode == 490 ||
        StatusCode == 547 ||
        StatusCode == 38

      ) {
        StatusIcon = ColorWarning;
      }
      if (
        StatusCode == 2 ||
        StatusCode == 451 || StatusCode == 463 ||
        StatusCode == 28 ||
        //order status - delivered
        StatusCode == 512 ||
        StatusCode == 548 ||
        StatusCode == 39
      ) {
        StatusIcon = ColorSuccess;
      }
      if (
        StatusCode == 3 ||
        StatusCode == 32 ||
        StatusCode == 453 ||
        StatusCode == 454 ||
        StatusCode == 455 ||
        StatusCode == 462 ||
        StatusCode == 464 ||
        StatusCode == 30 ||
        StatusCode == 31 ||
        //orderstatus - cancelled
        StatusCode == 494 || StatusCode == 495 || StatusCode == 511 ||
        StatusCode == 549 ||
        StatusCode == 40
      ) {
        StatusIcon = ColorDanger;
      }
      if (
        //orderstatus - active and out for delivery
        StatusCode == 486 || StatusCode == 487 || StatusCode == 488 || StatusCode == 489 || StatusCode == 491 || StatusCode == 492
      ) {
        StatusIcon = ColorPrimary;
      }
      if (
        //orderstatus - delivery failed
        StatusCode == 493 ||
        StatusCode == 550

      ) {
        StatusIcon = ColorDark;
      }
      return StatusIcon;
    } else {
      if (
        StatusCode == "default.inactive" ||
        StatusCode == "default.suspended" ||
        StatusCode == "transaction.pending" ||
        StatusCode == "transaction.processing" ||
        StatusCode == "product.inactive" ||
        StatusCode == "invoice.pending" ||
        StatusCode == 'deal.paused' ||

        // order status
        StatusCode == "orderstatus.ready" || StatusCode == "orderstatus.readytopickup" || StatusCode == "orderstatus.outfordelivery"
      ) {
        StatusIcon = ColorWarning;
      }
      if (
        StatusCode == "default.active" ||
        StatusCode == "transaction.success" ||
        StatusCode == "product.active" ||
        StatusCode == "orderstatus.delivered" ||
        StatusCode == "invoice.paid" ||
        StatusCode == 'deal.published' ||
        StatusCode == 'deal.approved' ||
        StatusCode == "sent"
      ) {
        StatusIcon = ColorSuccess;
      }
      if (
        StatusCode == "default.disabled" ||
        StatusCode == "default.blocked" ||
        StatusCode == "transaction.error" ||
        StatusCode == "transaction.failed" ||
        StatusCode == "transaction.cancelled" ||
        StatusCode == "product.outofstock" ||
        StatusCode == "invoice.cancelled" ||
        StatusCode == "invoice.expired" ||
        StatusCode == 'deal.approvalpending' ||
        StatusCode == "orderstatus.cancelledbyuser" || StatusCode == "orderstatus.cancelledbyseller" || StatusCode == "orderstatus.cancelledbysystem"
      ) {
        StatusIcon = ColorDanger;
      } if (StatusCode == "orderstatus.new" || StatusCode == "orderstatus.pendingconfirmation" || StatusCode == "orderstatus.confirmed" || StatusCode == "orderstatus.preparing"
      ) {
        StatusIcon = ColorPrimary;
      }
      if (
        StatusCode == "orderstatus.deliveryfailed" ||
        StatusCode == "product.notavailable" ||
        StatusCode == "invoice.draft" ||
        StatusCode == "deal.draft"
      ) {
        StatusIcon = ColorDark;
      }
      if (
        StatusCode == "deal.approved"
      ) {
        StatusIcon = Colorteal;
      }
      return StatusIcon;
    }
  }
  GetTimeDifferenceS(Date: any, CompareTo: any) {
    return this.GetTimeDifferenceSs(Date, CompareTo);
  }
  TerminalsBubbleSort(terminals: any[]) {
    let swapped;
    do {
      swapped = false;
      for (let i = 0; i < terminals.length; i++) {

        if (terminals[i + 1] == undefined) {
          continue;
        }

        if (moment(terminals[i].LastTransactionDate).isBefore(moment(terminals[i + 1].LastTransactionDate))) {
          let tmp = terminals[i];
          terminals[i] = terminals[i + 1];
          terminals[i + 1] = tmp;
          swapped = true;
        }
      }
    } while (swapped);

    return terminals;
  }

  public SortTerminals(Data: any[]): void {
    this.ResetSortedTerminals();

    this.SortedTerminals.All = Data;

    var SevenDayBefore = moment().startOf("day").subtract(6, "d");
    var ThirtyDayBefore = moment().startOf("day").subtract(30, "d");

    for (let index = 0; index < Data.length; index++) {

      const TClone = lodash.cloneDeep(Data[index]);

      var IsNotLastTransactionDate: boolean = (TClone.LastTransactionDate == undefined) || (TClone.LastTransactionDate == null);

      if (IsNotLastTransactionDate) {
        this.SortedTerminals.UnUsed.push(TClone);
        Data[index].ActivityStatus = "Unused";
        continue;
      }

      var TCloneDate = moment(TClone.LastTransactionDate);

      if (TCloneDate.isBefore(ThirtyDayBefore)) {
        this.SortedTerminals.Dead.push(TClone);
        Data[index].ActivityStatus = this.POSActivityStatus.Dead;
      } else if (TCloneDate.isBefore(SevenDayBefore)) {
        this.SortedTerminals.Idle.push(TClone);
        Data[index].ActivityStatus = this.POSActivityStatus.Idle;
      } else if (TCloneDate.isAfter(SevenDayBefore)) {
        this.SortedTerminals.Active.push(TClone);
        Data[index].ActivityStatus = this.POSActivityStatus.Active;
      } else {
        this.SortedTerminals.UnUsed.push(TClone);
        Data[index].ActivityStatus = this.POSActivityStatus.Unused;
      }
    }
  }

  //moved methods from helper
  GetDateSs(Date, TimeZone, Format) {
    try {
      if (Date != undefined && Date != null) {
        var TDate = moment.tz(Date, 'GMT');
        var FDate = moment.tz(TDate, TimeZone).format(Format);
        return FDate;
      }
      else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }
  GetTimeSs(Date, TimeZone, Format) {
    try {
      if (Date != undefined && Date != null) {
        var TDate = moment.tz(Date, 'GMT');
        var FDate = moment.tz(TDate, TimeZone).format(Format);
        return FDate;
      }
      else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }
  GetDateTimeSs(Date, TimeZone, Format) {
    try {
      if (Date != undefined && Date != null) {
        var TDate = moment.tz(Date, 'GMT');
        var FDate = moment.tz(TDate, TimeZone).format(Format);
        return FDate;
      }
      else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }
  GetDateTimes(Date, TimeZone) {
    try {
      if (Date != undefined && Date != null) {
        var TDate = moment.tz(Date, 'GMT');
        var FDate = moment.tz(TDate, TimeZone);
        return FDate;
      }
      else {
        return new Date();
      }
    } catch (error) {
      return new Date();
    }
  }
  GetTimeDifferenceSs(Date, CompareTo) {
    var date1 = moment.tz(Date, 'GMT'),
      date2 = moment.tz(CompareTo, 'GMT');
    var Duration = moment.duration(date2.diff(date1));
    var DiffernceO =
    {
      Years: 0,
      Month: 0,
      Weeks: 0,
      Days: 0,
      Hours: 0,
      Minutes: 0,
      Seconds: 0
    }
    DiffernceO.Years = Math.round(Math.abs(Duration.asYears()));
    DiffernceO.Month = Math.round(Math.abs(Duration.asMonths()));
    DiffernceO.Weeks = Math.round(Math.abs(Duration.asWeeks()));
    DiffernceO.Days = Math.round(Math.abs(Duration.asDays()));
    DiffernceO.Hours = Math.round(Math.abs(Duration.asHours()));
    DiffernceO.Minutes = Math.round(Math.abs(Duration.asMinutes()));
    DiffernceO.Seconds = Math.round(Math.abs(Duration.asSeconds()));
    if (DiffernceO.Years > 0) {
      return DiffernceO.Years + ' years';
    }
    else if (DiffernceO.Month > 0) {
      return DiffernceO.Years + ' months';
    }
    else if (DiffernceO.Days > 0) {
      return DiffernceO.Days + ' days';
    }
    else if ((DiffernceO.Hours) > 0) {
      return (DiffernceO.Hours) + ' hours ' + (DiffernceO.Minutes - ((DiffernceO.Hours - 1) * 60)) + ' mins';
    }
    else if (DiffernceO.Minutes > 0) {
      return DiffernceO.Minutes + ' min';
    }
    else {
      return DiffernceO.Seconds + ' sec';
    }
  }



  //End of moved methods
  ResetSortedTerminals(): void {
    this.SortedTerminals.Active = [];
    this.SortedTerminals.Idle = [];
    this.SortedTerminals.Dead = [];
    this.SortedTerminals.UnUsed = [];
  }
  DateInUTC(dt: any): any {
    try {
      var m = dt;
      var timezoneoffset = -(moment().zone());
      var m: any = moment(dt).utc().add(timezoneoffset, 'minutes');
      return m;
    } catch (error) {
      return dt;
    }

  }


  GetSearchConditionStrictFromArray(
    BaseString,
    ColumnName,
    ColumnType,
    ColumnValueArray,
    Condition
  ) {
    if (BaseString == undefined || BaseString == null) {
      BaseString = "";
    }
    var SearchExpression = BaseString;
    if (
      ColumnName != undefined &&
      ColumnType != undefined &&
      ColumnName != null &&
      ColumnType != null &&
      ColumnName != "" &&
      ColumnType != "" //&&
    ) {
      if (
        ColumnValueArray != undefined ||
        ColumnValueArray != null ||
        ColumnValueArray.length != 0
      ) {
        if (ColumnType == "text") {
          var TExpression = "";
          for (let index = 0; index < ColumnValueArray.length; index++) {
            const ColumnValue = ColumnValueArray[index];
            if (TExpression != "") {
              TExpression += " OR ";
            }
            if (ColumnValue != null && ColumnValue != "") {
              TExpression +=
                ColumnName + " " + Condition + ' "' + ColumnValue + '" ';
            } else {
              TExpression +=
                " " + ColumnName + " " + Condition + " null " + " ";
            }
          }
          if (TExpression != "") {
            if (SearchExpression != "") {
              SearchExpression =
                "( " + SearchExpression + ") AND (" + TExpression + ")";
            } else {
              SearchExpression = TExpression;
            }
          }
        } else if (ColumnType == "number") {
          var TColSearch = "";
          if (ColumnValueArray.length == 1) {
            const ColumnValue = ColumnValueArray[0];
            if (ColumnValue != null && ColumnValue != "") {
              TColSearch +=
                " " + ColumnName + " " + Condition + ' "' + ColumnValue + '"';
            } else {
              TColSearch += " " + ColumnName + " " + Condition + " " + "";
            }
          } else {
            for (let index = 0; index < ColumnValueArray.length; index++) {
              const ColumnValue = ColumnValueArray[index];
              if (TColSearch != "") {
                TColSearch += " OR ";
              }
              if (ColumnValue != null && ColumnValue != "") {
                TColSearch +=
                  " " +
                  ColumnName +
                  " " +
                  Condition +
                  ' "' +
                  ColumnValue +
                  '" ';
              } else {
                TColSearch += "  " + ColumnName + " " + Condition + " " + " ";
              }
            }
          }
          if (TColSearch != "") {
            if (SearchExpression != "") {
              SearchExpression =
                "( " + SearchExpression + ") AND (" + TColSearch + ")";
            } else {
              SearchExpression = TColSearch;
            }
          }
        }
      }
      return SearchExpression;
    } else {
      return BaseString;
    }
  }

  MakeFilterSnapPermanent(): void {
    this.FilterSnap = lodash.cloneDeep(this.FilterSnapTemprary);
    this.ComputeFilterState();
  }

  ResetFilterSnap(): void {
    var index: number = (this.FilterSnap.id < 1) ? 0 : (this.FilterSnap.id - 1);
    this.FilterSnapTemprary = lodash.cloneDeep(this.Active_FilterOptions[index]);
    this.FilterSnap = lodash.cloneDeep(this.Active_FilterOptions[index]);
  }

  RemoveItemFromArrayByField(ItemNameToRemove, DataSet: any[]) {
    for (let index = 0; index < DataSet.length; index++) {
      const element = DataSet[index];
      if (element.includes(ItemNameToRemove)) {
        DataSet.splice(index, 1);
      }
    }
    return DataSet;
  }

  OpenModal(ModalId) {
    setTimeout(() => {
      $("#" + ModalId).modal("show");
    }, 100);
  }
  CloseModal(ModalId) {
    $("#" + ModalId).modal("hide");
  }

  GetFixedDecimalNumber(Value:any) {
    if (Value != undefined && Value != null) {
      return Math.round((Value + Number.EPSILON) * 100) / 100;
    }
    else {
      return Value;
    }
  }
  GetPercentageFromNumber(SmallNumber, BigNumber) {
    return (SmallNumber / BigNumber) * 100;
  }
  GetAmountFromPercentage(Amount, Percentage) {
    return this.GetFixedDecimalNumber((Amount * Percentage) / 100);
  }

  PreventTextButAcceptDecimal(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar) && (event.which < 48 && event.keyCode !== 46) || event.which > 57) {
      event.preventDefault();
    }
  }

  defaultImage(event:any) {
    event.target.src = "assets/img/Dummy.png";
  }
  scrollTop=()=>
  {
    window.scroll({ 
      top: 0, 
      left: 0, 
      behavior: 'smooth' 
    });
  }

}

enum HostType {
  Live,
  Test,
  Tech,
  Dev
}


