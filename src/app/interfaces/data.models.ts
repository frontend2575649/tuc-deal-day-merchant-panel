export interface OResponse {
    Status: string;
    EndDate: any;
    Message: string;
    ResponseCode: string;
    Result: any;
}


export interface ONetworkResponse {
    zx: string;
    fx: string;
    vx: string;
}
export interface HCXAddress {
    AddressId?: string | null;
    Address?: string | null;
    Latitude?: number | null;
    Longitude?: number | null;

    CityAreaId?: number | null;
    CityAreaCode?: string | null;
    CityAreaName?: string | null;


    CityId?: number | null;
    CityCode?: string | null;
    CityName?: string | null;

    StateId?: number | null;
    StateCode?: string | null;
    StateName?: string | null;

    CountryId?: number | null;
    CountryCode?: string | null;
    CountryName?: string | null;

    PostalCode?: string | null;
    MapAddress?: string | null;
}