export class OAccessUser {
    public Name!: string;
    public FirstName!: string;
    public LastName!: string;
    public UserName!: string;
    public EmailAddress!: string;
    public ContactNumber!: string;
    public MobileNumber!: string;
    public Gender!: string;
    public GenderCode!: string;
    public DateOfBirth!: Date;
    public Address!: string;
    public AddressLatitude: number = 0;
    public AddressLongitude: number = 0;
    public EmailVerificationStatus: number = 0;
    public ContactNumberVerificationStatus: number = 0;
}

export class OAccessUserAccount {
    public AccountId: number = 0;
    public ActiveOwnerId: number = 0;
    public ActiveOwnerKey!: string;
    public DisplayName!: string;
    public AccountKey!: string;
    public IsTucPlusEnabled!: boolean;
    public AccountType!: string;
    public AccountTypeCode!: string;
    public AccountCode!: string;
    public EmailAddress!: string;
    public ReferralUrl!: string;
    public ReferralCode!: string;
    public RoleKey!: string;
    public RoleName!: string;
    public IconUrl!: string;
    public PosterUrl!: string;
    public MerchantIconUrl!: string;
    public IsAccessPinSet: number = 0;
    public Permissions: any[] = [];
    public SystemNamePermissions: any[] = [];
    public SystemSubscription: any = {};
    public CreateDate!: string;
    public CreateDateS!: string;
  }
  export class OAccessUserAccountOwner {
    public AccountTypeCode: number = 0;
    public AccountId: number = 0;
    public AccountKey!: string;
    public DisplayName!: string;
    public Name!: string;
    public AccountCode!: string;
    public IconUrl!: string;
    public IsTucPlusEnabled!: boolean;
  }
  export class OAccessUserCountry {
    public CountryId!: number;
    public CountryKey!: string;
    public CountryName!: string;
    public CountryIso!: string;
    public CountryIsd!: string;
    public CurrencyName!: string;
    public CurrencyNotation!: string;
    public CurrencySymbol!: string;
    public CurrencyCode!: string;
  }
  export class OAccessUserAccountRole {
    public Name!: string;
    public SystemName!: string;
    public RolePermissions!: Array<OAccessUserAccountRolePermission>;
  }
  export class OAccessUserAccountRolePermission {
    public Name!: string;
    public SystemName!: string;
    public IsDefault: number = 0;
    public IsAccessPinRequired: number = 0;
    public IsPasswordRequired: number = 0;
  }
  
  export class OAccessUserLocation {
    public City!: string;
  
    public CountryCode!: string;
    public CountryName!: string;
  
    public RegionCode!: string;
    public RegionName!: string;
  
    public ZipCode!: string;
    public TimeZone!: string;
    public Latitude!: string;
    public Longitude!: string;
  
    public UpdateTime!: string;
  }
  export class OAccessUserDevice {
    public SerialNumber!: string;
  }