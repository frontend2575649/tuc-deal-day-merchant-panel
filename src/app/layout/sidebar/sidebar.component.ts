import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LogoutModalComponent } from 'src/app/shared/components/logout-modal/logout-modal.component';
import { HelperService } from 'src/app/utils';

declare var $:any;
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent {
  constructor(
    public _HelperService: HelperService,
    private _Router: Router,
    public modalService: NgbModal,
  ) { }

  collapseBar()
  {
      let body = document.querySelector('body')
      let main = document.querySelector('.main-wrapper')
      if(body.classList.contains('isCollapsed')){
          body.classList.remove('isCollapsed')
      }else{
        body.classList.add("isCollapsed");
        main.classList.add("isCollapsed");
      }
  }



  confirmLogout() {
    this.modalService.open(LogoutModalComponent, 
        { ariaLabelledBy: 'create-store-modal-body', backdrop: 'static', size: 'md' })
  }
}
