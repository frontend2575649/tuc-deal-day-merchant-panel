import { Component, OnInit } from '@angular/core';
import { HelperService } from 'src/app/utils';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor( 
    private Helper: HelperService
  ) { }

  ngOnInit() {
  }
  returnToDashboard()
  {
    this.Helper._Router.navigate(['/'])
  }

}
