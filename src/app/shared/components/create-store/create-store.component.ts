import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { HCXAddress, OResponse } from 'src/app/interfaces';
import { HelperService } from 'src/app/utils';

@Component({
    selector: 'app-create-store',
    templateUrl: './create-store.component.html',
    styleUrls: ['./create-store.component.css']
})
export class CreateStoreComponent implements OnInit {
    _modal: any;
    imageChangedEvent: any = '';
    croppedImage: any = '';
    _editStoreItem: any;
    editStore:boolean = false;
    modalTitle: string = 'Set up your store';
    @Output('reloadList') reloadList = new EventEmitter<any>();
    @Input('modal') set modal(value: any) {
        if (value) {
            this.initForm();
            this._modal = value;
        }
    }
    @Input('editStoreItem') set editStoreItem(value: any) {
        this._editStoreItem = value;
        if (value) {
            this.setStoreForm(value);
        }
    }
    StoreForm!: FormGroup;
    _Address: HCXAddress;
    isFormSubmitted: boolean = false;
    public BusinessCategories = [];
    public S2BusinessCategories: any = [];
    public ShowCategorySelector: boolean = false;
    public errorMessages: any = {
        storeNameRequired: 'Store name is required',
        storeNameLength: 'Store name must be between 4 - 256 characters',
        storeManagerPhoneNumberRequired: 'Phone number is required',
        storeManagerPhoneNumberLength: 'Phone number must be between 8 - 14 number',
        bankName: 'Bank name is required',
        bankNameLength: 'Bank name must be between 4 - 100 characters',
        storeCategoryrequired: 'Store category is required',
        addressRequired: 'Detailed Address is required',
        stateRequired: 'State is required',
        cityRequired: 'City is required'
    }
    cars: any = [1, 2, 3, 4];
    constructor(
        private modalService: NgbModal,
        public _HelperService: HelperService,
        private fb: FormBuilder
    ) { }
    ngOnInit(): void {
        this.GetBusinessCategories();
    }
    initForm() {
        this.StoreForm = this.fb.group({
            Task: this._HelperService.AppConfig.Api.ThankUCash.SaveStore,
            MerchantId: this._HelperService.UserAccount.AccountId,
            MerchantKey: this._HelperService.UserAccount.AccountKey,
            StoreDisplayName: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(256)])],
            StoreManagerMobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
            CategoryDetails: ['', Validators.required],
            PostalCode: [''],
        });
        this._HelperService.Icon_Crop_Clear();
    }
    countryChange(eevnt: any) {
    }
    storeFormControl(control: any): any {
        return this.StoreForm.controls[control];
    }
    AddressChange(Address: any) {
        this._Address = Address;
    }

    GetBusinessCategories() {
        this._HelperService.IsFormProcessing = true;
        this.ShowCategorySelector = false;
        let PData = {
            Task: this._HelperService.AppConfig.Api.Core.getcategories,
            SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
            SortExpression: 'Name asc',
            Offset: 0,
            Limit: 1000,
        }
        PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
            PData.SearchCondition,
            "TypeCode",
            this._HelperService.AppConfig.DataType.Text,
            this._HelperService.AppConfig.HelperTypes.MerchantCategories,
            "="
        );
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Op, PData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    if (_Response.Result.Data != undefined) {
                        this.BusinessCategories = _Response.Result.Data;

                        this.ShowCategorySelector = false;
                        // this._ChangeDetectorRef.detectChanges();
                        for (let index = 0; index < this.BusinessCategories.length; index++) {
                            const element: any = this.BusinessCategories[index];
                            this.S2BusinessCategories = [...this.S2BusinessCategories, {
                                id: element.ReferenceId,
                                key: element.ReferenceKey,
                                text: element.Name
                            }];
                        }
                        this.ShowCategorySelector = true;
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            },
            () => this._HelperService.IsFormProcessing = false
        );

    }
    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event.target.files;
        if (event.target?.files?.length >= 1) {
            this._HelperService._SetFirstImageOrNone(event.target.files);
        }
    }
    removeImage() {
        this._HelperService._Icon_Cropper_BaseImage = '';
        this._HelperService._Icon_Cropper_Image = '';
    }
    drop(ev: any) {
        ev.preventDefault();
        let file: any;
        if (ev.dataTransfer.items) {
            let item = ev.dataTransfer.items[0];
            if (item.kind === 'file') {
                file = item.getAsFile();
            }
        } else {
            file = ev.dataTransfer.files[0];
        }
        this._HelperService._SetFirstImageOrNone([file]);
    }
    SubmitForm() {
        let storeImage =  this._HelperService._Icon_Cropper_Data ? this._HelperService._Icon_Cropper_Image : null
        if(!storeImage){
            return this._HelperService.NotifyError("Please add a store image");
        }
        this.isFormSubmitted = true;
        if (!this._Address.Address == undefined || this._Address.Address == null || this._Address.Address == "" || this._Address.CityId < 1) {
            return this._HelperService.NotifyError("Please select store location");
        }
        else if (this.StoreForm.valid) {
            this._HelperService.IsFormProcessing = true;
            let pData: any = {
                ...this.StoreForm.value,
                CategoryDetails: this.setCategoryDetails(),
                StoreManagerMobileNumber:  this.editStore ? `${this.StoreForm.value.StoreManagerMobileNumber}` :
                                            `+234${this.StoreForm.value.StoreManagerMobileNumber}`,
                PostalCode: this._Address.PostalCode,
                StateId: this._Address.StateId,
                StateKey: this._Address.StateCode,
                StateName: this._Address.StateName,
                CityId: this._Address.CityId,
                CityKey: this._Address.CityCode,
                CityName: this._Address.CityName,
                Address: this._Address.Address,
                MapAddress: this._Address.MapAddress,
                Latitude: this._Address.Latitude,
                Longitude: this._Address.Longitude,
                IconContent: this._HelperService._Icon_Cropper_Data.Name ? this._HelperService._Icon_Cropper_Data : {}
            }
            if (this._editStoreItem) {
                let _Icon_Cropper_Data = this._HelperService._Icon_Cropper_Data;
                pData.ReferenceId = this._editStoreItem.ReferenceId;
                pData.ReferenceKey = this._editStoreItem.ReferenceKey;
                pData.AddressId = this._Address.AddressId;
                pData.IconContent = (_Icon_Cropper_Data && _Icon_Cropper_Data.Name) ? _Icon_Cropper_Data : null;
            }
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Store, pData);
            _OResponse.subscribe(
                _Response => {
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this.StoreForm.reset();
                        this.reloadList.emit({ reload: true, message: _Response.Message });
                        this._modal.close();
                        this._HelperService.Icon_Crop_Clear();
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                }, () => {
                    this._HelperService.IsFormProcessing = false;
                });
        }

    }
    setCategoryDetails() {
        let categoryD = [];
        if (this.StoreForm.valid && this.StoreForm.value.CategoryDetails) {
            this.StoreForm.value.CategoryDetails.forEach(formVal => {
                let catIndex = this.S2BusinessCategories.findIndex(e => e.id == formVal);
                if (catIndex > -1) {
                    categoryD.push({
                        CategoryId: this.S2BusinessCategories[catIndex].id,
                        CategoryKey: this.S2BusinessCategories[catIndex].key,
                        CategoryName: this.S2BusinessCategories[catIndex].text
                    })
                }
            });
        }
        return categoryD;
    }
    setStoreForm(StoreItem: any) {
        this._HelperService.IsFormProcessing = true;
        // get Details of Store API
        let pData: any = {
            Task: 'getstore',
            ReferenceId: StoreItem.ReferenceId,
            ReferenceKey: StoreItem.ReferenceKey
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Store, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    let StoreDetails = _Response.Result;
                    this.StoreForm.patchValue({
                        Task: this._HelperService.AppConfig.Api.ThankUCash.UpdateStore,
                        MerchantId: StoreDetails.MerchantId,
                        MerchantKey: StoreDetails.MerchantKey,
                        StoreDisplayName: StoreDetails.DisplayName,
                        StoreManagerMobileNumber: StoreDetails.MobileNumber,
                        BankDetails: StoreDetails.BankDetails,
                        CategoryDetails: StoreDetails.CategoryDetails && StoreDetails.CategoryDetails.map(e => e?.CategoryId),
                        PostalCode: StoreDetails.AddressDetails?.PostalCode,
                    });
                    this._Address = StoreDetails.AddressDetails;
                    this._HelperService._Icon_Cropper_Image = StoreDetails.IconUrl;
                    this.editStore = true;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }, () => {
                this._HelperService.IsFormProcessing = false;
            });
    }
}
