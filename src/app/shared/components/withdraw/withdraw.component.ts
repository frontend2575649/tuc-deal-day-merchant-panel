import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { HCXAddress, OResponse } from 'src/app/interfaces';
import { HelperService } from 'src/app/utils';

@Component({
    selector: 'app-withdraw-store',
    templateUrl: './withdraw.component.html',
    styleUrls: ['./withdraw.component.css']
})
export class withdrawModalComponent implements OnInit {

    PaymentForm:FormGroup;
    ConfigDetails:any;
    BankList:any;
    constructor(
        public activeModal: NgbActiveModal,
        private fb: FormBuilder,
        public _HelperService: HelperService
    )
    {
        this.GetConfiguration();
        this.fetchBanks()
    }
    ngOnInit(): void {
        this.PaymentForm = this.fb.group({
            BankId:['',[Validators.required]],
            BankKey:['', [Validators.required]],
            Amount:['', [Validators.required]]
        })
    }

    GetConfiguration()
    {
        this._HelperService.IsFormProcessing = false;
        let pData = {
            OperationType: "new",
            Task: "getsummarizecashoutconfiguration",
            AccountId: this._HelperService.UserAccount.AccountId,
            AccountKey: this._HelperService.UserAccount.AccountKey
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.CashOut, pData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
                this.ConfigDetails = _Response.Result.Deals as any;
              return;
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          });

    }

    get FormFields() { return this.PaymentForm.controls; }
    
    fetchBanks(){
        this._HelperService.IsFormProcessing = true;
        let pData = {
            Task: "getbankaccounts",
            TotalRecords: 0,
            Offset: 0,
            Limit: 10,
            RefreshCount: true,
            SearchCondition: `AccountId == \"${this._HelperService.AppConfig.AccountId}\"`,
            SortExpression: "",
            IsDownload: false,
            ReferenceKey: this._HelperService.AppConfig.AccountKey,
            ReferenceId: this._HelperService.AppConfig.AccountId,
       };
       let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Banks, pData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this.BankList = _Response.Result.Data as any;
              return;
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          });
      }
    
    InitiateCashout(data:FormGroup){
        this._HelperService.IsFormProcessing = true;
        let pData = {
            OperationType: "new",
            Task: "cashoutinitialize",
            AccountId:  this._HelperService.AppConfig.AccountId,
            AccountKey:  this._HelperService.AppConfig.AccountKey,
            StatusCode: "default.active",
            RedeemFrom: 776,
            ...data,  
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.CashOut, pData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
                this.PaymentForm.reset()
                this.activeModal.dismiss()
                this._HelperService.NotifySuccess(_Response.Message);
                window.location.reload()
              return;
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          });
    }

Stringify(data:string){ 
    return JSON.stringify(data) 
}

setBankAccount(data:any){
    let parsedSource = JSON.parse(data.target.value)
    this.PaymentForm.get('BankId').setValue(Number(parsedSource.BankId))
    this.PaymentForm.get('BankKey').setValue(Number(parsedSource.BankReferenceKey))
}

}