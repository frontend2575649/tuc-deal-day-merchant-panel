import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HelperService } from 'src/app/utils';
import html2pdf from 'html2pdf.js';

@Component({
    selector: 'app-transaction-store',
    templateUrl: './transaction-view.component.html',
    styleUrls: ['./transaction-view.component.css']
})
export class TransactionViewComponent implements OnInit {

    PaymentForm:FormGroup;
    @Input () item;
    constructor(
        public activeModal: NgbActiveModal,
        private fb: FormBuilder,
        public _HelperService: HelperService
    )
    {}
    ngOnInit(): void {
    }

    printPdf() {
        const content = document.getElementById('content-to-print');
        content.style.padding = '20px';
        const options = {
          filename: 'Invoice.pdf',
          image: { type: 'jpeg', quality: 0.98 },
          html2canvas: { scale: 2 },
          jsPDF: { 
            unit: 'in', 
            format: 'letter', 
            orientation: 'portrait', 
            // use 'addHTML' instead of 'from'
            // add padding/margin using 'marginLeft' and 'marginRight'
            // set 'autoPaging' to 'false' to ensure padding/margin is applied to all pages
            callback: function(pdf) {
              pdf.internal.scaleFactor = 2.834645669291339;
              const pageWidth = pdf.internal.pageSize.width - (pdf.internal.pageSize.width - pdf.internal.getPageSize().width) / 2;
              const margin = 0.5;
              pdf.addHTML(document.body, {
                pagesplit: false,
                margin: { top: margin, right: margin, bottom: margin, left: margin },
                marginLeft: pageWidth + margin,
                marginRight: pageWidth + margin,
                autoPaging: false
              }, function() {
                pdf.save();
              });
            }
          }
        };
    
        html2pdf()
          .set(options)
          .from(content)
          .save();
         setTimeout(() => {
            content.style.padding = null;
         }, 1500); 
    }

}