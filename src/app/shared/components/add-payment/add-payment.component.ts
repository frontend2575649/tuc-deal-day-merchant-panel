import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { OResponse } from 'src/app/interfaces';
import { HelperService } from 'src/app/utils';
import swal from 'sweetalert2';

@Component({
    selector: 'app-paymentadd-store',
    templateUrl: './add-payment.component.html',
    styleUrls: ['./add-payment.component.css']
})
export class addPaymentComponent implements OnInit {

    PaymentForm:FormGroup;
    BankList: any;
    task: string = 'savebankaccount';
    dataEdit:any;
    constructor(
        public activeModal: NgbActiveModal,
        private fb: FormBuilder,
        public _HelperService: HelperService
    )
    {
        this.fetchBanks()
    }
    ngOnInit(): void {
        this.PaymentForm = this.fb.group({
            Name: ['',[Validators.required, Validators.minLength(3)]],
            BankName: ['',[Validators.required, Validators.minLength(3)]],
            BankCode: ['',[Validators.required, Validators.minLength(3)]],
            AccountNumber: ['',[Validators.required, Validators.minLength(10)]],
            IsPrimaryAccount:[false]
        })
    }

    addAccount(data: FormGroup)
    {
    this._HelperService.IsFormProcessing = true;
    let pData =  
        {
            Task:this.task,
            AccountId: this._HelperService.AppConfig.AccountId,
            AccountKey: this._HelperService.AppConfig.AccountKey,
            ReferenceId: 0,
            ReferenceKey: "",
           ...data,
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Banks, pData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
                 this.BankList = _Response.Result as any;
                 this._HelperService.NotifySuccess(_Response.Message);
                 setTimeout(() => {
                   window.location.reload()
                 }, 900);
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          });
    }

    fetchBanks(){
        this._HelperService.IsFormProcessing = true;
        let pData = {
            Task: "getbankaccounts",
            TotalRecords: 0,
            Offset: 0,
            Limit: 10,
            RefreshCount: true,
            SearchCondition: `AccountId == \"${this._HelperService.AppConfig.AccountId}\"`,
            SortExpression: "",
            ReferenceKey: null,
            ReferenceId: 0,
            IsDownload: false
       };
       let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Banks, pData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this.BankList = _Response.Result.Data as any;
              return;
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          });
      }

    EditAccount(data:any){
        this.task = 'updatebankaccount'
        this.dataEdit = data
        this.PaymentForm.patchValue({
            Name: data.Name,
            AccountNumber: data.AccountNumber,
            BankName:data.BankName,
            BankCode: data.BankCode,
            IsPrimaryAccount: data.IsPrimaryAccount  
        })
        this.PaymentForm.get('Name')?.disable()
        this.PaymentForm.get('AccountNumber')?.disable()
        this.PaymentForm.get('BankName')?.disable()
        this.PaymentForm.get('BankCode')?.disable()
    }
    updateAccount(data: FormGroup)
    {
        this._HelperService.IsFormProcessing = true;
        let pData = {
            ReferenceId: this.dataEdit.ReferenceId,
            ReferenceKey: this.dataEdit.ReferenceKey,
            ...this.dataEdit,
            ...data,
            Task:"updatebankaccount",  
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Banks, pData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
                this.BankList = _Response.Result as any;
                this._HelperService.NotifySuccess(_Response.Message);
                 setTimeout(() => {
                   window.location.reload()
                 }, 900);
              return;
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          });
    }

    DeleteAccount(data:any){
        swal.fire({
            title: "<h6>Are you sure you want to delete this Account?</h6>",
            customClass: "",
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Continue",
            cancelButtonText: "Cancel"
        }).then((result) => {
            if (result.value) {
                let pData = {
                    Task: "deletebankaccount",
                    ReferenceKey: data.ReferenceKey,
                    ReferenceId: data.ReferenceId
                }
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Banks, pData);
                _OResponse.subscribe(
                    _Response => {
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess(_Response.Message);
                            setTimeout(() => {
                                window.location.reload()
                            }, 900);
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });
    }

}