import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { HCXAddress, OResponse } from 'src/app/interfaces';
import { HelperService } from 'src/app/utils';

@Component({
  selector: 'app-regaddress',
  templateUrl: './regaddress.component.html',
  styleUrls: ['./regaddress.component.css']
})
export class RegaddressComponent implements OnInit {


  MapAddress: any;
  center: any = { lat: 9.0338725, lng: 8.677457 };
  zoom = 12;
  options: google.maps.MapOptions = {
    center: { lat: 9.0338725, lng: 8.677457 },
    zoom: 12,
    fullscreenControl: false,
    streetViewControl: false,
    mapTypeControl: false,
    panControl: true
  };
  markerOptions: any = { draggable: false };
  markerPositions: any[] = [];
  markerPosition: any;
  public validState = "";
  IsAddressSet: boolean = false;
  ShowCity: boolean = true;
  ShowState: boolean = true;
  HCXLocManager_S2States_Data: any[] = [];
  HCXLocManager_S2Cities_Data: any[] = [];
  countryData:any;
  country_List = [
    {
        ReferenceId: 1,
        ReferenceKey: "nigeria",
        Name: "Nigeria",
        IconUrl: "../../../../assets/images/countries/nigeria_icon.png",
        Isd: "234",
        Iso: "ng",
        CurrencyName: "Naira",
        CurrencyNotation: "NGN",
        CurrencySymbol: "₦",
        CurrencyHex: "&#8358;",
        NumberLength: 10
    },
    {
        ReferenceId: 87,
        ReferenceKey: "ghana",
        Name: "Ghana",
        IconUrl: "../../../../assets/images/countries/ghana_icon.png",
        Isd: "233",
        Iso: "gh",
        CurrencyName: "Cedi",
        CurrencyNotation: "GHS",
        CurrencySymbol: "₵",
        CurrencyHex: "&#8373;",
        NumberLength: 9
    },
    {
        ReferenceId: 118,
        ReferenceKey: "kenya",
        Name: "Kenya",
        IconUrl: "../../../../assets/images/countries/kenya_icon.png",
        Isd: "254",
        Iso: "key",
        CurrencyName: "Shilling",
        CurrencyNotation: "KHS",
        CurrencySymbol: "KSh",
        CurrencyHex: "KSh",
        NumberLength: 9
    }
]
  @Input('data') public data: any = {
    Address: null,
    Latitude: null,
    Longitude: null,
    CityAreaId: null,
    CityAreaCode: null,
    CityAreaName: null,
    CityId: null,
    CityCode: null,
    CityName: null,
    StateId: null,
    StateCode: null,
    StateName: null,
    CountryId: null,
    CountryCode: null,
    CountryName: null,
    PostalCode: null,
    MapAddress: null,
  };
  @Output('OnAddressSet') AddressComponent = new EventEmitter<HCXAddress>();
  constructor(
    private _HelperService: HelperService
  ) { }
  ngOnInit(): void {
    if (this.data.CountryCode == undefined || this.data.CountryCode == null || this.data.CountryCode == "") {
      this.data.CountryId = this._HelperService.UserCountry.CountryId;
      this.data.CountryCode = this._HelperService.UserCountry.CountryKey;
      this.data.CountryName = this._HelperService.UserCountry.CountryName;
    }
    this.IsAddressSet = true;
    this.HCXLoc_Manager_GetStates();
  }
  HCXLocManager_AddressChange(event: any) {
    this.MapAddress = event.formatted_address;
    this.center = { lat: event.geometry.location.lat(), lng: event.geometry.location.lng() }
    this.markerPosition = { lat: event.geometry.location.lat(), lng: event.geometry.location.lng() }
    this.data.Latitude = event.geometry.location.lat();
    this.data.Longitude = event.geometry.location.lng();
    this.data.MapAddress = event.formatted_address;
    this.data.Address = event.formatted_address;

    this.data.CityAreaId = 0;
    this.data.CityAreaCode = null;
    this.data.CityAreaName = null;

    this.data.CityId = 0;
    this.data.CityCode = null;
    this.data.CityName = null;

    this.data.StateId = 0;
    this.data.StateCode = null;
    this.data.StateName = null;
    this.data.PostalCode = null;

    var tAddress = this._HelperService.GoogleAddressArrayToJson(event.address_components);

    if(tAddress){
      let countryName=tAddress.country;
      this.countryData=this.country_List.find(ele => ele.Name === countryName);
    //  this._HelperService.UserCountry.CountryId=this.countryData.ReferenceId;
    //   this._HelperService.UserCountry.CountryKey=countryData.ReferenceKey;
    //   this._HelperService.UserCountry.CountryName=countryData.Name;
    this.data.CountryCode=this.countryData.ReferenceKey;
       this.data.CountryId=this.countryData.ReferenceId;
    }
    if (tAddress.postal_code != undefined && tAddress.postal_code != null && tAddress.postal_code != "") {
      this.data.PostalCode = tAddress.postal_code;
    }
    if (tAddress.country != undefined && tAddress.country != null && tAddress.country != "") {
      this.data.CountryName = tAddress.country;
    }
    if (tAddress.administrative_area_level_1 != undefined && tAddress.administrative_area_level_1 != null && tAddress.administrative_area_level_1 != "") {
      this.data.StateName = tAddress.administrative_area_level_1;
    }
    if (tAddress.locality != undefined && tAddress.locality != null && tAddress.locality != "") {
      this.data.CityName = tAddress.locality;
    }
    if (tAddress.administrative_area_level_2 != undefined && tAddress.administrative_area_level_2 != null && tAddress.administrative_area_level_2 != "") {
      this.data.CityAreaName = tAddress.administrative_area_level_2;
    }
    if (tAddress.administrative_area_level_2 != undefined && tAddress.administrative_area_level_2 != null && tAddress.administrative_area_level_2 != "") {
      this.data.CityAreaName = tAddress.administrative_area_level_2;
    }
    if (this.data.CountryName != this.countryData.Name) {
      this._HelperService.NotifyError('Currently we’re not serving in this area, please add locality within ' + this._HelperService.UserCountry.CountryName);
      this.HCXLocManager_OpenUpdateManager_Clear();
    }
    else {
      // this.data.CountryId = this.countryData.ReferenceId;
      // this.data.CountryCode = this._HelperService.UserCountry.CountryKey;
      // this.data.CountryName = this._HelperService.UserCountry.CountryName;
      this.HCXLoc_Manager_GetStates();
    }
    // this.HCXLoc_Manager_GetStates();
  }
  HCXLoc_Manager_GetStates() {
    this.ShowState = false;
    var PData = {
      Task: this._HelperService.AppConfig.Api.Core.getstates,
      ReferenceKey: this.data.CountryCode,
      ReferenceId: this.data.CountryId,
      Offset: 0,
      Limit: 1000,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.State, PData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            var _Data = _Response.Result.Data as any[];
            if (_Data.length > 0) {
              let finalCat: any = [];
              this.ShowState = false;
              _Data.forEach(element => {
                var Item = {
                  id: element.ReferenceId,
                  key: element.ReferenceKey,
                  text: element.Name,
                  additional: element,
                };
                if (this.data.StateName != undefined || this.data.StateName != null || this.data.StateName != '') {
                  if (element.Name == this.data.StateName) {
                    this.data.StateId = Item.id;
                    this.data.StateCode = Item.key;
                    this.data.StateName = Item.text;
                    this.ShowCity = false;
                    this.HCXLoc_Manager_City_Load();
                    setShowStateFlag(true)
                  }
                }
                finalCat.push(Item);
              });
              this.HCXLocManager_S2States_Data = finalCat;
              this.ShowState = true;
              if (this.data.CityName != undefined && this.data.CityName != null && this.data.CityName != "") {
                this.HCXLoc_Manager_GetStateCity(this.data.CityName);
              }
            }
            else {
              setShowStateFlag(true);
            }
          }
          else {
            setShowStateFlag(true);
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });

    let setShowStateFlag = function (this: any, state: boolean) {
      setTimeout(() => {
        this.ShowState = state;
      }, 500);
    }.bind(this);
  }
  HCXLoc_Manager_City_Load() {
    let PData: any = {
      Task: this._HelperService.AppConfig.Api.Core.getcities,
      ReferenceKey: this.data.StateCode,
      ReferenceId: this.data.StateId,
      SearchCondition: "",
      Offset: 0,
      Limit: 1000,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.City, PData);
    _OResponse.subscribe(
      _Response => {
        let finalRes: any = [];
        _Response.Result.Data.forEach((element: any) => {
          var Item = {
            id: element.ReferenceId,
            key: element.ReferenceKey,
            text: element.Name,
            additional: element,
          };
          finalRes = [...finalRes, Item];
        });
        this.HCXLocManager_S2Cities_Data = finalRes;
      });
  }
  HCXLoc_Manager_GetStateCity(CityName: string) {
    let PData: any = {
      Task: this._HelperService.AppConfig.Api.Core.getcities,
      ReferenceKey: this.data.StateCode,
      ReferenceId: this.data.StateId,
      SearchCondition: this._HelperService.GetSearchConditionStrict('', 'Name', 'text', CityName, '='),
      Offset: 0,
      Limit: 1,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.City, PData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            var _Result = _Response.Result.Data;
            let finalRes: any = [];
            _Result.forEach((element: any) => {
              var Item = {
                id: element.ReferenceId,
                key: element.ReferenceKey,
                text: element.Name,
                additional: element,
              };
              finalRes.push(Item);
            });
            this.HCXLocManager_S2Cities_Data = finalRes;
            if (_Result != undefined && _Result != null && _Result.length > 0) {
              var Item = _Result[0];
              this.data.CityId = Item.ReferenceId;
              this.data.CityCode = Item.ReferenceKey;
              this.data.CityName = Item.Name;
              this.ShowCity = false;
              setTimeout(() => {
                this.ShowCity = true;
              }, 500);
              this.HCXLoc_Manager_SetAddress_Emit();
            }
            else {
              this.data.CityId = 0;
              this.data.CityCode = null;
              this.data.CityName = null;
              this.ShowCity = false;
              setTimeout(() => {
                this.ShowCity = true;
              }, 500);
            }
          }
          else {
            this.data.CityId = 0;
            this.data.CityCode = null;
            this.data.CityName = null;
            this.ShowCity = false;
            setTimeout(() => {
              this.ShowCity = true;
            }, 500);
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
  HCXLoc_Manager_City_Change(Item: any) {
    let _selectedCity: any = this.HCXLocManager_S2Cities_Data.find(e => e.id == Item);

    if (_selectedCity != undefined && _selectedCity != null) {
      this.data.CityId = _selectedCity.id;
      this.data.CityCode = _selectedCity.key;
      this.data.CityName = _selectedCity.text;
      this.HCXLoc_Manager_SetAddress_Emit();
    }
  }
  HCXLoc_Manager_StateSelected(Item: any) {
    let _selectedState: any = this.HCXLocManager_S2States_Data.find(e => e.id == Item);
    if (Item == "43") {
      this.validState = _selectedState;
      this._HelperService.NotifyError("Please select Valid state");
    }
    else if (_selectedState != undefined && _selectedState != null) {
      this.data.StateId = _selectedState.id;
      this.data.StateCode = _selectedState.key;
      this.data.StateName = _selectedState.text;
      this.data.CityId = null;
      this.data.CityCode = null;
      this.data.CityName = null;
      this.ShowCity = false;
      this.HCXLoc_Manager_City_Load();
      setTimeout(() => {
        this.ShowCity = true;
      }, 500);
      this.HCXLoc_Manager_SetAddress_Emit();
    }
  }
  HCXLocManager_OpenUpdateManager_Clear() {
    this.IsAddressSet = false;
    this.data =
    {
      Latitude: 0,
      Longitude: 0,

      Address: null,
      MapAddress: null,

      CountryId: this._HelperService.UserCountry.CountryId,
      CountryCode: this._HelperService.UserCountry.CountryKey,
      CountryName: this._HelperService.UserCountry.CountryName,

      StateId: 0,
      StateName: null,
      StateCode: null,

      CityId: 0,
      CityCode: null,
      CityName: null,

      CityAreaId: 0,
      CityAreaName: null,
      CityAreaCode: null,

      PostalCode: null,
    }
  }
  CityChange() {

  }
  StateChange() {

  }
  HCXLoc_Manager_SetAddress_Emit() {
    if (this.validState == "43") {
      this.data.StateId = 43
    }
    this.IsAddressSet = true;
    this.AddressComponent.emit(this.data);
  }

}












