import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegaddressComponent } from './regaddress.component';

describe('RegaddressComponent', () => {
  let component: RegaddressComponent;
  let fixture: ComponentFixture<RegaddressComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegaddressComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegaddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
