import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { HelperService } from 'src/app/utils';
import { OResponse } from 'src/app/interfaces';

@Component({
    selector: 'app-logout-modal',
    templateUrl: './logout-modal.component.html',
    styleUrls: ['./logout-modal.component.css']
})
export class LogoutModalComponent implements OnInit {

    constructor(
        public activeModal: NgbActiveModal, 
        public _HelperService: HelperService,
        private _Router: Router,
    )
    {}

    ngOnInit(): void {
        
    }

    processLogout() {
            var pData = {
              Task: this._HelperService.AppConfig.Api.Logout
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.System, pData);
            _OResponse.subscribe(
              _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                  this._HelperService.NotifySuccess(_Response.Message);
                  this._HelperService.DeleteStorage('templocmerchant');
    
                  this._HelperService.templocmerchant = false;
                  this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Account);
                  this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Location);
                  this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.OReqH);
                  this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Permissions);
                  localStorage.clear();
                  this.activeModal.dismiss()
                  this._Router.navigate([this._HelperService.AppConfig.Pages.System.Login]);
    
                }
                else {
                  this._HelperService.NotifyError(_Response.Message);
                }
              },
              _Error => {
                this._Router.navigate([this._HelperService.AppConfig.Pages.System.Login]);
              });
      }
}