import { NgModule } from '@angular/core';

import { CreateStoreComponent } from './components/create-store/create-store.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { IconsModule } from './index';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { GoogleMapsModule } from '@angular/google-maps';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { HcxaddressmanagerComponent } from './components/hcxaddressmanager/hcxaddressmanager.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { TruncateTextPipe } from '../pipe/truncate-text.pipe';
import { RegaddressComponent } from './components/regaddress/regaddress.component';
import { LogoutModalComponent } from './components/logout-modal/logout-modal.component';
import { addPaymentComponent } from './components/add-payment/add-payment.component';
import { withdrawModalComponent } from './components/withdraw/withdraw.component';
import { TransactionViewComponent } from './components/transaction-view/transaction-view.component';

@NgModule({
  imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IconsModule,
        NgbModule,
        NgSelectModule,
        GoogleMapsModule, 
        GooglePlaceModule,
        ImageCropperModule
  ],
  declarations:[
    CreateStoreComponent,
    HcxaddressmanagerComponent,
    RegaddressComponent,
    TruncateTextPipe,
    LogoutModalComponent,
    addPaymentComponent,
    withdrawModalComponent,
    TransactionViewComponent
  ],
  exports: [
    CreateStoreComponent,
    HcxaddressmanagerComponent,
    RegaddressComponent,
    TruncateTextPipe,
    LogoutModalComponent,
    addPaymentComponent,
    withdrawModalComponent,
    TransactionViewComponent
  ]
})
export class SharedModule { }