import { TestBed } from '@angular/core/testing';

import { IndexResolver } from './index.resolver';

describe('IndexResolver', () => {
  let resolver: IndexResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(IndexResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
